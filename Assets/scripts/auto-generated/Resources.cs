//This class is auto-generated do not modify
namespace k
{
	public static class Resources
	{
		public const string ANDROID_NATIVE_SETTINGS = "AndroidNativeSettings";
		public const string GOLD = "gold";
		public const string SOCIAL_SETTINGS = "SocialSettings";
		public const string ISDSETTINGS_RESOURCE = "ISDSettingsResource";
		public const string IOSNATIVE_SETTINGS = "IOSNativeSettings";
		public const string FACEBOOK_SETTINGS = "FacebookSettings";
		public const string INFO = "Info";
		public const string INFO_PLIST = "en.lproj/InfoPlist";
		public const string SPRITE_DEFAULT = "SpriteDefault";
		public const string SPRITE_MASK = "SpriteMask";
	}
}