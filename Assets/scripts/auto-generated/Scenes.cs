//This class is auto-generated do not modify
namespace k
{
	public static class Scenes
	{
		public const string TEST_GAME = "testGame";
		public const string _INIT_INTRO = "__initIntro";
		public const string _INIT = "__init";

		public const int TOTAL_SCENES = 3;


		public static int nextSceneIndex()
		{
			if( UnityEngine.Application.loadedLevel + 1 == TOTAL_SCENES )
				return 0;
			return UnityEngine.Application.loadedLevel + 1;
		}
	}
}