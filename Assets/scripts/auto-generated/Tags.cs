//This class is auto-generated do not modify
namespace k
{
	public static class Tags
	{
		public const string UNTAGGED = "Untagged";
		public const string RESPAWN = "Respawn";
		public const string FINISH = "Finish";
		public const string EDITOR_ONLY = "EditorOnly";
		public const string MAIN_CAMERA = "MainCamera";
		public const string PLAYER = "Player";
		public const string GAME_CONTROLLER = "GameController";
		public const string SHIELD = "shield";
		public const string INVULNERABLE = "invulnerable";
		public const string G2 = "g2";
		public const string G3 = "g3";
		public const string BOMB = "bomb";
		public const string G5 = "g5";
		public const string WORLD = "world";
	}
}