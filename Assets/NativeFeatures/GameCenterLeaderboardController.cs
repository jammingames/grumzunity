﻿using UnityEngine;
using System.Collections;

public class GameCenterLeaderboardController : MonoBehaviour
{

#if UNITY_IPHONE

		private string leaderBoardId = "jammingamesGrumzTestLeaderBoard";
		private string TEST_ACHIEVEMENT_1_ID = "com.jammingames.grumz.earn100";

		private static bool IsInitialized = false;


		// Use this for initialization
		void Awake ()
		{
				if (!IsInitialized) {
			
						//Achievement registration. If you skip this step GameCenterManager.achievements array will contain only achievements with reported progress 
						GameCenterManager.RegisterAchievement (TEST_ACHIEVEMENT_1_ID);
						//			GameCenterManager.RegisterAchievement (TEST_ACHIEVEMENT_2_ID);
			
			
						//Listen for the Game Center events
						GameCenterManager.OnAchievementsProgress += HandleOnAchievementsProgress;
						GameCenterManager.OnAchievementsReset += HandleOnAchievementsReset;
			
						GameCenterManager.OnScoreSubmitted += OnScoreSubmitted;
						GameCenterManager.OnPlayerScoreLoaded += HandleOnPlayerScoreLoaded;
			
			
						GameCenterManager.OnAuthFinished += OnAuthFinished;
			
						GameCenterManager.OnAchievementsLoaded += OnAchievementsLoaded;
			
			
						//Initializing Game Center class. This action will trigger authentication flow
						GameCenterManager.Init ();
						IsInitialized = true;

				}

				ScoreManager.instance.OnEarnedTimeAttack += HandleOnEarnedTimeAttack;
				ScoreManager.instance.OnHighScore += HandleOnSubmitScore;
				ScoreManager.instance.OnRequestLeaderboard += ShowGameCenter;
				
				GetScore ();
		}

		

	#region GameEvents

		void HandleOnEarnedTimeAttack (bool newScore)
		{
				if (newScore) {
						SubmitAchievements (100);
				}
		}

		void HandleOnSubmitScore (bool isNewScore, int newScore)
		{
				if (isNewScore) {
						SubmitScore (newScore);
				}
		}

	#endregion


		public void ShowGameCenter ()
		{
				GameCenterManager.ShowLeaderboards ();
		}

		public void SubmitScore (int score)
		{
				GameCenterManager.ReportScore (score, leaderBoardId);
		}

		public void GetScore ()
		{
				GameCenterManager.LoadCurrentPlayerScore (leaderBoardId);
		
		}

		public void SubmitAchievements (int score)
		{
				GameCenterManager.SubmitAchievement (score, TEST_ACHIEVEMENT_1_ID, true);
		}





	
	#region eventHandler
	
	

	
		private void OnAchievementsLoaded (ISN_Result result)
		{
		
				Debug.Log ("OnAchievementsLoaded");
				Debug.Log (result.IsSucceeded);
		
				if (result.IsSucceeded) {
						Debug.Log ("Achievements were loaded from iOS Game Center");
			
						foreach (GK_AchievementTemplate tpl in GameCenterManager.Achievements) {
								Debug.Log (tpl.Id + ":  " + tpl.Progress);
						}
				}
		
		}
	
		void OnLeaderboardSetsInfoLoaded (ISN_Result res)
		{
				Debug.Log ("OnLeaderboardSetsInfoLoaded");
				GameCenterManager.OnLeaderboardSetsInfoLoaded -= OnLeaderboardSetsInfoLoaded;
				if (res.IsSucceeded) {
						foreach (GK_LeaderboardSet s in GameCenterManager.LeaderboardSets) {
								Debug.Log (s.Title);
								Debug.Log (s.Identifier);
								Debug.Log (s.GroupIdentifier);
						}
				}
		
		
				if (GameCenterManager.LeaderboardSets.Count == 0) {
						return;
				}
		
				GK_LeaderboardSet LeaderboardSet = GameCenterManager.LeaderboardSets [0];
				LeaderboardSet.OnLoaderboardsInfoLoaded += OnLoaderboardsInfoLoaded;
				LeaderboardSet.LoadLeaderBoardsInfo ();
		
		}
	
		void OnLoaderboardsInfoLoaded (ISN_LoadSetLeaderboardsInfoResult res)
		{
				res.LeaderBoardsSet.OnLoaderboardsInfoLoaded -= OnLoaderboardsInfoLoaded;
		
				if (res.IsSucceeded) {
						foreach (GK_LeaderBoardInfo l in res.LeaderBoardsSet.BoardsInfo) {
								Debug.Log (l.Title);
								Debug.Log (l.Description);
								Debug.Log (l.Identifier);
						}
				}
		
		}
	
		void HandleOnAchievementsReset (ISN_Result obj)
		{
				Debug.Log ("All Achievements were reset");
		}
	
	
		private void HandleOnAchievementsProgress (GK_AchievementProgressResult result)
		{
				if (result.IsSucceeded) {
						GK_AchievementTemplate tpl = result.Achievement;
						Debug.Log (tpl.Id + ":  " + tpl.Progress.ToString ());
				}
		}
	
		private void HandleOnPlayerScoreLoaded (GK_PlayerScoreLoadedResult result)
		{
				if (result.IsSucceeded) {
						GK_Score score = result.loadedScore;
						int loadedScore = unchecked((int)score.GetLongScore ());
						//			bestScore = Convert.ToInt32 (score.GetLongScore ());
						ScoreManager.instance.CheckHighScore (loadedScore);
						//bestScore = (int)score.GetLongScore ();
						//PlayerPrefs.SetInt (HIGH_SCORE, bestScore);
						//IOSNativePopUpManager.showMessage ("Leaderboard " + score.leaderboardId, "Score: " + score.score + "\n" + "Rank:" + score.rank);
				} else if (result.IsFailed) {
//						if (PlayerPrefs.HasKey (HIGH_SCORE))
//								bestScore = PlayerPrefs.GetInt (HIGH_SCORE);
//						else {
//								firstGame = true;
//								bestScore = 0;
//								//PlayerPrefs.SetInt (HIGH_SCORE, 0);
//				SubmitScore(bestScore);
//								//SubmitScore (bestScore);
//						}
				}
		}
	
	
	
	
		private void OnScoreSubmitted (ISN_Result result)
		{
				//GameCenterManager.OnScoreSubmitted -= OnScoreSubmitted;
		
				if (result.IsSucceeded) {
			
						Debug.Log ("Score Submitted");
				} else {
						Debug.Log ("Score failed to submit to game center");
				}
		}
	
	
		void OnAuthFinished (ISN_Result res)
		{
				if (res.IsSucceeded) {
						//IOSNativePopUpManager.showMessage ("Player Authed ", "ID: " + GameCenterManager.Player.Id + "\n" + "Alias: " + GameCenterManager.Player.Alias);
			
				} else {
						//IOSNativePopUpManager.showMessage ("Game Center ", "Player authentication failed");
				}
		}
	
	
		void OnPlayerSignatureRetrieveResult (GK_PlayerSignatureResult result)
		{
				Debug.Log ("OnPlayerSignatureRetrieveResult");
		
				if (result.IsSucceeded) {
			
						Debug.Log ("PublicKeyUrl: " + result.PublicKeyUrl);
						Debug.Log ("Signature: " + result.Signature);
						Debug.Log ("Salt: " + result.Salt);
						Debug.Log ("Timestamp: " + result.Timestamp);
			
				} else {
						Debug.Log ("Error code: " + result.Error.Code);
						Debug.Log ("Error description: " + result.Error.Description);
				}
		
				GameCenterManager.OnPlayerSignatureRetrieveResult -= OnPlayerSignatureRetrieveResult;
		
		
		
		}

	#endregion

#endif

}
