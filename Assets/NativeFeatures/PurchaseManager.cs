﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PurchaseManager : ISN_Singleton<PurchaseManager>
{

		public void BuyAds ()
		{
#if UNITY_IPHONE
				IOSDialog dialog = IOSDialog.Create ("Purchase Remove Ads?", "Remove Ads forever for " + localPrice + "?");
				dialog.OnComplete += HandleOnComplete;
#endif
		}

#if UNITY_ANDROID

#endif


	
		public const string REMOVE_ADS = "123567432";
		public static string localPrice;
		public bool hasBoughtAds = false;
//		public SetPurchasePriceText priceText;
		// Use this for initialization
		void Start ()
		{
				if (!PlayerPrefs.HasKey ("Ads")) {
						PlayerPrefs.SetInt ("Ads", 1);
				}
				
				if (PlayerPrefs.GetInt ("Ads") == 1) {
						#if UNITY_ANDROID
		
						#endif

									
						#if UNITY_IPHONE
						IOSInAppPurchaseManager.OnStoreKitInitComplete += OnStoreKitInitComplete;
						IOSInAppPurchaseManager.OnTransactionComplete += OnTransactionComplete;

						IOSInAppPurchaseManager.instance.addProductId (REMOVE_ADS);
						//IOSInAppPurchaseManager.instance.addProductId(SMALL_PACK);
						IOSInAppPurchaseManager.instance.loadStore ();
#endif
				} else if (PlayerPrefs.GetInt ("Ads") == 0) {
						G.i.TriggerRemoveAds ();
						Instance.hasBoughtAds = true;
				}

		
		}

				#if UNITY_IPHONE

		void HandleOnComplete (IOSDialogResult obj)
		{
				//parsing result
				switch (obj) {
				case IOSDialogResult.YES:
						IOSInAppPurchaseManager.instance.buyProduct (REMOVE_ADS);
						Debug.Log ("Yes button pressed");
						break;
				case IOSDialogResult.NO:
						IOSNativePopUpManager.dismissCurrentAlert ();
						Debug.Log ("No button pressed");
						break;
				}

				IOSNativePopUpManager.showMessage ("Result", obj.ToString () + " button pressed");
		}

		private static void OnStoreKitInitComplete (ISN_Result result)
		{
				IOSInAppPurchaseManager.OnStoreKitInitComplete -= OnStoreKitInitComplete;
		
				if (result.IsSucceeded) {
						IOSInAppPurchaseManager.instance.addProductId (REMOVE_ADS);
						print ("Can purchase in app stuff? :    " + IOSInAppPurchaseManager.Instance.IsInAppPurchasesEnabled);
						Debug.Log ("Inited successfully, Avaliable products cound: " + IOSInAppPurchaseManager.Instance.products.Count.ToString ());
						localPrice = IOSInAppPurchaseManager.Instance.GetProductById (REMOVE_ADS).localizedPrice;
//						if (Instance.priceText != null)
//								Instance.priceText.SetPriceText (localPrice);
				} else {
						Debug.Log ("StoreKit Init Failed.  Error code: " + result.Error.Code + "\n" + "Error description:" + result.Error.Description);
						IOSInAppPurchaseManager.OnStoreKitInitComplete += OnStoreKitInitComplete;
						IOSInAppPurchaseManager.Instance.loadStore ();


				}
		}

		private static void OnTransactionComplete (IOSStoreKitResult responce)
		{
		
				Debug.Log ("OnTransactionComplete: " + responce.ProductIdentifier);
				Debug.Log ("OnTransactionComplete: state: " + responce.State);

				switch (responce.State) {
				case InAppPurchaseState.Purchased:
				case InAppPurchaseState.Restored:
			//Our product been succsesly purchased or restored
			//So we need to provide content to our user 
			//depends on productIdentifier
						UnlockProducts (responce.ProductIdentifier);
						break;
				case InAppPurchaseState.Deferred:
			//iOS 8 introduces Ask to Buy, which lets 
			//parents approve any purchases initiated by children
			//You should update your UI to reflect this 
			//deferred state, and expect another Transaction 
			//Complete  to be called again with a new transaction state 
			//reflecting the parent's decision or after the 
			//transaction times out. Avoid blocking your UI 
			//or gameplay while waiting for the transaction to be updated.
						break;
				case InAppPurchaseState.Failed:
			//Our purchase flow is failed.
			//We can unlock interface and report user that the purchase is failed. 
						Debug.Log ("Transaction failed with error, code: " + responce.Error.Code);
						Debug.Log ("Transaction failed with error, description: " + responce.Error.Description);
						break;
				}
		
				IOSNativePopUpManager.showMessage ("Store Kit Response", "product " + responce.ProductIdentifier + " state: " + responce.State.ToString ());
		}

	
		private static void UnlockProducts (string productIdentifier)
		{
				switch (productIdentifier) {
				case REMOVE_ADS:
						G.i.TriggerRemoveAds ();
						Instance.hasBoughtAds = true;
						PlayerPrefs.SetInt ("Ads", 0);
						IOSNativePopUpManager.showMessage ("Congratulations!", "Thank you for your purchase");
			//code for adding small game money amount here
						break;
		
			
				}
		}
				#endif
}
