﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetPurchasePriceText : MonoBehaviour
{

		Text _text;
		string newPrice = "";
		// Use this for initialization
		void OnEnable ()
		{
				_text = GetComponent<Text> ();
				if (newPrice != "")
						_text.text = newPrice;
		
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void SetPriceText (string price)
		{
				newPrice = price;
				_text.text = price;
		}
}
