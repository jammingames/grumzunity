﻿using UnityEngine;
using System.Collections;

public class CloudController : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
				InitICloud ();
		}
		
		
		
		
		void TestICloud ()
		{

			
				iCloudManager.Instance.setString ("TestStringKey", "Hello World");
				iCloudManager.Instance.setFloat ("TestFloatKey", 1.1f);
		
				string msg = "hello world";
				System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding ();
				byte[] data = encoding.GetBytes (msg);
				iCloudManager.Instance.setData ("TestByteKey", data);
		}

		void InitICloud ()
		{
				
				iCloudManager.OnCloundInitAction += OnCloundInitAction;
				//iCloudManager.OnCloudDataChangedAction += HandleOnCloudDataChangedAction;
				iCloudManager.OnCloudDataReceivedAction += HandleOnCloudDataReceivedAction;
		
				iCloudManager.Instance.init ();
		}

		void OnCloundInitAction (ISN_Result result)
		{
				if (result.IsSucceeded) {
						//IOSNativePopUpManager.showMessage ("iCloud", "Initialization Sucsess!");
						TestICloud ();
				} else {
						IOSNativePopUpManager.showMessage ("iCloud", "Initialization Failed!");
				}
		}

		private void HandleOnCloudDataChangedAction ()
		{

		}

		private void HandleOnCloudDataReceivedAction (iCloudData data)
		{
				if (data.IsEmpty) {
						IOSNativePopUpManager.showMessage (data.key, "data is empty");
				} else {
						IOSNativePopUpManager.showMessage (data.key, data.stringValue);
				}
		}	
}
