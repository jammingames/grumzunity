﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
#if UNITY_EDITOR
using System.IO;
#endif


public class ScreenShotManager : MonoBehaviour
{

		public GameObject[] thingsToDisable;
		public GameObject[] thingsToEnable;
		public GameObject artForShot;
		


		bool[] thingsToEnableState;
		bool[] thingsToDisableState;
		
		public Button[] buttonsToDisable;

		public Text scoreText;

		bool isFacebook;
		float score, best;


		
		bool isEnabled;
		void Awake ()
		{
#if UNITY_IPHONE
				IOSSocialManager.OnFacebookPostResult += HandleOnFacebookPostResult;
				IOSSocialManager.OnTwitterPostResult += HandleOnTwitterPostResult;
#endif
		}

		void Start ()
		{
				


				thingsToEnableState = new bool[thingsToEnable.Length];
				thingsToDisableState = new bool[thingsToDisable.Length];

				for (int i = 0; i < thingsToDisable.Length; i++) {
						thingsToDisableState [i] = thingsToDisable [i].activeInHierarchy;

				}
				
				for (int i = 0; i < thingsToEnable.Length; i++) {
						thingsToEnableState [i] = thingsToEnable [i].activeInHierarchy;

			
				}
		}
		
		

		public void ShareToFB ()
		{
				StartCoroutine (ScreenshotSequence (true));
		}

		public void ShareToTW ()
		{
				StartCoroutine (ScreenshotSequence (false));
		}


		
		public void ToggleStuff ()
		{
				if (isEnabled) {
						StartCoroutine (DisableStuff ());
						isEnabled = false;
				} else if (!isEnabled) {
						StartCoroutine (EnableStuff ());
						isEnabled = true;
				}
		}


		IEnumerator DisableStuff ()
		{
				for (int i = 0; i< buttonsToDisable.Length; i++) {
						buttonsToDisable [i].interactable = false;
				}

				
		
				for (int i = 0; i < thingsToDisable.Length; i++) {
					
						thingsToDisable [i].SetActive (false);
				}
				yield return null;
				for (int i = 0; i < thingsToEnable.Length; i++) {
					
						thingsToEnable [i].SetActive (true);
			
				}
		
				scoreText.gameObject.SetActive (true);
				scoreText.text = "SCORE\n" + ScoreManager.instance.totalScore;
		
		
		
				yield return null;
		
				artForShot.SetActive (true);
				AdBannerController.instance.DisableAdBanner ();
		}

		IEnumerator EnableStuff ()
		{
		
				yield return null;
		
				artForShot.SetActive (false);
		
				scoreText.text = " ";
		
//				scoreText.gameObject.SetActive (false);
		
		
		
				
				if (thingsToEnable.Length > 0) {

						for (int i = 0; i < thingsToEnable.Length; i++) {
								thingsToEnable [i].SetActive (thingsToEnableState [i]);
						}
			
				}
				
				for (int i = 0; i < thingsToDisable.Length; i++) {
						thingsToDisable [i].SetActive (thingsToDisableState [i]);
				}
				for (int i = 0; i< buttonsToDisable.Length; i++) {
						buttonsToDisable [i].interactable = true;
				}
				yield return null;
		}


		//public void TakeShot(float currentScore, float bestScore, bool isFb)
		
		IEnumerator ScreenshotSequence (bool isFB)
		{
				yield return StartCoroutine (DisableStuff ());
				//yield return new WaitForSeconds (2);
				




				yield return new WaitForEndOfFrame ();
				// Create a texture the size of the screen, RGB24 format
				int width = Screen.width;
				int height = Screen.height;
				Texture2D tex = new Texture2D (width, height, TextureFormat.RGB24, false);
				// Read screen contents into the texture
				tex.ReadPixels (new Rect (0, 0, width, height), 0, 0);
				tex.Apply ();
				yield return new WaitForEndOfFrame ();

				#if UNITY_EDITOR
				
				byte[] bytes = tex.EncodeToPNG ();
				int imageCount = Directory.GetFiles (@Application.dataPath + "/../screenCaps", "*.png").Length + 1;
		Debug.Log(imageCount);

				File.WriteAllBytes (Application.dataPath + "/../screenCaps/testscreen-" + imageCount + ".png", bytes);
		
				#endif


#if UNITY_IPHONE
				if (isFB) {

						IOSSocialManager.instance.FacebookPost ("Top new score in #Grumz ", "http://picnicgamelabs.com/comingsoon/", tex);
				} else {
						IOSSocialManager.instance.TwitterPost ("Top new score in #Grumz ", "http://picnicgamelabs.com/comingsoon/", tex);
				}



				IOSCamera.Instance.SaveTextureToCameraRoll (tex);		
#endif
#if UNITY_ANDROID
		if (isFB) {
			AndroidSocialGate.StartShareIntent("New #Grumz Score!", "Top new score in #Grumz", tex,  "facebook.katana");
			//SPShareUtility.Fa
		}
		else
		{
			AndroidSocialGate.StartShareIntent("New #Grumz Score!", "Top new score in #Grumz", tex, "twi" );

			//SPShareUtility.TwitterShare("Top new score in #Grumz", tex);

		}


#endif

				

				Destroy (tex);

				yield return StartCoroutine (EnableStuff ());

		

		

		
	

				

		}

		




	
		void HandleOnTwitterPostResult (ISN_Result res)
		{
				if (res.IsSucceeded) {
						//		IOSNativePopUpManager.showMessage ("Posting example", "Post Success!");
				} else {
						//		IOSNativePopUpManager.showMessage ("Posting example", "Post Failed :(");
				}
		}
	
		void HandleOnFacebookPostResult (ISN_Result res)
		{
				if (res.IsSucceeded) {
						//		IOSNativePopUpManager.showMessage ("Posting example", "Post Success!");
				} else {
						//		IOSNativePopUpManager.showMessage ("Posting example", "Post Failed :(");
				}
		}



		static ScreenShotManager _instance = null;
		public static ScreenShotManager instance {
				get {
						if (!_instance) {
								// check if a ScreenShotManager is already available in the scene graph
								_instance = FindObjectOfType (typeof(ScreenShotManager)) as ScreenShotManager;

							
							
						}

						return _instance;
				}
		}


		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}

}
