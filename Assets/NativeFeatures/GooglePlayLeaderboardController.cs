﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GooglePlayLeaderboardController : MonoBehaviour
{

#if UNITY_ANDROID
		//example
		private const string LEADERBOARD_NAME = "leaderboard_highscore";
		//private const string LEADERBOARD_NAME = "REPLACE_WITH_YOUR_NAME";
	
	
		//example
		private const string LEADERBOARD_ID = "CgkIzf3oi8IZEAIQAQ";
		//private const string LEADERBOARD_ID = "REPLACE_WITH_YOUR_ID";

		void Awake ()
		{
				
		}

		void Start ()
		{
				//listen for GooglePlayConnection events
				GooglePlayConnection.ActionPlayerConnected += OnPlayerConnected;
				GooglePlayConnection.ActionPlayerDisconnected += OnPlayerDisconnected;
		
		
				GooglePlayConnection.ActionConnectionResultReceived += ActionConnectionResultReceived;
		
//				GooglePlayConnection.Instance.connect ();
		
			
				
//				GooglePlayManager.ActionAvailableDeviceAccountsLoaded += ActionAvailableDeviceAccountsLoaded;
		
//				GooglePlayManager.Instance.RetrieveDeviceGoogleAccounts ();
				
		
		
				if (GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) {
						//checking if player already connected
						OnPlayerConnected ();
				} 


				ScoreManager.instance.OnEarnedTimeAttack += HandleOnEarnedTimeAttack;
				ScoreManager.instance.OnHighScore += HandleOnSubmitScore;
				ScoreManager.instance.OnRequestLeaderboard += showLeaderBoard;

				GooglePlayConnection.Instance.connect ();
				//GooglePlayConnection.Instance.connect ();


			
				//GooglePlayManager.ActionAvaliableDeviceAccountsLoaded += ActionAvaliableDeviceAccountsLoaded;
				//GooglePlayManager.Instance.RetriveDeviceGoogleAccounts ();

		}
		
//
//		void Update ()
//		{
//				if (Input.GetKeyDown (KeyCode.S)) {
//						Debug.Log ("Test: " + GooglePlayConnection.state);
//						GooglePlayConnection.Instance.connect ();
//				}
//		}


		void OnDestroy ()
		{
				if (!GooglePlayConnection.IsDestroyed) {
			
						GooglePlayConnection.ActionPlayerConnected -= OnPlayerConnected;
						GooglePlayConnection.ActionPlayerDisconnected -= OnPlayerDisconnected;
						GooglePlayConnection.ActionConnectionResultReceived -= ActionConnectionResultReceived;
				}
		
				if (!GooglePlayManager.IsDestroyed) {
			

						GooglePlayManager.ActionScoreSubmited -= OnScoreSubmited;
			
			
						GooglePlayManager.ActionAvailableDeviceAccountsLoaded -= ActionAvailableDeviceAccountsLoaded;
						
			

			
				}
		}





		private void GetAccs ()
		{
				GooglePlayManager.Instance.RetrieveDeviceGoogleAccounts ();
		}

	
		private void showLeaderBoardsUI ()
		{
				GooglePlayManager.Instance.ShowLeaderBoardById (LEADERBOARD_ID);
				//SA_StatusBar.text = "Showing Leader Boards UI";
		}
	
		private void loadLeaderBoards ()
		{
		
				//listening for load event 
				GooglePlayManager.ActionLeaderboardsLoaded += OnLeaderBoardsLoaded;
				GooglePlayManager.Instance.LoadLeaderBoards ();
				//SA_StatusBar.text = "Loading Leader Boards Data...";
		}
	
		private void showLeaderBoard ()
		{
				GooglePlayManager.Instance.ShowLeaderBoardById (LEADERBOARD_ID);
				//SA_StatusBar.text = "Shwoing Leader Board UI for " + LEADERBOARD_ID;
		}
	
	
		private void submitScore (int newScore)
		{
				GooglePlayManager.Instance.SubmitScore (LEADERBOARD_NAME, newScore);
				//SA_StatusBar.text = "Score " + score.ToString () + " Submited for " + LEADERBOARD_NAME;
		}
	

	
		private void ResetBoard ()
		{
				GooglePlayManager.Instance.ResetLeaderBoard (LEADERBOARD_ID);
				UpdateBoardInfo ();
		}
	
	
	

	
	
	
		public void clearDefaultAccount ()
		{
				GooglePlusAPI.Instance.ClearDefaultAccount ();
		}
	
	

	
	
	
		//--------------------------------------
		// EVENTS
		//--------------------------------------
	#region GameEvents
	
		void HandleOnEarnedTimeAttack (bool newScore)
		{
				if (newScore) {
						Debug.Log ("time attack unlocked");
						//			SubmitAchievements (100);
				}
		}
	
		void HandleOnSubmitScore (bool isNewScore, int newScore)
		{
				if (isNewScore) {
						submitScore (newScore);
				}
		}
	
	#endregion

	#region events
	
		private void OnLeaderBoardsLoaded (GooglePlayResult result)
		{
				GooglePlayManager.ActionLeaderboardsLoaded -= OnLeaderBoardsLoaded;
		
				if (result.isSuccess) {
						if (GooglePlayManager.Instance.GetLeaderBoard (LEADERBOARD_ID) == null) {
								//AN_PoupsProxy.showMessage ("Leader boards loaded", LEADERBOARD_ID + " not found in leader boards list");
								return;
						}
			
			
						Debug.Log (LEADERBOARD_NAME + "  score  " + GooglePlayManager.Instance.GetLeaderBoard (LEADERBOARD_ID).GetCurrentPlayerScore (GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).score.ToString ());
						
						long newScore = GooglePlayManager.Instance.GetLeaderBoard (LEADERBOARD_ID).GetCurrentPlayerScore (GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).score;
						int scoreToCheck = unchecked((int)newScore);
						ScoreManager.instance.CheckHighScore (scoreToCheck);
						//int loadedScore = unchecked((int)scoreToCheck;

//						AN_PoupsProxy.showMessage (LEADERBOARD_NAME + "  score", GooglePlayManager.Instance.GetLeaderBoard (LEADERBOARD_ID).GetCurrentPlayerScore (GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).score.ToString ());
				} else {
						Debug.Log (result.message);
//						AN_PoupsProxy.showMessage ("Leader-Boards Loaded error: ", result.message);
				}
		
				UpdateBoardInfo ();
		
		}
	
		private void UpdateBoardInfo ()
		{
				GPLeaderBoard leaderboard = GooglePlayManager.Instance.GetLeaderBoard (LEADERBOARD_ID);
				if (leaderboard != null) {
						Debug.Log ("Id: " + leaderboard.id);
						Debug.Log ("Name: " + leaderboard.name);
						Debug.Log ("All Time Score: " + leaderboard.GetCurrentPlayerScore (GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).score);
			
				} else {
						Debug.Log ("All Time Score: " + " -1");
				}
		}
	
		private void OnScoreSubmited (GooglePlayResult result)
		{
				Debug.Log ("Score Submited:  " + result.message);
		}
	
		private void OnScoreUpdated (GooglePlayResult res)
		{
				UpdateBoardInfo ();
		}
	
	
	
		private void OnPlayerDisconnected ()
		{
				Debug.Log ("Player Disconnected");
				//playerLabel.text = "Player Disconnected";
		}
	
		private void OnPlayerConnected ()
		{
				Debug.Log ("Player Connected");
				Debug.Log (GooglePlayManager.Instance.player.name + "(" + GooglePlayManager.Instance.currentAccount + ")");
				//listen for GooglePlayManager events
				GooglePlayManager.ActionScoreSubmited += OnScoreSubmited;
				GooglePlayManager.ActionScoreRequestReceived += OnScoreUpdated;

				loadLeaderBoards ();
		
		
		

		}
	
		private void ActionConnectionResultReceived (GooglePlayConnectionResult result)
		{
		
				if (result.IsSuccess) {
						Debug.Log ("Connected!");
				} else {
						Debug.Log ("Cnnection failed with code: " + result.code.ToString ());
				}
				Debug.Log ("ConnectionResul:  " + result.code.ToString ());
		}


	
	
	
	
		private void ActionAvailableDeviceAccountsLoaded (List<string> accounts)
		{
				string msg = "Device contains following google accounts:" + "\n";
				foreach (string acc in GooglePlayManager.Instance.deviceGoogleAccountList) {
						msg += acc + "\n";
				} 
		
				Debug.Log ("Accounts Loaded" + msg + " Sign With Fitst one");
				//dialog.ActionComplete += SighDialogComplete;
				GooglePlayConnection.Instance.connect (GooglePlayManager.Instance.deviceGoogleAccountList [0]);
		
		}
	
		private void SighDialogComplete (AndroidDialogResult res)
		{
				if (res == AndroidDialogResult.YES) {
						GooglePlayConnection.Instance.connect (GooglePlayManager.Instance.deviceGoogleAccountList [0]);
				}
		
		}
	#endregion
	
#endif


}
