﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdBannerController : MonoBehaviour
{
#if UNITY_IPHONE

		private iAdBanner banner = null;
#endif
		
		
		public bool showVid = false;
		
		int numDeaths = 0;
		public int elapsedTime = 0;
		public int adTime = 50;

		// Use this for initialization
		void Awake ()
		{
				G.i.OnStateChange += HandleOnStateChange;
				G.i.OnRemoveAds += HandleOnRemoveAds;
		}

		void HandleOnRemoveAds ()
		{
				G.i.OnStateChange -= HandleOnStateChange;
				G.i.OnRemoveAds -= HandleOnRemoveAds;
				GameObject.Destroy (gameObject);
		}

		GameState currentState;

		public IEnumerator CountTime ()
		{
				while (elapsedTime < adTime) {
						yield return StartCoroutine (Auto.Wait (1));
						elapsedTime++;
				}
				if (numDeaths > 1) {
						showVid = true;
				}
		}

		void ResetCounter ()
		{
				showVid = false;
				elapsedTime = 0;
				numDeaths = 0;
				StartCoroutine (CountTime ());
		}



		void Start ()
		{
				#if UNITY_IPHONE
		
				banner = iAdBannerController.instance.CreateAdBanner (TextAnchor.UpperCenter);
				banner.ShowOnLoad = false;
				banner.AdLoadedAction += OnBannerLoaded;
#endif
				if (Advertisement.isSupported)
						Advertisement.Initialize ("1009024", false);
				
				StartCoroutine (CountTime ());
				//banner
		}

		void OnBannerClicked ()
		{
				Debug.Log ("Clicked!\n");
		}
		void OnBannerLoaded ()
		{
				#if UNITY_IPHONE
		
				Debug.Log ("Loaded!\n");
				
				if (currentState == GameState.End)
						banner.Show ();
				//banner.visible = true;
#endif
		}

		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == GameState.End) {
						
						numDeaths++;
						if (elapsedTime >= adTime && numDeaths > 2)
								showVid = true;
						if (showVid && Advertisement.IsReady ()) {
								print (Advertisement.IsReady ());
								Advertisement.Show ();
								ResetCounter ();
						}
						#if UNITY_IPHONE
						 else if (banner.IsLoaded && !banner.IsOnScreen) {
								banner.Show ();


						} else {
								Debug.Log ("banner still loading: " + banner.id);
						}
#endif
				} else if (currentState == GameState.End && nextState != GameState.End) {
						#if UNITY_IPHONE
			
						if (banner.IsOnScreen) {
								banner.Hide ();
								banner.ShowOnLoad = false;
						}
#endif
				}

				currentState = nextState;
		
		}
		

		public void DisableAdBanner ()
		{
				#if UNITY_IPHONE
		

				banner.Hide ();
				banner.ShowOnLoad = false;
#endif

				
		}


		static AdBannerController _instance = null;
		public static AdBannerController instance {
				get {
						if (!_instance) {
								// check if a AdBannerController is already available in the scene graph
								_instance = FindObjectOfType (typeof(AdBannerController)) as AdBannerController;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("AdBannerController");
										_instance = obj.AddComponent<AdBannerController> ();
								}
						}

						return _instance;
				}
		}


		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}

}
