//
//using UnityEngine;
//using UnityEditor;
//using System.Linq;
//using System.Collections;
//using System.Collections.Generic;
//using System.Reflection;
//using System;
//
//[CustomEditor(typeof(FunctionDemo))]
//public class FunctionDemoEditor : Editor
//{
//		static string[] methods;
//		static string[] parameters;
//		static Dictionary<MethodInfo, ParameterInfo> objectTable = new Dictionary<MethodInfo, ParameterInfo> ();
//		static MethodInfo[] methodsToCall;
//		static string[] ignoreMethods = new string[] { "Start", "Update" };
//	
//		static FunctionDemoEditor ()
//		{
//				
//
//
//				var param =
//			typeof(FunctionDemo)
//				.GetMethods (BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public) // Instance methods, both public and private/protected
//				.Where (x => x.DeclaringType == typeof(FunctionDemo)) // Only list methods defined in our own class
//				//.Where (x => x.GetParameters ().Length > 0) // Make sure we only get methods with zero argumenrts
//				.Where (x => !ignoreMethods.Any (n => n == x.Name)) // Don't list methods in the ignoreMethods array (so we can exclude Unity specific methods, etc.)
//				.Cast<MethodInfo> ()
//				.Select (x => (MethodInfo)x);
//				methodsToCall = param.ToArray ();
//
//				methods =
//			typeof(FunctionDemo)
//				.GetMethods (BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public) // Instance methods, both public and private/protected
//				.Where (x => x.DeclaringType == typeof(FunctionDemo)) // Only list methods defined in our own class
//				//.Where (x => x.GetParameters ().Length == 0) // Make sure we only get methods with zero argumenrts
//				.Where (x => !ignoreMethods.Any (n => n == x.Name)) // Don't list methods in the ignoreMethods array (so we can exclude Unity specific methods, etc.)
//				.Select (x => x.Name)
//				.ToArray ();
//
//
//				for (int i = 0; i < methodsToCall.Length; i++) {
//						Debug.Log (methodsToCall [i].Name);
//						methods [i] += " ( ";
//						foreach (ParameterInfo par in methodsToCall[i].GetParameters()) {
//								//Debug.Log(p.)
//								//foreach (ParameterInfo par in p) {
//								methods [i] += par.ParameterType.Name + ", ";
//								Debug.Log (par.Name + "  " + "   " + par.ParameterType);
//								//}
//						}
//						methods [i] += " )";
//				}
//		
//		}
//	
//	
//	
//		public override void OnInspectorGUI ()
//		{
//				FunctionDemo obj = target as FunctionDemo;
//		
//				if (obj != null) {
//						int index;
//			
//						try {
//								index = methodsToCall
//					.Select ((v, i) => new { Name = v, Index = i })
//						.First (x => x.Name.ToString () == obj.MethodToCall)
//						.Index;
//						} catch {
//								index = 0;
//						}
//			
//						obj.MethodToCall = methodsToCall [EditorGUILayout.Popup (index, methods)].Name.ToString ();
//				}
//		}
//}
