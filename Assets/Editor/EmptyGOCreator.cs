using UnityEngine;
using UnityEditor;

//Put this under a folder called Editor in your project
public class EmptyGOCreator
{
		[MenuItem ("Tools/Create Empty at level %g")]
		static void CreateEmptyGOAtLevel ()
		{
				var go = new GameObject ("GameObject");
				go.transform.parent = Selection.activeGameObject == null ? null : Selection.activeGameObject.transform.parent;
				go.transform.position = Vector3.zero;
		}
	
		[MenuItem ("Tools/Create Empty below level %k")]
		static void CreateEmptyGOAsChild ()
		{
				var go = new GameObject ("GameObject");
				go.transform.parent = Selection.activeGameObject == null ? null : Selection.activeGameObject.transform;
				go.transform.localPosition = Vector3.zero;
		}

		[MenuItem ("Tools/Create Empty below all at level %l")]
		static void CreateEmptyGObjsAsChild ()
		{
				var gObjs = Selection.gameObjects;
				for (int i = 0; i < gObjs.Length; i++) {
						var go = new GameObject ("GameObject");
						go.transform.parent = gObjs [i].transform.parent;
						go.transform.localPosition = Vector3.zero;
						gObjs [i].transform.parent = go.transform;

				}
		}

		[MenuItem ("Tools/remove all but sprite renderers")]
		static void StripToSpriteRenderer ()
		{
				GameObject root;
				if (GameObject.Find ("CenterPoint"))
						root = GameObject.Find ("CenterPoint");
				else {

						root = new GameObject ("CenterPoint");
						root.transform.position = Vector3.zero;
				}
				var gObjs = Selection.gameObjects;
				for (int i = 0; i < gObjs.Length; i++) {
						if (gObjs [i].transform.childCount > 0) {

								for (int j = 0; j < gObjs[i].transform.childCount; j++) {
										if (gObjs [i].transform.GetChild (j).GetComponent<SpriteRenderer> ()) {
												//string name = gObjs[i]
												var go = new GameObject ("clone_" + gObjs [i].transform.GetChild (j).name);
												go.transform.parent = root.transform;
												go.transform.position = gObjs [i].transform.GetChild (j).transform.position;
												go.transform.rotation = gObjs [i].transform.GetChild (j).transform.rotation;
												go.transform.localScale = gObjs [i].transform.GetChild (j).transform.localScale;
												go.AddComponent<SpriteRenderer> ();
												go.GetComponent<SpriteRenderer> ().sprite = gObjs [i].transform.GetChild (j).GetComponent<SpriteRenderer> ().sprite;
												go.GetComponent<SpriteRenderer> ().color = gObjs [i].transform.GetChild (j).GetComponent<SpriteRenderer> ().color;
												GameObject.DestroyImmediate (gObjs [i].transform.GetChild (j).gameObject);
								
												//gObjs [i].transform.parent = go.transform;
					
										}
								}

						}
						if (gObjs [i].GetComponent<SpriteRenderer> ()) {
								var go = new GameObject ("clone_" + gObjs [i].name);
								go.transform.parent = root.transform;
								go.transform.position = gObjs [i].transform.position;
								go.transform.rotation = gObjs [i].transform.rotation;
								go.transform.localScale = gObjs [i].transform.localScale;
								go.AddComponent<SpriteRenderer> ();
								go.GetComponent<SpriteRenderer> ().sprite = gObjs [i].GetComponent<SpriteRenderer> ().sprite;
								go.GetComponent<SpriteRenderer> ().color = gObjs [i].GetComponent<SpriteRenderer> ().color;
								GameObject.DestroyImmediate (gObjs [i]);
						}
			
				}
		}

		[MenuItem ("Tools/Unparent All Selected %j")]
		static void UnParentSelected ()
		{
				var gObjs = Selection.gameObjects;
				for (int i = 0; i < gObjs.Length; i++) {
						if (gObjs [i].transform.childCount > 0) {
								for (int j = 0; j <= gObjs[i].transform.childCount-1; j++) {

										gObjs [i].transform.GetChild (j).gameObject.transform.parent = gObjs [i].transform.root;
								}
						}
			
				}
		}

	
		[MenuItem ("Tools/Delete children of selected")]
		static void DeleteChildrenOfSelected ()
		{
				var gObjs = Selection.gameObjects;
				for (int i = 0; i < gObjs.Length; i++) {
						
						GameObject.DestroyImmediate (gObjs [i].transform.GetChild (0).gameObject);
			
			
				}
		}

		[MenuItem ("Tools/Add Circle Collider to children of selected")]
		static void AddCircleColliderToChildrenOfSelected ()
		{
				var gObjs = Selection.gameObjects;
				for (int i = 0; i < gObjs.Length; i++) {
			
						gObjs [i].transform.GetChild (0).gameObject.AddComponent<CircleCollider2D> ();
			
			
				}
		}

	
	
		[MenuItem ("Tools/Toggle SpriteRenderer on selected objects %u")]
		static void ToggleSpriteRendererOnSelectedObjects ()
		{

				SpriteRenderer spr = Selection.activeGameObject == null ? null : Selection.activeGameObject.GetComponent<SpriteRenderer> ();

		
		
				if (spr.enabled == false)
						spr.enabled = true;
				else
						spr.enabled = false;
				
			

		}

	
	
		[MenuItem ("Tools/Add Box2D Collider to children of selected")]
		static void AddBox2DColliderToChildrenOfSelected ()
		{
				var gObjs = Selection.gameObjects;
				
				for (int i = 0; i < gObjs.Length; i++) {
						
						gObjs [i].transform.GetChild (0).gameObject.AddComponent<EdgeCollider2D> ();
						//gObjs [i].transform.GetChild (0).gameObject.GetComponent<BoxCollider2D> ().size = new Vector2 (0.2f, 0.05f);
						gObjs [i].transform.GetChild (0).gameObject.GetComponent<EdgeCollider2D> ().points = new Vector2[2] {
								new Vector2 (-0.13f, 0),
								new Vector2 (0.13f, 0)
						};

						//new int[5] {1, 2, 3, 4, 5};
			
				}
		}

	
		[MenuItem ("Tools/Add BoxCollider2D to all selected")]
		static void AddBoxCollider2DToSelection ()
		{
				var gObjs = Selection.gameObjects;
				for (int i = 0; i < gObjs.Length; i++) {

						gObjs [i].AddComponent<BoxCollider2D> ();
			
				}
		}
}