﻿using UnityEngine;
using System.Collections;

public class BounceCoroutine : MonoBehaviour
{

		// Use this for initialization

		public float scaleFactor;
		public float duration;
		public float delay;
		public EaseType easer;
		public bool scaleToOrig = true;
		Vector3 origScale;
		

		void Awake ()
		{
				origScale = transform.localScale;
		}

		public void TurnOff ()
		{
//				StartCoroutine (Auto.Wait (0.9f, () => {		
//						StopAllCoroutines ();
//						this.enabled = false;
//				}));
		}

		

		void OnEnable ()
		{
				
				StartCoroutine (Auto.Wait (delay, () => {
						if (scaleToOrig)
								transform.localScale = origScale;
						BounceUp ();
				}));
			
		}

		void OnDisable ()
		{
				StopAllCoroutines ();
		}

		public void Disable ()
		{
				StopAllCoroutines ();
		}

		void BounceUp ()
		{
				StartCoroutine (transform.ScaleTo (origScale * scaleFactor, duration, easer, BounceDown));
		}
		void BounceDown ()
		{
				StartCoroutine (transform.ScaleTo (origScale, duration, easer, BounceUp));
		}

		// Update is called once per frame
		void Update ()
		{
	
		}
}
