﻿using UnityEngine;
using System.Collections;

public class FadeCoroutine : MonoBehaviour
{
	
		// Use this for initialization
	
		
		public float duration;
		public float delay;
		public float target = 0;
		float origAlpha;


		SpriteRenderer spr;
	
		void Awake ()
		{
				spr = GetComponent<SpriteRenderer> ();
				origAlpha = spr.color.a;

			

		}

		void OnDisable ()
		{
				StopAllCoroutines ();
				if (G.i != null) {
						G.i.OnPlayerDeath -= HandleOnPlayerDeath;
				}
		}

		void HandleOnPlayerDeath ()
		{
				StopAllCoroutines ();		
		}

	
		void Start ()
		{
		
		}
	
		void OnEnable ()
		{
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
				StartCoroutine (Auto.Wait (delay, () => {
			
						BounceUp ();
						//StartCoroutine (transform.ScaleTo (transform.localScale * scaleFactor, duration, easer, BounceDown));
				}));
		}
	
		void BounceUp ()
		{
				StartCoroutine (spr.FadeSprite (origAlpha, target, duration, 0, BounceDown));
		}
		void BounceDown ()
		{
				StartCoroutine (spr.FadeSprite (spr.color.a, origAlpha, duration, 0, BounceUp));
		}
	
		// Update is called once per frame
		void Update ()
		{
		
		}
}
