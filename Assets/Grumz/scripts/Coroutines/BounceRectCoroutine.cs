﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class BounceRectCoroutine : MonoBehaviour
{

		// Use this for initialization

		public float scaleFactor;
		public float duration;
		public float delay;
		public EaseType easer;
		Vector3 origScale;
		RectTransform trans;


		void Awake ()
		{
				origScale = Vector3.one;
				trans = GetComponent<RectTransform> ();
		}

		void Start ()
		{
				
		}

		void OnEnable ()
		{
				trans.localScale = origScale;
				StartCoroutine (Auto.Wait (delay, () => {

						
						StartCoroutine (trans.ScaleTo (trans.localScale * scaleFactor, duration, easer, BounceDown));
				}));
		}

		void BounceUp ()
		{
				StartCoroutine (trans.ScaleTo (trans.localScale * scaleFactor, duration, easer, BounceDown));
		}
		void BounceDown ()
		{
				StartCoroutine (trans.ScaleTo (origScale, duration, easer, BounceUp));
		}

		// Update is called once per frame
		void Update ()
		{
	
		}
}
