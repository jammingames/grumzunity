﻿using UnityEngine;
using System.Collections;

public class RepositionWithinScreenEdges : MonoBehaviour
{



		float leftEdge, rightEdge, botEdge, topEdge;
		Vector3 pos;
		public float speed = 3f;
		// Use this for initialization
		void Start ()
		{
				//yield return new WaitForEndOfFrame ();
				leftEdge = G.i.LEFT_SCREEN_EDGE;
				rightEdge = G.i.RIGHT_SCREEN_EDGE;
				botEdge = G.i.BOT_SCREEN_EDGE;
				topEdge = G.i.TOP_SCREEN_EDGE;
			
		}
	
		// Update is called once per frame
		void Update ()
		{
				pos = transform.position;
//				
//				if (pos.x < 0 && pos.y < (topEdge - 0.1f) && pos.y > (botEdge + 0.1f))
//						pos.x = leftEdge;
//				else if (pos.x > 0 && pos.y < (topEdge - 0.1f) && pos.y > (botEdge + 0.1f))
//						pos.x = rightEdge;
//				if (pos.y < 0 && pos.x > leftEdge && pos.x < rightEdge)
//						pos.y = botEdge;
//				else if (pos.y > 0 && pos.x > leftEdge && pos.x < rightEdge)
//						pos.y = topEdge;
				pos += transform.forward * speed;
				pos.x = Mathf.Clamp (pos.x, leftEdge, rightEdge);
				pos.y = Mathf.Clamp (pos.y, botEdge, topEdge);
				
				transform.position = pos;
		}
}
