﻿using UnityEngine;
using System.Collections;

public class SpawnAtPosition : MonoBehaviour
{

		public GameObject prefab;
		public Transform spawnPos;
		public Transform parent;
		public Vector3 offset;
		public Vector3 rotation;

		// Use this for initialization
		void Start ()
		{
				
		}

	
		public void SpawnPrefab ()
		{

				Vector3 pos;
				if (spawnPos != null) {
						pos = spawnPos.localPosition + offset;
			
			
				} else {
			
						pos = transform.localPosition + offset;
				}
				//var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				if (parent != null)
						go.transform.parent = parent;
		}


		public void SpawnPrefab (GameObject prefab, float scaleFactor)
		{
				//				if (prefab == null)
				//						prefab = UIManager.instance.characterPrefabs [UIManager.instance.spriteIndex];
				Vector3 pos;
				if (spawnPos != null) {
						pos = spawnPos.localPosition + offset;
			
			
				} else {
			
						pos = transform.localPosition + offset;
				}
				//var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				go.transform.localScale *= scaleFactor;
		
				if (parent != null)
						go.transform.parent = parent;
		}

	
		public void SpawnPrefab (GameObject prefab)
		{
//				if (prefab == null)
//						prefab = UIManager.instance.characterPrefabs [UIManager.instance.spriteIndex];
				Vector3 pos;
				if (spawnPos != null) {
						pos = spawnPos.localPosition + offset;
			
			
				} else {
			
						pos = transform.localPosition + offset;
				}
				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				//var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				
				if (parent != null)
						go.transform.parent = parent;
		}

		public void SpawnPrefab (GameObject prefab, Vector3 pos)
		{

				pos += offset;
			
				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				//var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				
				if (parent != null)
						go.transform.parent = parent;
		}	

		public void SpawnPrefab (GameObject prefab, Vector3 pos, float scaleFactor)
		{
		
				pos += offset;
		
				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				//var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				go.transform.localScale *= scaleFactor;
				if (parent != null)
						go.transform.parent = parent;
		}	


		
		// Update is called once per frame
		void Update ()
		{
	
		}
}
