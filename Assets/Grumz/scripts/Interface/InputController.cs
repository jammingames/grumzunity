﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



public class InputController : MonoBehaviour, IRequireUserInput
{
	
	
		public IUserInputProxy InputProxy { get; set; }
		List<IActorPhysicsProxy> actors = new List<IActorPhysicsProxy> ();
		IActorPhysicsProxy actor;

		bool mouseDown;

		private void Awake ()
		{
//				actors = (IActorPhysicsProxy)gameObject.GetComponents (typeof(IActorPhysicsProxy));
				var components = GetComponents<MonoBehaviour> ().Where (c => c is IActorPhysicsProxy).Cast<IActorPhysicsProxy> ();
				foreach (var component in components) {
						actors.Add (component);
				}

				
		}

		void Update ()
		{
				if (InputProxy == null)
						return;
				if (!mouseDown)
						mouseDown = Input.anyKey;
		
		
				for (int i = 0; i < actors.Count; i++) {
						actors [i].HandleMouseInput (mouseDown);
				}
				mouseDown = false;
			
			
		}
	
		private void FixedUpdate ()
		{
				
		}
}