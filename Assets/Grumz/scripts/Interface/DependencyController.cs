﻿using UnityEngine;
using System.Linq;
using System.Collections;

public class DependencyController : MonoBehaviour
{

		public GameObject[] actors;

		// Use this for initialization
		void Start ()
		{
				InitializeUserInput ();
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		void InitializeUserInput ()
		{
				IUserInputProxy input = new RealInput ();
				for (int i = 0; i < actors.Length; i++) {

						var components = actors [i].GetComponents<MonoBehaviour> ().Where (c => c is IRequireUserInput).Cast<IRequireUserInput> ();
						foreach (var component in components)
								component.InputProxy = input;
				}
		}

}
