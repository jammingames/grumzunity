﻿using UnityEngine;
using System.Collections;

public interface IRequireActorPhysics
{
		IActorPhysicsProxy ActorProxy { get; set; }
}