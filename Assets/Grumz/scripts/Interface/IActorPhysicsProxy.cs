﻿using UnityEngine;
using System.Collections;

public interface IActorPhysicsProxy
{
		void HandleInput (float horizontal, float button, bool isDown);
		void HandleMouseInput (bool isDown);
		
}