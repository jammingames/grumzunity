﻿using System.Linq;
using UnityEngine;
using System.Collections;

class RealActorPhysics : IActorPhysicsProxy, IRequireUserInput
{

		public IUserInputProxy InputProxy { get; set; }
		public void HandleInput (float horizontal, float button, bool isDown)
		{
		}
		public void HandleMouseInput (bool isDown)
		{
		}

}