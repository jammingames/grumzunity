﻿using UnityEngine;
using System.Collections;

public class DelayAudioOnStart : MonoBehaviour
{

		public float duration;


		void OnEnable ()
		{
//				AudioSource audio = GetComponent<AudioSource> ();
//				AudioManager.instance.musicAudio = audio;
//
//				//debug.log (AudioManager.instance.musicAudio);
		}

		// Use this for initialization
		void Start ()
		{
				
				AudioSource audio = GetComponent<AudioSource> ();
				
				audio.enabled = false;
				StartCoroutine (Auto.Wait (duration, () => {
						audio.enabled = true;
				}));
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
