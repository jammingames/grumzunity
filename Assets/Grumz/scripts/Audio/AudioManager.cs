﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
		private string _PLAYFX = "canPlayFX";
		private string _PLAYMUSIC = "canPlayMusic";
		private string SOUNDVOL = "sfxVol";
		private string MUSICVOL = "musicVol";	


		public bool canPlay = true;
		public bool canPlayFX, canPlayMusic;
		
		public AudioMixer mixer;
		public AudioMixerSnapshot mutedSnap, normalSnap;
		public Toggle fxToggle, musicToggle;

		public AudioSource g1Death;

		float origMusicVol, origSfxVol;

		float origVolume;
		//bool canPlayMusic = true;


		

		public void PlayAudio (AudioSource clip)
		{
				if (canPlay == true && clip.clip != null)
						clip.Play ();
		}

		public void PlayAudio (AudioClip clip)
		{
				if (canPlay == true && clip != null)
						AudioSource.PlayClipAtPoint (clip, transform.position);
		}


		void DoThing (bool test)
		{
				TogglePlayFX ();
		}

		void Awake ()
		{
				//mutedSnap.TransitionTo (0.1f);
				
				
				
				if (fxToggle != null && musicToggle != null) {

						if (PlayerPrefs.HasKey (_PLAYFX)) {
								canPlayFX = (PlayerPrefs.GetInt (_PLAYFX) > 0) ? true : false;
								fxToggle.isOn = !canPlayFX;
						} else {
								canPlayFX = true;	
								fxToggle.isOn = !canPlayFX;
								PlayerPrefs.SetInt (_PLAYFX, 1);
						}
						if (PlayerPrefs.HasKey (_PLAYMUSIC)) {
								canPlayMusic = (PlayerPrefs.GetInt (_PLAYMUSIC) > 0) ? true : false;
								musicToggle.isOn = !canPlayMusic;
						} else {
								canPlayMusic = true;
								musicToggle.isOn = false;
								PlayerPrefs.SetInt (_PLAYMUSIC, 1);
						}
						mixer.GetFloat (SOUNDVOL, out origSfxVol);
						mixer.GetFloat (MUSICVOL, out origMusicVol);
				}
				
				if (origSfxVol < -20)
						origSfxVol = 0;
				if (origMusicVol < -20)
						origMusicVol = 0.26f;

				
			

				if (!_instance)		
						_instance = this;
				//canPlay = true;


		}

		void Start ()
		{
				if (mixer != null) {

						normalSnap.TransitionTo (1.45f);
				}
		}



		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.M)) {
						TogglePlayFX ();
						TogglePlayMusic ();
				}
		}

		public void TogglePlayFX ()
		{
				if (canPlayFX) {
						canPlayFX = false;
						PlayerPrefs.SetInt (_PLAYFX, 0);
						mixer.SetFloat (SOUNDVOL, -80);
				} else {
						canPlayFX = true;
						PlayerPrefs.SetInt (_PLAYFX, 1);
						mixer.SetFloat (SOUNDVOL, origSfxVol);
				}
				//		canPlay = !canPlay;
		}
		
		public void TogglePlayMusic ()
		{
				if (canPlayMusic) {
						canPlayMusic = false;
						PlayerPrefs.SetInt (_PLAYMUSIC, 0);
						mixer.SetFloat (MUSICVOL, -80);
				} else {
						canPlayMusic = true;
						PlayerPrefs.SetInt (_PLAYMUSIC, 1);
						mixer.SetFloat (MUSICVOL, origMusicVol);
				}
		}

		public void TurnOffMusic ()
		{
				canPlayMusic = false;
				PlayerPrefs.SetInt (_PLAYMUSIC, 0);
				mixer.SetFloat (MUSICVOL, -80);
				musicToggle.isOn = !canPlayMusic;
		}
	
	
		static AudioManager _instance = null;
		public static AudioManager instance {
				get {
						if (!_instance) {
								// check if an AudioManager is already available in the scene graph
								_instance = FindObjectOfType (typeof(AudioManager)) as AudioManager;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("AudioManager");
										_instance = obj.AddComponent<AudioManager> ();
								}
						}

						return _instance;
				}
		}


		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}
}
