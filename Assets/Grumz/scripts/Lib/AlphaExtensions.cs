﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
		static Fader _instance = null;

		public static Fader instance {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(Fader)) as Fader;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("Fader");
										_instance = obj.AddComponent<Fader> ();
								}
						}

						return _instance;
				}
		}

		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}

		public void FadeInOut (Material fadeObj)
		{
				StartCoroutine (fadeObj.Fade (1.0f, 0.2f, 0.6f, 
						() => {
				
						StartCoroutine (fadeObj.Fade (0.1f, 1.0f, 0.6f, 
										() => {
								//	Debug.Log ("fade is finished");
								//Do things when fade is done
						}));
				}));
		}


		public void FadeColorIn (Material fadeObj, float target, float duration, Action onComplete)
		{
				StartCoroutine (fadeObj.FadeColor (1, target, duration, onComplete));
		}

	
		public void FadeColorOut (Material fadeObj, float target, float duration, Action onComplete)
		{
				StartCoroutine (fadeObj.FadeColor (fadeObj.color.a, target, duration, onComplete));
		}


		public void FadeAlphaIn (Material fadeObj, float duration, Action onComplete)
		{
				StartCoroutine (fadeObj.FadeAlpha (0, 1f, duration, onComplete));
		}
		public void FadeAlphaIn (Material fadeObj, float target, float duration, Action onComplete)
		{
				StartCoroutine (fadeObj.FadeAlpha (fadeObj.color.a, target, duration, onComplete));
		}

		public void FadeAlphaOut (Material fadeObj, float duration, Action onComplete)
		{
				StartCoroutine (fadeObj.FadeAlpha (1, 0f, duration, onComplete));
		}
	
		public void FadeAlphaOut (Material fadeObj, float target, float duration, Action onComplete)
		{
				StartCoroutine (fadeObj.FadeAlpha (fadeObj.color.a, target, duration, onComplete));
		}
}

public static class AlphaExtensions
{
		public  static IEnumerator Fade (
				this Material mat,
				float start,
				float target,
				float duration,
				System.Action onComplete)
		{
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						mat.SetFloat ("_Cutoff", temp);	
						yield return 0;
				}
				
				if (onComplete != null)
						onComplete ();
				
		}


	
		public  static IEnumerator ChangeMinX (
		this Material mat,
		float start,
		float target,
		float duration,
		System.Action onComplete)
		{
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						mat.SetFloat ("_MinX", temp);	
						yield return 0;
				}
		
				if (onComplete != null)
						onComplete ();
		
		}


		public  static IEnumerator ChangeFill (

		this Material mat,
		float start,
		float target,
		float duration,
		System.Action onComplete)
		{
				//	Debug.Log ("Change Fill");
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						mat.SetFloat ("_Fill", temp);	
						yield return 0;
				}
		
				if (onComplete != null)
						onComplete ();
		
		}
	
	
		public  static IEnumerator SpeedUp (
		this Animator mat,
		float target,
		float duration,
		System.Action onComplete)
		{
				float start = mat.speed;
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						mat.speed = temp;
						yield return 0;
				}
		
				if (onComplete != null)
						onComplete ();
		
		}
	

	
	
		public  static IEnumerator FadeTextAlpha (
		this Text mat,
		float start,
		float target,
		float duration,
		System.Action onComplete)
		{
				Color col = mat.color;
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						col.a = temp;
						mat.color = col;
						yield return 0;
				}
		
				if (onComplete != null)
						onComplete ();
		
		}

	
		public  static IEnumerator LerpImageColor (
		this Graphic mat,
		Color start,
		Color target,
		float duration,
		System.Action onComplete)
		{
				Color col = mat.color;
				float elapsed = 0;
				Color temp = start;
				Color range = target - temp;
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						col = temp;
						mat.color = col;
						yield return 0;
				}
		
				if (onComplete != null)
						onComplete ();
		
		}

	
		public  static IEnumerator FadeImageAlpha (
		this Graphic mat,
		float start,
		float target,
		float duration,
		System.Action onComplete)
		{
				Color col = mat.color;
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						col.a = temp;
						mat.color = col;
						yield return 0;
				}
		
				if (onComplete != null)
						onComplete ();
		
		}


		public  static IEnumerator FadeAlpha (
				this Material mat,
				float start,
				float target,
				float duration,
				System.Action onComplete)
		{
				Color col = mat.color;
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						col.a = temp;
						mat.color = col;
						yield return 0;
				}
				
				if (onComplete != null)
						onComplete ();
				
		}


	
		public  static IEnumerator FadeColor (
		this Material mat,
		float start,
		float target,
		float duration,
		System.Action onComplete)
		{
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						mat.SetFloat ("_GrayScale", temp);	
						yield return 0;
				}
		
				if (onComplete != null)
						onComplete ();
		
		}



		public  static IEnumerator FadeSprite (
		this SpriteRenderer spr,
	
		float target,
		float duration,
		float delay,
		System.Action onComplete)
		{
				string nom = spr.name;
				//Debug.Log ("starting fade on: " + spr.name);
				float start = spr.color.a;
				Color col = spr.color;
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				yield return new WaitForSeconds (delay);
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						col.a = temp;
						if (spr == null)
								Debug.Log ("null on " + nom);
//						Debug.Log (spr.name);
						spr.color = col;
						yield return 0;
				}
//				Debug.Log ("ending fade on: " + spr.name);
				if (onComplete != null)
						onComplete ();
		
		}

	
		public  static IEnumerator FadeSprite (
		this SpriteRenderer spr,
		float start,
		float target,
		float duration,
		float delay,
		System.Action onComplete)
		{
				Color col = spr.color;
				float elapsed = 0;
				float temp = start;
				float range = target - temp;
				yield return new WaitForSeconds (delay);
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp = start + range * (elapsed / duration);
						col.a = temp;
						spr.color = col;
						yield return 0;
				}
		
				if (onComplete != null)
						onComplete ();
		
		}
	
	
	
		public  static IEnumerator FadeSpriteColor (
		this SpriteRenderer spr,
		Color start,
		Color target,
		float duration,
		float delay,
		System.Action onComplete)
		{
				Color col = spr.color;
				float elapsed = 0;
				Color temp = start;
				float rangeR = target.r - temp.r;
				float rangeG = target.g - temp.g;
				float rangeB = target.b - temp.b;
				float rangeA = target.a - temp.a;
				yield return new WaitForSeconds (delay);
				while (elapsed < duration) {
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						temp.r = start.r + rangeR * (elapsed / duration);
						temp.g = start.g + rangeG * (elapsed / duration);
						temp.b = start.b + rangeB * (elapsed / duration);
						temp.a = start.a + rangeA * (elapsed / duration);
						col.r = temp.r;
						col.g = temp.g;
						col.b = temp.b;
						col.a = temp.a;
						spr.color = col;
						yield return 0;
				}
		
				if (onComplete != null)
						onComplete ();
		
		}
	

}
