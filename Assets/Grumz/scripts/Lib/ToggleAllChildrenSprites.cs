﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ToggleAllChildrenSprites : MonoBehaviour
{
		public float delay;
		List<GameObject> children;

		void Awake ()
		{
				
				children = new List<GameObject> ();
				SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer> ();
				for (int i = 0; i <= sprites.Length-1; i++) {
						children.Add (sprites [i].gameObject);
				}
		}
		// Use this for initialization
		void Start ()
		{
				StartCoroutine (Auto.Wait (delay, () =>
				{
						StartCoroutine (ToggleChildren ());
				}));

		}


		IEnumerator ToggleChildren ()
		{
				for (int i = 0; i <= children.Count-1; i++) {
//						print ("turning off: " + children [i].name);
						children [i].gameObject.SetActive (false);
				}
				yield return new WaitForEndOfFrame ();
				for (int i = 0; i <=children.Count-1; i++) {
//						print ("turning on: " + children [i].name);
						children [i].gameObject.SetActive (true);
				}
		}
}
