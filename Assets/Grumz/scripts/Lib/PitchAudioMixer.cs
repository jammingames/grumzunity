﻿using UnityEngine;
using System.Collections;

public class PitchAudioMixer : MonoBehaviour
{
		public GameState pitchDownState, pitchUpState;
		AudioSource source;
		public float pitch1, pitch2;
		public float duration;
		public EaseType easer;

		// Use this for initialization
		void Start ()
		{
				source = GetComponent<AudioSource> ();
				G.i.OnStateChange += HandleOnStateChange;
				StartCoroutine (source.ChangePitch (source.pitch, pitch1, duration, easer, null));
		}

		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == pitchDownState) {

						StartCoroutine (source.ChangePitch (source.pitch, pitch1, duration, easer, null));
				} else if (nextState == pitchUpState) {

						StartCoroutine (source.ChangePitch (source.pitch, pitch2, duration, easer, null));
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.LeftArrow))
						StartCoroutine (source.ChangePitch (source.pitch, pitch1, duration, easer, null));
				if (Input.GetKeyDown (KeyCode.RightArrow))
						StartCoroutine (source.ChangePitch (source.pitch, pitch2, duration, easer, null));
		}
}
