﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class PitchAudioCoroutine : MonoBehaviour
{
		public GameState pitchIntroState, pitchDownState, pitchUpState;
		AudioSource source;
		public AudioMixer mixer;
		public float pitch1, pitch2, pitchIntro;
		public float duration, durationDown;

		public float cutOffDown, cutOffUp;
		public EaseType easer, easerDown;
		

		// Use this for initialization
		void Start ()
		{

				
				source = GetComponent<AudioSource> ();
				G.i.OnStateChange += HandleOnStateChange;
				//StartCoroutine (source.ChangePitch (source.pitch, pitchIntro, duration, easer, null));
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
		}

		void HandleOnPlayerDeath ()
		{
//				StartCoroutine (source.ChangePitch (source.pitch, pitch1, duration * 2.5f, easer, () => {
//						StartCoroutine (source.ChangePitch (source.pitch, pitch2, duration, easer, null));
//				}));
				float current;
				mixer.GetFloat ("cutOffFreq", out current);
				StartCoroutine (mixer.ChangeAudioParam ("cutOffFreq", current, cutOffDown, durationDown, easerDown, () => {
//						StartCoroutine (Auto.Wait (1.5f, () => {  
//								StartCoroutine (mixer.ChangeAudioParam ("cutOffFreq", cutOffDown, cutOffUp, 2, EaseType.SmoothStepInOut, null));
						//}));
				}));
		}

		public void PitchDownThenUp ()
		{
//				StartCoroutine (source.ChangePitch (source.pitch, pitch1, duration * 2f, easer, () => {
//						StartCoroutine (source.ChangePitch (source.pitch, pitch2, duration, easer, null));
//				}));
		}



		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == pitchDownState) {
						float current;
						mixer.GetFloat ("cutOffFreq", out current);
						StartCoroutine (mixer.ChangeAudioParam ("cutOffFreq", current, cutOffDown, durationDown, easerDown, null));
//					
//						StartCoroutine (source.ChangePitch (source.pitch, pitch1, duration, easer, () => {
//								StartCoroutine (source.ChangePitch (source.pitch, pitch2, duration / 2, easer, null));
//						}));
				} else if (nextState == pitchUpState) {
						float current;
						mixer.GetFloat ("cutOffFreq", out current);
						StartCoroutine (mixer.ChangeAudioParam ("cutOffFreq", current, cutOffUp, duration, easer, null));
//						StartCoroutine (source.ChangePitch (source.pitch, pitch1, duration, easer, () => {
//								StartCoroutine (source.ChangePitch (source.pitch, pitch2, duration / 2, easer, null));
//						}));
				} else if (nextState == pitchIntroState) {
						//	StartCoroutine (source.ChangePitch (source.pitch, pitchIntro, duration, easer, null));
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.LeftArrow))
						HandleOnPlayerDeath ();
				//StartCoroutine (source.ChangePitch (source.pitch, pitch1, duration, easer, null));
				if (Input.GetKeyDown (KeyCode.RightArrow)) {
						float current;
						mixer.GetFloat ("cutOffFreq", out current);
						StartCoroutine (mixer.ChangeAudioParam ("cutOffFreq", current, cutOffUp, duration, easer, null));
				}
		}
}
