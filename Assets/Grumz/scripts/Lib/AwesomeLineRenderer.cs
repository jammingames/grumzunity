﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class LineParams
{
		public List<Vector3> vertPositions;
		public LineRenderer line;
		public Color startColor, endColor;
		public float startWidth, endWidth;
		public Material mat;
		public LineParams (Material _mat, Color _startCol, Color _endCol, float _width, List<Vector3> vertPos)
		{
				mat = _mat;
				startColor = _startCol;
				endColor = _endCol;
				startWidth = _width;
				endWidth = _width;

				vertPositions = vertPos;
		}
}

public class AwesomeLineRenderer : MonoBehaviour
{
		public List<Vector3> vertPositions;
		public LineRenderer line;
		Color startColor, endColor;
		float startWidth, endWidth;
		Material mat;
		
		


		public AwesomeLineRenderer (LineRenderer newLine)
		{
				line = newLine;
				vertPositions = new List<Vector3> ();
		}

		public AwesomeLineRenderer (Transform parent, int index)
		{
				
				
		}

		void Awake ()
		{
				gameObject.AddComponent<MaterialSort> ();
				vertPositions = new List<Vector3> ();
				line = GetComponent (typeof(LineRenderer)) as LineRenderer;
				if (line == null) {
						line = gameObject.AddComponent (typeof(LineRenderer)) as LineRenderer;
				}		
				
		}

		public void CreateNew (Material mat, Color startCol, Color endCol, float width, float widthEnd, Vector3 startPos, Vector3 endPos)
		{
				SetMaterial (mat);			
				SetColors (startCol, endCol);
				line.useWorldSpace = false;
				SetWidth (width, widthEnd);
				line.SetVertexCount (2);
				line.SetPosition (0, startPos);
				vertPositions.Add (startPos);
				vertPositions.Add (endPos);
				line.SetPosition (1, endPos);
		}

		public LineParams SaveLine (Material _mat, Color _startCol, Color _endCol, float _width, List<Vector3> vertPos)
		{
				return new LineParams (_mat, _startCol, _endCol, _width, vertPos);
		}

		public LineParams SaveLine ()
		{
				return new LineParams (mat, startColor, endColor, startWidth, vertPositions);
		}


		public void RecreateLine (LineParams lineSave)
		{
				GameObject canvas = new GameObject ("lineClone"); 
				canvas.transform.parent = transform;
				canvas.transform.rotation = transform.rotation;	
				var newLine = (LineRenderer)canvas.AddComponent<LineRenderer> ();
				newLine.material = new Material (lineSave.mat);
				newLine.SetWidth (lineSave.startWidth, lineSave.endWidth);
				newLine.SetVertexCount (lineSave.vertPositions.Count + 1);
				newLine.SetColors (lineSave.startColor, lineSave.endColor);
				for (int i = 0; i < vertPositions.Count; i++) {
						newLine.SetPosition (i, lineSave.vertPositions [i]);
				}
		}

		public void RecreateLine ()
		{
				GameObject canvas = new GameObject ("lineClone"); 
				canvas.transform.parent = transform;
				canvas.transform.rotation = transform.rotation;	
				var newLine = (LineRenderer)canvas.AddComponent<LineRenderer> ();
				newLine.material = new Material (mat);
				newLine.SetWidth (startWidth, endWidth);
				newLine.SetVertexCount (vertPositions.Count + 1);
				newLine.SetColors (startColor, endColor);
				for (int i = 0; i < vertPositions.Count; i++) {
						newLine.SetPosition (i, vertPositions [i]);
				}
		}

		public void SetMaterial (Material _mat)
		{
				mat = _mat;
				line.material = new Material (mat);	
		}
		
		public Material GetMaterial ()
		{
				return mat;
		}


		public void SetWidth (float start, float end)
		{
				line.SetWidth (start, end);
				startWidth = start;
				endWidth = end;
		}
		
		public float GetStartWidth ()
		{
				return startWidth;
		}
		public float GetEndWidth ()
		{
				return endWidth;
		}

		public void SetPosition (int index, Vector3 position)
		{						
				vertPositions [index - 1] = position;
				line.SetPosition (index, position);
		}

		public void SetPosition (LineRenderer newLine, int index, Vector3 position)
		{						
				vertPositions [index - 1] = position;
				newLine.SetPosition (index, position);
		}

		public void SetColors (Color start, Color end)
		{
				startColor = start;
				endColor = end;
				line.SetColors (start, end);
		}

		public void AddToLine (Vector3 newPos)
		{	
				vertPositions.Add (newPos);
				line.SetVertexCount (vertPositions.Count + 1);
				SetPosition (vertPositions.Count, newPos);	
		}

		public int GetVertexCount ()
		{
				return vertPositions.Count;
		}

		public Vector3 GetPosition (int index)
		{
				return vertPositions [index];
		}

		public Color GetColor (int index)
		{
				if (index == 0)
						return startColor;
				else {
						return endColor;
				}
		}
}
