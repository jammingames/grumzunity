﻿using UnityEngine;
using System.Collections;

public class RigidBodyTowardsTransform : MonoBehaviour
{
	
		//public Transform target;
		public float force;
		public bool moveAway = false;
	
	
		public Rigidbody2D _target;
		public Rigidbody2D target { get { return target; } set { _target = value; } }
	
		public float targetDistance = 4.5f;
		//	public float minDist = 1.2f;
		public float maxVelocityChange = 10.0f;
		public float maxSpeed = 20.0f;
		float distance;
//		bool fwdMove = true;


//		Rigidbody2D rb;
//		Quaternion childOrigRotation;
		public Transform glowSprite;
		bool hasSetScale = false;
		ScaleTowards scaler;

		SpriteRenderer spr;


		void Awake ()
		{
				//		rb = GetComponent<Rigidbody2D> ();
				spr = GetComponentInChildren<SpriteRenderer> ();
				//	childOrigRotation = transform.GetChild (0).localRotation;

			

				
				
		}


		void Start ()
		{

				
				if (_target == null)
						_target = G.i.playerObj.GetComponent<Rigidbody2D> ();

		}

		public void HitPlayer ()
		{
				StartCoroutine (spr.FadeSprite (0, 0.15f, 0, null));
		}
		
	
		void Update ()
		{	
				if (_target) {
//						var dir = _target.transform.position - transform.position;
//						var angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
						distance = Vector2.Distance (transform.position, _target.position);
						
						////debug.log (transform.rotation.eulerAngles);
						if (distance < targetDistance) {
//								if (GetComponentInChildren<AngularDisplacementRotation> ().enabled) {
//										GetComponentInChildren<AngularDisplacementRotation> ().enabled = false;
								//	StartCoroutine (transform.GetChild (0).RotateTo (childOrigRotation, 0.5f, null));

								if (glowSprite != null && !hasSetScale) {
										if (scaler != null)
												scaler.enabled = true;
										hasSetScale = true;
										if (glowSprite.GetComponent<BounceCoroutine> ())
												glowSprite.GetComponent<BounceCoroutine> ().enabled = false;
										if (scaler == null) {

												if (glowSprite.transform.GetComponent<ScaleTowards> ())
														scaler = glowSprite.gameObject.GetComponent<ScaleTowards> ();
												else
														scaler = glowSprite.gameObject.AddComponent<ScaleTowards> ();
										}
										scaler.SetScaleParams (Vector3.one, Vector3.one * 3, Vector3.one, 0.2f);
										scaler.ChangeScale (new Vector3 (2.5f, 2.5f, 2.5f), 0.3f);
										StartCoroutine (transform.MoveToTransform (_target.transform, 0.3f, EaseType.SmoothStepInOut, () => {
												Destroy (gameObject);
										}));
										enabled = false;
								}

								

//								
//								if (transform.rotation != targetRotation) {
//
//
//										currentLerpTime = Mathf.MoveTowards (currentLerpTime, duration, Time.deltaTime);
//										if (currentLerpTime > duration) {
//												currentLerpTime = duration;
//										}
//										t = currentLerpTime / duration;
//
//										transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, t / 2);
//										//transform.rotation = targetRotation
//										lastTarget = targetRotation;
//								} else if (currentLerpTime >= duration) {
//										currentLerpTime = 0; 
//										t = 0;
//								}
						} else if (distance > targetDistance) {
								hasSetScale = false;
								if (scaler != null)
										scaler.enabled = false;
								if (GetComponentInChildren<AngularDisplacementRotation> ()) {
										if (!GetComponentInChildren<AngularDisplacementRotation> ().enabled)
												GetComponentInChildren<AngularDisplacementRotation> ().enabled = true;
								}
						}
						
				}
				
				
		}
//	
//		void FixedUpdate ()
//		{
//				if (_target) {
//						
//						if (distance < targetDistance) {
//								Vector2 dir = _target.transform.position - transform.position;
//								//transform.LookAt (_target.transform);
//								Vector2 targetVelocity = dir;
//								//targetVelocity = transform.TransformDirection (targetVelocity);
//								targetVelocity *= force;
//								
//								// Apply a force that attempts to reach our target velocity
//								Vector2 velocity = rb.velocity;
//								Vector2 velocityChange = (targetVelocity - velocity);
//								velocityChange.x = Mathf.Clamp (velocityChange.x, -maxVelocityChange, maxVelocityChange);
//								velocityChange.y = Mathf.Clamp (velocityChange.y, -maxVelocityChange, maxVelocityChange);
//								
//								rb.AddForce (velocityChange);
//								velocity = rb.velocity;
//								velocity.x = Mathf.Clamp (velocity.x, -maxSpeed, maxSpeed);
//								velocity.y = Mathf.Clamp (velocity.y, -maxSpeed, maxSpeed);
//								rb.velocity = velocity;
//						}
//			
//			
//				}
		//}
	
		//
		//		void Move ()
		//		{
		//				targetVelocity = transform.TransformDirection (targetVelocity);
		//				targetVelocity *= speed;
		//		
		//				// Apply a force that attempts to reach our target velocity
		//				Vector3 velocity = rigidbody.velocity;
		//				Vector3 velocityChange = (targetVelocity - velocity);
		//				velocityChange.x = Mathf.Clamp (velocityChange.x, -maxVelocityChange, maxVelocityChange);
		//				velocityChange.y = Mathf.Clamp (velocityChange.y, -maxVelocityChange, maxVelocityChange);
		//				velocityChange.z = 0;
		//				rigidbody.AddForce (velocityChange, ForceMode.VelocityChange);
		//
		//		}
}