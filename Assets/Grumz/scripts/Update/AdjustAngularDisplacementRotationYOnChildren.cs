﻿using UnityEngine;
using System.Collections;

public class AdjustAngularDisplacementRotationYOnChildren : MonoBehaviour
{

		public AngularDisplacementRotationY[] rotators;
		public float speed = 10; 
		float lastSpeed;

		// Use this for initialization
		void Start ()
		{
				lastSpeed = speed;
				rotators = GetComponentsInChildren<AngularDisplacementRotationY> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (speed != lastSpeed) {
						//		//debug.log ("adjusting speed");
						for (int i = 0; i < rotators.Length; i++) {
								rotators [i].speed = speed;
						}
						lastSpeed = speed;
				}
		}
}
