﻿using UnityEngine;
using System.Collections;

public class AngularDisplacementRotationY : MonoBehaviour
{
	
		//float distanceTravelled = 0;
		public float distancePerFrame = 0;
		public float speed = 10;
		Vector3 lastPosition;
		public float circumference;
		public bool randomize;
		public bool parent;
		public Transform target;
		public bool isY;
		public bool canMove = true;


	
		void Start ()
		{
				G.i.OnPlayerDeath += this.HandleOnPlayerDeath;
				
				lastPosition = target.position;
				
				
		}

		void HandleOnPlayerDeath ()
		{
				canMove = false;
				this.enabled = false;
		}
	
		void Update ()
		{
				if (!canMove)
						return;
				
						
				distancePerFrame = (target.position.y - lastPosition.y) + 0.1f;
				lastPosition = target.position;
				
		
				//float 
				float rotation = Mathf.Abs (distancePerFrame) / circumference;
//				if (distancePerFrame < 0)
//						rotation *= -1;
				if (randomize)
						rotation *= Random.Range (0.1f, rotation * 2);
				//				//debug.log (rotation);
				if (rotation > 0)
						transform.Rotate (Vector3.forward, -rotation * speed);
				else if (rotation < 0)
						transform.Rotate (-Vector3.forward, -rotation * speed);
		}
}
