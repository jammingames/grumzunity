﻿using UnityEngine;
using System.Collections;

public class RotateForever : MonoBehaviour
{
	
		//float distanceTravelled = 0;
		public float rotation;
		public float speed = 10;


		public bool randomize;
	
		void Start ()
		{
	
		}
	
		void Update ()
		{
				//		distanceTravelled += Vector3.Distance (transform.position, lastPosition);
				//	circumference = 2 * Mathf.PI * transform.parent.collider2D.bounds.extents.y;
	
		
				//float 
	
				if (randomize)
						rotation *= Random.Range (0.1f, rotation * 2);
				//				//debug.log (rotation);
				if (rotation > 0)
						transform.Rotate (Vector3.forward, -rotation * speed);
				else if (rotation < 0)
						transform.Rotate (-Vector3.forward, -rotation * speed);
		}
}
