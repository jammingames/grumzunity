﻿using UnityEngine;
using System.Collections;

public class RotateShield : MonoBehaviour
{
		public float speed = 10;
		public float increaseAmmount = 1;
		public float rotation;
		public float heatUpRotation;
		public float powerDownRotation;
		public float overHeatRotation;
		float targetRotation;
		float origRotation;
		bool canMove = true;
		// Use this for initialization
		void Awake ()
		{
				G.i.OnHeatUp += HandleOnHeatUp;
				G.i.Overheated += HandleOverheated;

				origRotation = rotation;
				targetRotation = rotation;
				G.i.OnPlayerDeath += this.HandleOnPlayerDeath;
				G.i.OnStateChange += HandleOnStateChange;
				G.i.OnPlayerShield += HandleOnPlayerShield;
		}

		void HandleOnPlayerShield (bool isStart)
		{
				if (isStart)

						canMove = true;
				else if (!isStart)
						canMove = false;
		}

		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == GameState.Game) {
						this.enabled = true;
						canMove = true;
				}
		}


		// Update is called once per frame
		void Update ()
		{

				if (!canMove)
						return;
				if (rotation < targetRotation) {
						rotation += increaseAmmount;
				} else if (rotation > targetRotation) {
						rotation -= increaseAmmount * 3;
				}
				RotateNormal ();
		}

		void HandleOnPlayerDeath ()
		{
				canMove = false;
				this.enabled = false;
		}

		void RotateNormal ()
		{

				transform.Rotate (Vector3.forward, Time.deltaTime * rotation * -speed);

		}

		void HandleOverheated (bool isStart)
		{
				if (isStart == false) {
//						StopAllCoroutines ();

//						StartCoroutine (render.FadeSpriteColor (render.color, col [0], 0.3f, 0.01f, null));
				}
		}

		void HandleOnHeatUp (int state)
		{
				if (!gameObject.activeSelf)
						return;
				StopAllCoroutines ();
				if (state == 0) {
						targetRotation = heatUpRotation;
						//StartCoroutine (render.FadeSpriteColor (render.color, col [1], duration, 0, null));

				} else if (state == 1) {


						rotation = 0;
						targetRotation = overHeatRotation;


//						StartCoroutine (render.FadeSpriteColor (render.color, col [2], 0.3f, 0, null));


				} else if (state == 2) {

						targetRotation = origRotation;
//						StartCoroutine (render.FadeSpriteColor (render.color, col [0], 0.3f, 0, null));



				}

		}
}
