﻿using UnityEngine;
using System.Collections;

public class ScaleByVelocity : MonoBehaviour
{


		Vector3 origScale;
		public Rigidbody2D rb;
		Vector3 targetScale;
		Vector2 currentVelocity;
		bool hasChanged = false;
		

		// Use this for initialization
		void Start ()
		{
				rb = transform.parent.GetComponent<Rigidbody2D> ();
				origScale = transform.localScale;
				targetScale = new Vector3 (origScale.x / 2, origScale.y, origScale.y);
				transform.localScale = targetScale;
		}

		void Update ()
		{
				
				//transform.localScale = targetScale;

				if (rb.velocity.x > 0 && !hasChanged) {
						hasChanged = true;
						//		//debug.log ("it dun it");
						StartCoroutine (transform.ScaleTo (origScale, 0.0001f, null));
				}
		}
}
