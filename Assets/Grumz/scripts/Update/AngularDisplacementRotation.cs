﻿using UnityEngine;
using System.Collections;

public class AngularDisplacementRotation : MonoBehaviour
{

		//float distanceTravelled = 0;
		public float distancePerFrame = 0;
		public float speed = 10;
		Vector3 lastPosition;
		public float circumference;
		public bool randomize;
		public bool parent;
		
		void Start ()
		{
				if (parent)
						lastPosition = transform.parent.position;
				else
						lastPosition = transform.position;
		}
	
		void Update ()
		{
				
				if (parent) {

						distancePerFrame = transform.parent.position.x - lastPosition.x;
						lastPosition = transform.parent.position;
				} else {
						distancePerFrame = transform.position.x - lastPosition.x;
						lastPosition = transform.position;
				}
				
				//float 
				float rotation = distancePerFrame / circumference;
				if (randomize)
						rotation *= (Random.value + 0.3f);
//				//debug.log (rotation);
				if (rotation > 0)
						transform.Rotate (Vector3.forward, -rotation * speed);
				else if (rotation < 0)
						transform.Rotate (-Vector3.forward, -rotation * speed);
		}
}
