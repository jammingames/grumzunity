﻿using UnityEngine;
using System.Collections;

public class LerpAlpha : MonoBehaviour, IActorPhysicsProxy
{

	
		float startAlpha;
		float regularAlpha;
		float heatedAlpha;
		[Range(0, 1)]
		public float
				endAlpha;

		public Color zombieColor;
		Color origCol;
		//	public Material mat;

		SpriteRenderer _renderer;
		public Material mat, mat2;
		public float duration = 5;

		public bool lerpColor;
		public bool regular = true;
		bool hasHeated = false;
		Color col;
		public float t = 0.95f;
	
		// Use this for initialization
		void Start ()
		{
				origCol = mat.color;
				G.i.Overheated += HandleOverheated;
				_renderer = GetComponent<SpriteRenderer> ();
				_renderer.material = mat;
				//mat = _renderer.material;
				startAlpha = 0.95f;
				
				regularAlpha = startAlpha;
				heatedAlpha = 0.75f;
				mat.SetFloat ("_Cutoff", regularAlpha);	
				col = _renderer.color;
				col.a = regularAlpha;
				mat2.color = col;
				
				//startColor = mat.color;

						 
				//mat.color = startColor;
		}

		void HandleOverheated (bool isStart)
		{
				if (isStart) {
						
						mat.color = zombieColor;
						hasHeated = true;
						startAlpha = heatedAlpha;
						t = startAlpha;
				} else {
						mat.color = origCol;
						hasHeated = false;
						//startAlpha = regularAlpha;
						startAlpha = 0.95f;
						t = startAlpha;
				}
				
				mat.SetFloat ("_Cutoff", startAlpha);
				//col.a = 1.3f - startAlpha;
				_renderer.color = col;
				
				//mat.color
		}


		//public void ChangeMaterial
		
		void Update ()
		{
				if (Input.anyKey && !hasHeated) 
						lerpColor = true;
				else
						lerpColor = false;

				if (lerpColor && t >= endAlpha) {
						t -= Time.deltaTime / duration;
				} else if (!lerpColor && t <= startAlpha) {
						t += Time.deltaTime / (duration / 2);
				}
				mat.SetFloat ("_Cutoff", t);	
				//col.a = 1.3f - t;
				_renderer.color = col;
				//mat.color = Color.Lerp (startColor, endColor, t);

				
		}
	


		public IEnumerator LerpColors ()
		{
			
				while (t > 0) {

						mat.SetFloat ("_Cutoff", t);
						if (t > 0) { // while t below the end limit...
								// increment it at the desired rate every update:
								t -= Time.deltaTime / duration;
						}
						yield return null;
				}
		}
		
		public void HandleInput (float h, float y, bool isThing)
		{

		}

		public void HandleMouseInput (bool isDown)
		{
				////debug.log ("Button pushed");
				//		lerpColor = isDown;
		}

}
