﻿using UnityEngine;
using System.Collections;

public class RotateTowards : MonoBehaviour
{
	
	
	
	
	
		public float targetDuration = 1;
		public float currentRotation = 1;
		public float targetRotation = 1;
		public float currentLerpTime = 0;
		public EaseType easer;
		public float speed = 1;
		public float rotation = 1;
		float origRotation;
		bool canMove = true;

		float range = 0;
		float start;

		void Awake ()
		{
				origRotation = rotation;
				range = 0;
				start = range;
				
		}
	
		void OnEnable ()
		{
		
				currentRotation = origRotation;
				targetRotation = currentRotation;
				targetDuration = 1;
				
				start = currentRotation;
				range = targetRotation - start;
		
		}
	
		// Update is called once per frame
		void Update ()
		{

				if (Input.GetKeyDown (KeyCode.Alpha8))
						Application.targetFrameRate = 20;
				if (Input.GetKeyDown (KeyCode.Alpha9))
						Application.targetFrameRate = 60;
				if (!canMove)
						return;
				currentRotation = rotation;
				if (currentRotation != targetRotation) {
						currentLerpTime = Mathf.MoveTowards (currentLerpTime, targetDuration, Time.deltaTime);
						if (currentLerpTime > targetDuration) {
								currentLerpTime = targetDuration;
						}
						currentRotation = Mathf.Lerp (start, targetRotation, Ease.FromType (easer) (currentLerpTime / targetDuration));
						//currentRotation = start + range * Ease.FromType (easer) (currentLerpTime / targetDuration);
						rotation = currentRotation;
				}

				transform.Rotate (Vector3.forward, rotation * -speed * Time.deltaTime);
		
		}
	
		public void ChangeRotation (float target, float duration)
		{
				start = currentRotation;
		
				targetRotation = target;
				targetDuration = duration;
				range = targetRotation - start;
				currentLerpTime = 0;
				easer = EaseType.Linear;
		}
	
		public void ChangeRotation (float target, float duration, EaseType ease)
		{
				start = currentRotation;
				targetRotation = target;
				targetDuration = duration;
		
				range = targetRotation - start;
				currentLerpTime = 0;
				easer = ease;
				////debug.log (targetDuration);
		}

		public void SetRotation (float newRotation, float newSpeed)
		{
				rotation = newRotation;
				currentRotation = newRotation;
				origRotation = newRotation;
				speed = newSpeed;
		}

		public void Start ()
		{
				canMove = true;
		}
		
		public void Stop ()
		{
				canMove = false;
		}
	
	
}
