﻿using UnityEngine;
using System.Collections;

public class RotateLerpTowardsPlayer : MonoBehaviour
{


		public Rigidbody2D _target;
		public float rotSpeed = 5f;

		// Use this for initialization
		void Start ()
		{
				if (_target == null)
						_target = G.i.playerObj.GetComponent<Rigidbody2D> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
	

				var dir = _target.transform.position - transform.position;
				var angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
				// fast rotation

		
				// distance between target and the actual rotating object
		
		
		
				Quaternion targetRotation = Quaternion.AngleAxis (angle, Vector3.forward);
				// calculate the Quaternion for the rotation
				Quaternion rot = Quaternion.Slerp (transform.rotation, targetRotation, rotSpeed * Time.deltaTime);
		
				rot = Quaternion.Euler (new Vector3 (0, 0, rot.eulerAngles.z)); 						
				//Apply the rotation 
				transform.rotation = rot; 
		}
}
