﻿using UnityEngine;
using System.Collections;

public class LerpColorTowards : MonoBehaviour
{
	
	
		SpriteRenderer spr;
	
		public EaseType easer;
	

		public float targetDuration;
		public Color currentCol;
		public Color targetCol;
		
		
		public float currentLerpTime = 0;
		public Color origColor;
	
	
	
		Color range = new Color (0, 0, 0);
		Color start;
		// Use this for initialization
	
		void Awake ()
		{
				spr = GetComponent<SpriteRenderer> ();
				origColor = spr.color;

				range.a = 0;
				currentCol = spr.color;
				targetDuration = 1;
				targetCol = currentCol;
		
		}

		
		void OnEnable ()
		{
				currentLerpTime = 0;
				targetDuration = 1;
		}
	
	


	
		// Update is called once per frame
		void Update ()
		{
	
		
				currentCol = spr.color;
				if (currentCol != targetCol) {
						currentLerpTime = Mathf.MoveTowards (currentLerpTime, targetDuration, Time.deltaTime);
						if (currentLerpTime > targetDuration) {
								currentLerpTime = targetDuration;
						}
			
						currentCol = Color.Lerp (start, targetCol, Ease.FromType (easer) (currentLerpTime / targetDuration));
						spr.color = currentCol;
				}
		}

		public void ResetColors ()
		{
				currentLerpTime = 0;
				currentCol = Color.white;
//				spr.color = Color.white;
				targetDuration = 1f;
				start = Color.white;
				targetCol = Color.white;
		}
	
		public void SetColorParams (Color curCol, Color tarCol, float duration)
		{
				currentLerpTime = 0;
				currentCol = curCol;
				targetCol = tarCol;
				targetDuration = duration;
				start = currentCol;
				if (spr != null)
						spr.color = currentCol;
		}
	
	
		public void ChangeColor (Color target, float duration)
		{
		
		
		
				start = currentCol;
				targetCol = target;
			
				targetDuration = duration;
				range = targetCol - start;
		
		
				easer = EaseType.Linear;
		
				currentLerpTime = 0;
				
		}
	
	
		public void ChangeColor (Color target, float duration, EaseType ease)
		{
		
		
				start = currentCol;
				targetCol = target;
				targetDuration = duration;
				range = targetCol - start;
		
		
				this.easer = ease;
				currentLerpTime = 0;
				
		}
	
	
}
