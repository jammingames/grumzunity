﻿using UnityEngine;
using System.Collections;

public class ScaleTowards : MonoBehaviour
{
	
	

	
		
		public float targetDuration;
		Vector3 origScale;
		public Vector3 currentScale;
		public Vector3 targetScale;
		public float currentLerpTime = 0;
		public EaseType easer;
		
		Vector3 range;
		Vector3 start;
		Vector3 lastScale;

		void Awake ()
		{
				origScale = transform.localScale;
				currentScale = origScale;
				targetScale = origScale;
				lastScale = origScale;
		}

		void OnEnable ()
		{

				currentScale = origScale;
				
//				targetScale = origScale;
//				lastScale = new Vector3 (10, 10, 10);
//				targetDuration = 0.01f;

		
		}

		public void SetScaleParams (Vector3 currentScale, Vector3 targetScale, Vector3 lastScale, float targetDuration)
		{
				this.currentScale = currentScale;
				this.targetScale = targetScale;
				this.lastScale = lastScale;
				this.targetDuration = targetDuration;
		}
	
		// Update is called once per frame
		void Update ()
		{
				
				
				currentScale = transform.localScale;
				if (currentScale != targetScale) {

											
						
						currentLerpTime = Mathf.MoveTowards (currentLerpTime, targetDuration, Time.deltaTime);
						if (currentLerpTime > targetDuration) {
								currentLerpTime = targetDuration;
						}
						currentScale = (start + range * Ease.FromType (easer) (currentLerpTime / targetDuration));
						transform.localScale = currentScale;
					
				}
				lastScale = targetScale;
			
		}
	
		public void ChangeScale (Vector3 target, float duration)
		{

				if (lastScale != target) {

						start = transform.localScale;

						range = target - start;
						currentLerpTime = 0;
						targetScale = target;
						targetDuration = duration;
						easer = EaseType.Linear;
				}
			
		}

		public void ChangeScale (Vector3 target, float duration, EaseType ease)
		{
				if (lastScale != target) {
//						//debug.log ("changing scale from:  " + targetScale + " to: " + target);			
						start = transform.localScale;
		
						range = target - start;
						currentLerpTime = 0;
						targetScale = target;
						targetDuration = duration;
						easer = ease;
//				//debug.log (targetDuration);
				}
		}





}
