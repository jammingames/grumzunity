﻿using UnityEngine;
using System.Collections;

public class FadeTowards : MonoBehaviour
{


		SpriteRenderer spr;

		public EaseType easer;

		public float targetAlpha;
		public float targetDuration;
		public Color currentCol;
		public Color targetCol;
		public float t = 0;
		public bool isFading, shouldFade;
		public float currentLerpTime = 0;
		public Color origColor;


	
		Color range = new Color (0, 0, 0);
		Color start;
		// Use this for initialization
		
		void Awake ()
		{
				range.a = 0;
				start = new Color (0, 0, 0);
				spr = GetComponent<SpriteRenderer> ();
				currentCol = spr.color;
				origColor = spr.color;
				targetAlpha = currentCol.a;
				targetDuration = 1;
				targetCol = currentCol;
				start = currentCol;
				range = targetCol - start;

		}
	
		// Update is called once per frame
		void Update ()
		{
//				currentRotation = rotation;
//				if (currentRotation != targetRotation) {
//						currentLerpTime = Mathf.MoveTowards (currentLerpTime, targetDuration, Time.deltaTime);
//						if (currentLerpTime > targetDuration) {
//								currentLerpTime = targetDuration;
//						}
//						currentRotation = Mathf.Lerp (start, targetRotation, Ease.FromType (easer) (currentLerpTime / targetDuration));
//						//currentRotation = start + range * Ease.FromType (easer) (currentLerpTime / targetDuration);
//						rotation = currentRotation;
//				}
//


				currentCol = spr.color;
				if (currentCol != targetCol) {
						currentLerpTime = Mathf.MoveTowards (currentLerpTime, targetDuration, Time.deltaTime);
						if (currentLerpTime > targetDuration) {
								currentLerpTime = targetDuration;
						}

						currentCol = Color.Lerp (start, targetCol, Ease.FromType (easer) (currentLerpTime / targetDuration));
						spr.color = currentCol;
				}
		}

	
		public void ChangeColor (float target, float duration)
		{
		
			
		
				start = currentCol;
				targetAlpha = target;
				targetCol.a = targetAlpha;
				targetDuration = duration;
				range = targetCol - start;


				easer = EaseType.Linear;

				currentLerpTime = 0;
				t = 0;
		}


		public void ChangeColor (float target, float duration, EaseType ease)
		{


				start = currentCol;
				targetAlpha = target;
				targetCol.a = targetAlpha;
				targetDuration = duration;
				range = targetCol - start;
			

				this.easer = ease;
				currentLerpTime = 0;
				t = 0;
		}


}
