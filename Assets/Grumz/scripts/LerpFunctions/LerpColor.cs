﻿using UnityEngine;
using System.Collections;

public class LerpColor : MonoBehaviour, IActorPhysicsProxy
{

	
		public Color startColor;
		public Color endColor;
		//	public Material mat;

		SpriteRenderer _renderer;
		public float duration = 5;

		public bool lerpColor;
	
		float t = 0;
	
		// Use this for initialization
		void Start ()
		{
				_renderer = GetComponent<SpriteRenderer> ();
			
				startColor = _renderer.color;

						 
				//_renderer.color = startColor;
		}

		
		void Update ()
		{
				if (Input.anyKey) 
						lerpColor = true;
				else
						lerpColor = false;

				if (lerpColor && t < 1) {
						t += Time.deltaTime / duration;
				} else if (!lerpColor && t > 0) {
						t -= Time.deltaTime / (duration / 2);
				}

				_renderer.color = Color.Lerp (startColor, endColor, t);

				
		}
	


		public IEnumerator LerpColors ()
		{
			
				while (t < 1) {

						_renderer.color = Color.Lerp (startColor, endColor, t);
						if (t < 1) { // while t below the end limit...
								// increment it at the desired rate every update:
								t += Time.deltaTime / duration;
						}
						yield return null;
				}
		}
		
		public void HandleInput (float h, float y, bool isThing)
		{

		}

		public void HandleMouseInput (bool isDown)
		{
				////debug.log ("Button pushed");
				lerpColor = isDown;
		}

}
