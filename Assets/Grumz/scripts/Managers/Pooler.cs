﻿using UnityEngine;
using System.Collections;


public class Pooler : MonoBehaviour
{


		public GameObject prefab;
		public int quantity = 10;

		// Use this for initialization
		void Start ()
		{
				prefab.CreatePool (quantity);
		}

		

		public void SpawnInPool ()
		{
				prefab.Spawn ();
		}

		public void SpawnInPool (Vector3 position)
		{
				prefab.Spawn (position);
		}

		public void SpawnInPool (Vector3 position, Quaternion rotation)
		{
				prefab.Spawn (position, rotation);
		}

		public void SpawnInPool (Transform parent, Vector3 position, Quaternion rotation)
		{
				prefab.Spawn (parent, position, rotation);
		}

}
