﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
		//PlayerPref keys
		private string HIGH_SCORE = "highScore";
		private string EARNED_TIME_ATTACK = "hasTimeAttack";
		private string BEST_TIME = "bestTimeAttackScore";
		private string LAST_TIME = "timeAttackScore";



		private const string LEADERBOARD_ID = "CgkIipfs2qcGEAIQAA";
		private const string LEADERBOARD_NAME = "leaderboard_best_scores";


		public GameObject skullGlow;
	
		//FUCK YOU ANDROID YOU JERKFACE FUCKER!
	
	
		
	
	

		//--------------------------------------
		// INITIALIZE
		//--------------------------------------



		int bounceScore;
		int additionalScore;
		public int totalScore;
		public int bestScore;
		public bool hasScored100;
		public float lastTimeAttack;
		public float bestTimeAttack;
		bool firstGame = false;
		

		int lastScore;
		long scoreLong;

		bool tutorialDone = false;


		void Awake ()
		{
				if (!_instance)		
						_instance = this;
				else if (this != _instance)
						Destroy (this.gameObject);


				G.i.OnTutorial += HandleOnTutorial;

		
				
				G.i.OnStateChange += HandleOnStateChange;
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
				OnEarnedTimeAttack += HandleOnEarnedTimeAttack;




			




				GetPrefs ();


		}


		// Use this for initialization
		void OnEnable ()
		{
				totalScore = 0;
				lastScore = totalScore;
				if (tutorialDone) {
						G.i.OnBounceIncrease += HandleOnBounceIncrease;
						G.i.OnBounceAdd += HandleOnBounceAdd;
				}
		}


		// Update is called once per frame
		void Update ()
		{
				if (totalScore > lastScore) {
						TriggerScoreHasChanged ();

				}
				lastScore = totalScore;


				if (Input.GetKeyDown (KeyCode.Comma)) {
						ResetScore ();
				}
		
		}

		public void GetPrefs ()
		{
				if (!PlayerPrefs.HasKey (HIGH_SCORE)) {
						firstGame = true;
						PlayerPrefs.SetInt (HIGH_SCORE, 0);
						bestScore = 0;
				}
		
				if (!PlayerPrefs.HasKey (EARNED_TIME_ATTACK)) {
						hasScored100 = false;
						PlayerPrefs.SetInt (EARNED_TIME_ATTACK, 0);
				} else if (PlayerPrefs.GetInt (EARNED_TIME_ATTACK) == 1) {
						hasScored100 = true;
				}

				if (PlayerPrefs.HasKey (LAST_TIME))
						lastTimeAttack = PlayerPrefs.GetFloat (LAST_TIME);
				else {
						lastTimeAttack = 500;
						PlayerPrefs.SetInt (LAST_TIME, 500);
				}
				
				if (!PlayerPrefs.HasKey (BEST_TIME)) {
						PlayerPrefs.SetFloat (BEST_TIME, 500);
						bestTimeAttack = 500;
				} else	if (PlayerPrefs.GetFloat (BEST_TIME) > bestTimeAttack) {
						bestTimeAttack = PlayerPrefs.GetFloat (BEST_TIME);
				}
		}

	
	
		public void SubmitScore (int score)
		{

				if (!PlayerPrefs.HasKey (HIGH_SCORE) || PlayerPrefs.GetInt (HIGH_SCORE) < score) {
						PlayerPrefs.SetInt (HIGH_SCORE, score);
						TriggerSubmitScore (true, score);
				}
		}
	


	
		private void ResetScore ()
		{
				SubmitScore (0);
				PlayerPrefs.SetInt (HIGH_SCORE, 0);
				//				//debug.log ("resetting highscore to " + PlayerPrefs.GetInt ("highScore"));
		
		}

	
	


		public void CheckHighScore ()
		{
				int scoreToCheck = totalScore;
	
				if (!firstGame && bestScore < PlayerPrefs.GetInt (HIGH_SCORE)) {
						bestScore = PlayerPrefs.GetInt (HIGH_SCORE);
				}
	
				if (firstGame) {
						bestScore = scoreToCheck;
						PlayerPrefs.SetInt (HIGH_SCORE, bestScore);
						TriggerHighScore (true, bestScore);
				} else if (scoreToCheck > bestScore) {
						bestScore = scoreToCheck;
						PlayerPrefs.SetInt (HIGH_SCORE, bestScore);
						TriggerHighScore (true, bestScore);
						
				} else if (totalScore < bestScore) {
						ScoreManager.instance.TriggerHighScore (false, totalScore);
				}
				SubmitScore (totalScore);
		}

		public void CheckHighScore (int scoreToCheck)
		{

		
				if (!firstGame && bestScore < PlayerPrefs.GetInt (HIGH_SCORE)) {
						bestScore = PlayerPrefs.GetInt (HIGH_SCORE);
				}
		
				if (firstGame) {
						bestScore = scoreToCheck;
						PlayerPrefs.SetInt (HIGH_SCORE, bestScore);
						TriggerHighScore (true, bestScore);
				} else if (scoreToCheck > bestScore) {
						bestScore = scoreToCheck;
						PlayerPrefs.SetInt (HIGH_SCORE, bestScore);
						TriggerHighScore (true, bestScore);
			
				} else if (totalScore < bestScore) {
						ScoreManager.instance.TriggerHighScore (false, scoreToCheck);
				}
				SubmitScore (scoreToCheck);
		}
	
	
	
	


	
		void HandleOnEarnedTimeAttack (bool newScore)
		{
				if (newScore) {
//						SubmitAchievements (100);	
						hasScored100 = true;
						PlayerPrefs.SetInt (EARNED_TIME_ATTACK, 1);
				}
		}
	
		void HandleOnPlayerDeath ()
		{	
				if (totalScore >= 100 && !hasScored100) {
						TriggerEarnedTimeAttack (true);
				}

		
		
		}
	
		void HandleOnTutorial (bool isStart)
		{
				if (!isStart) {
						tutorialDone = true;
						G.i.OnBounceIncrease += HandleOnBounceIncrease;
						G.i.OnBounceAdd += HandleOnBounceAdd;
			
				}
		}
	
	
		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == GameState.Intro || nextState == GameState.StartGame || nextState == GameState.Game) {
						totalScore = 0;
						lastTimeAttack = 0;
						lastScore = 0;
						additionalScore = 0;
						bounceScore = 0;
				} else if (nextState == GameState.End) {
						CheckHighScore ();
				}
		}

		void HandleOnEarnedTimeAttack (float t)
		{
				lastTimeAttack = t;
				PlayerPrefs.SetFloat (LAST_TIME, lastTimeAttack);
				if (PlayerPrefs.GetFloat (LAST_TIME) > PlayerPrefs.GetFloat (BEST_TIME))
						PlayerPrefs.SetFloat (BEST_TIME, lastTimeAttack);
		}
	
	
		void HandleOnBounceIncrease (int numBounces)
		{
				bounceScore = numBounces;
				totalScore = bounceScore + additionalScore;
				if (totalScore == 100 && hasScored100) {
						TriggerTimeAttackScore (G.i.elapsedTime);
								
						
				}
		
		}
		void HandleOnBounceAdd (int numToAdd)
		{
				additionalScore += numToAdd;
				totalScore = bounceScore + additionalScore;
				if (totalScore == 100 && hasScored100) {
						TriggerTimeAttackScore (G.i.elapsedTime);
						
			
				}
		}
	
	





	#region events

		public delegate void ScoreChanged ();
		public event ScoreChanged OnScoreChanged, OnRequestLeaderboard;
	
		public delegate void HighScore (bool isNewScore,int newScore);
		public event HighScore OnHighScore, OnSubmitNewScore;

		public delegate void BoolScoreEvents (bool isTrue);
		public event BoolScoreEvents OnEarnedTimeAttack; 
	
		public delegate void OnGameScore (int score);
		public event OnGameScore OnFinishGameScore;

		public void TriggerLeaderboard ()
		{
				if (OnRequestLeaderboard != null) {
						OnRequestLeaderboard ();
				}
		}
	
		public void TriggerFinishGameScore (int i)
		{
				if (OnFinishGameScore != null) {
						OnFinishGameScore (i);
				}
		}

		public void TriggerSubmitScore (bool shouldSubmit, int newScore)
		{
				if (OnSubmitNewScore != null) {
						OnSubmitNewScore (shouldSubmit, newScore);
				}
		}

		
	
		public delegate void TimeAttackEvent (float time);
		public event TimeAttackEvent OnTimeAttackScore;
	
		public void TriggerTimeAttackScore (float t)
		{
				if (OnTimeAttackScore != null) {
						OnTimeAttackScore (t);
				}
		}
	
	
		public void TriggerScoreHasChanged ()
		{
				if (OnScoreChanged != null) {
						OnScoreChanged ();
				}
		}
	
		public void TriggerHighScore (bool isNewScore, int newScore)
		{
				if (OnHighScore != null) {
						OnHighScore (isNewScore, newScore);
				}
				if (isNewScore)
						skullGlow.SetActive (false);
				else if (!isNewScore && !skullGlow.activeInHierarchy)
						skullGlow.SetActive (true);
		}
	
		public void TriggerEarnedTimeAttack (bool newScore)
		{
				if (OnEarnedTimeAttack != null) {
						OnEarnedTimeAttack (newScore);
				}
		}

	#endregion

		static ScoreManager _instance = null;
		public static ScoreManager instance {
				get {
						if (!_instance) {
								
								_instance = FindObjectOfType (typeof(ScoreManager)) as ScoreManager;

								// nope, create a new one
							
						}

						return _instance;
				}
		}


		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}
}
