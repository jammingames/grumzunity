﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Flags]
public enum GameState
{
		NullState = 1 << 0,
		Intro = 1 << 1,
		MainMenu = 1 << 2,
		StartGame = 1 << 3,
		Game = 1 << 4,
		End = 1 << 5,
		Tutorial = 1 << 6,
}

//None   = 1 << 0, // 1
//Poison = 1 << 1, // 2
//Slow   = 1 << 2, // 4
//Mute   = 1 << 3  // 8
public class G : MonoBehaviour
{
		public bool superInvul = false;
		public bool shouldMove = true;		
		public Color col;
		public float duration = 0.5f;
		public float skinWidth;
//		bool inTransition = false;
		public PlayerMovement playerMove;
		bool isPrepared = false;

		public int numBounces;
		//	public BoxCollider2D worldCollider;


				
		public float BOT_SCREEN_EDGE, TOP_SCREEN_EDGE, LEFT_SCREEN_EDGE, RIGHT_SCREEN_EDGE;

		public static float[] increasePerBounce;
		public static int[] bounceIncreaseLevels;
		public GameObject playerObj;

		public float elapsedTime;
		public List<GameObject> enemies;
		public List<Enemy2Controller> g2Enemies;
		public List<Enemy3Controller> g3Enemies;
		public int g2EnemyCount;
		public int g3EnemyCount;
		public float heatupTime;
		public float heatupPercent;
		public float timeToCool;
		public float increaseFactor;
		public bool isOverheated = false;

		public GameObject instructionPanel;	
		public GameObject glowBtn;


		[HideInInspector]
		public HeatState
				currentHeatState = HeatState.NullState;
		
		//int numFrames = 0;
		//bool shouldPrint = false;
		GameState currentState = GameState.NullState;

		void OnBounce (int num)
		{
				//	numBounces
		}

		void OnEnabled ()
		{
				elapsedTime = 0;
				numBounces = 0;
		}

		void Start ()
		{

		}

		void Awake ()
		{
//				if (!_instance)		
//						_instance = this;
//				else if (this != _instance)
//						Destroy (this.gameObject);
//				
				Application.targetFrameRate = 60;
				instructionPanel.SetActive (true);
				instructionPanel.SetActive (false);

				
				OnBounceIncrease += OnBounce;
				RIGHT_SCREEN_EDGE = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, 0, 5)).x;
				LEFT_SCREEN_EDGE = Camera.main.ScreenToWorldPoint (new Vector3 (0, 0, 5)).x;
				
				BOT_SCREEN_EDGE = Camera.main.ScreenToWorldPoint (new Vector3 (0, 0, 5)).y;
				TOP_SCREEN_EDGE = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 5)).y;


		
				OnStateChange += HandleOnStateChange;
				SetGameState (GameState.Intro);
				//float offset = (playerMove.col.bounds.size.x * 2) + skinWidth;
				//worldCollider.size = new Vector2 (Mathf.Abs (LEFT_SCREEN_EDGE) + RIGHT_SCREEN_EDGE - offset, Mathf.Abs (BOT_SCREEN_EDGE) + TOP_SCREEN_EDGE - offset);
		}

		
		public delegate void BounceIncrease (int numBounces);
		public event BounceIncrease OnBounceIncrease, OnBounceAdd;
	
		public void TriggerBounceIncrease (int numBounces)
		{
				if (OnBounceIncrease != null)
						OnBounceIncrease (numBounces);
		}

		public void TriggerBounceAdd (int numToAdd)
		{
				if (OnBounceAdd != null)
						OnBounceAdd (numToAdd);
		}

		public delegate void HeatingEvent (HeatState state);
		public event HeatingEvent OnHeating;

		public void TriggerOnHeating (HeatState state)
		{
				if (OnHeating != null && currentHeatState != state) {
//						Debug.Log ("triggering state: " + state);

						OnHeating (state);
						currentHeatState = state;
				}
		}

		public delegate void HeatEvent (int state);
		public event HeatEvent OnHeatUp;

		public void TriggerHeating (int state)
		{
				if (OnHeatUp != null) {
					
						OnHeatUp (state);
				}
		}
		public delegate void TimedGameEvent (bool isStart,float duration);
		public event TimedGameEvent OnSuperInvul;
	
	
		public void TriggerSuperInvul (bool isInvul, float duration)
		{
				superInvul = isInvul;
				if (OnSuperInvul != null)
						OnSuperInvul (isInvul, duration);
		}

		public delegate void GameEvent (bool isStart);
		public event GameEvent Overheated, OnBossSpawn, OnPlayerInvul, OnPlayerChangeDirection, OnPlayerShield, OnTutorial, OnPlayerInvulStart;


		public void TriggerPlayerInvulStart (bool isStart)
		{
				if (OnPlayerInvulStart != null)
						OnPlayerInvulStart (isStart);
		}

		public void TriggerStartTutorial (bool isStart)
		{
				if (OnTutorial != null)
						OnTutorial (isStart);
		}

		public void TriggerPlayerShield (bool isShielded)
		{
				if (OnPlayerShield != null)
						OnPlayerShield (isShielded);
		}

		public void TriggerPlayerChange (bool isUp)
		{
				if (OnPlayerChangeDirection != null)
						OnPlayerChangeDirection (isUp);
		}

		public void TriggerPlayerInvul (bool isInvul)
		{
				if (OnPlayerInvul != null)
						OnPlayerInvul (isInvul);
		}

		public void TriggerOverheated (bool isStart)
		{
				isOverheated = isStart;
				if (Overheated != null)
						Overheated (isStart);
		}

		public void TriggerBoss (bool isStart)
		{
				if (OnBossSpawn != null)
						OnBossSpawn (isStart);
		}





		public System.Action<GameState> OnComplete;
		public GameState gameState { get; private set; }
		public GameState nextState;

		public delegate void OnStateChangeHandler (GameState nextState);
		public event OnStateChangeHandler OnStateChange;
		
		public delegate void OnStateUpdate ();
		public event OnStateUpdate OnStateUpdateHandler;

		public delegate void OnPlayerStuff ();
		public event OnPlayerStuff OnPlayerTap, OnPlayerDeath, OnPlayerLeaveDirection, OnFireBomb, OnRemoveAds;

		


		public delegate void OnHitPosition (HitPosition hitPos);
		public event OnHitPosition OnCollision;

		public void TriggerOnCollision (HitPosition hitPos)
		{
				if (OnCollision != null)
						OnCollision (hitPos);
		}

		public void TriggerRemoveAds ()
		{
				if (OnRemoveAds != null) {
						OnRemoveAds ();
				}
		}


		public void TriggerBombFire ()
		{
				if (OnFireBomb != null)
						OnFireBomb ();
		}


		public void TriggerPlayerLeaveDirection ()
		{
				if (OnPlayerLeaveDirection != null) {
						OnPlayerLeaveDirection ();
				}
		}
		
		
		public void TriggerPlayerDeath ()
		{
				if (OnPlayerDeath != null) {
						
						OnPlayerDeath ();
				}
		}

		public void TriggerPlayerTap ()
		{
				if (OnPlayerTap != null) {
						OnPlayerTap ();
				}
		}


		public void SetGameState (GameState gameState)
		{
				if (gameState != currentState) {

						if (OnStateChange != null) {
								OnComplete = null;
								OnStateChange (gameState);
						}
						if (currentState == GameState.MainMenu && gameState == GameState.StartGame) {
								StartCoroutine (Auto.Wait (1, () => {

										TriggerStartTutorial (true);
								}));
						}
						currentState = gameState;
				} else {
						//debug.log ("ALREADY IN THIS STATE");

				}

		}
	
		IEnumerator DelayedSetState (float duration, GameState state)
		{
				yield return new WaitForSeconds (duration);
				SetGameState (state);
		}
		
		public void DelayedStartGame (float duration)
		{
				StartCoroutine (DelayedSetState (duration, GameState.Game));
		}

		public void HandleOnStateChange (GameState state)
		{
				OnStateUpdateHandler = null;
//				inTransition = true;
				if (state == GameState.Intro) {
						numBounces = 0;
						StartCoroutine (Auto.Wait (0.4f, () => {
								SetGameState (GameState.MainMenu);}));
						
//						OnStateUpdateHandler += () => {
//								if (Input.anyKeyDown || Input.GetMouseButtonDown (0)) {
//										PrepareGame ();
//								}
//								//if (Input.anyKeyDown || Input.GetMouseButtonDown (0))
//								//SetGameState (GameState.MainMenu);
//						};

				} else if (state == GameState.MainMenu) {
						OnStateUpdateHandler += () => {
								if (Input.anyKeyDown || Input.GetMouseButtonDown (0)) {
										PrepareGame ();
								}
								//if (Input.anyKeyDown || Input.GetMouseButtonDown (0))
								//SetGameState (GameState.MainMenu);
						};
				} else if (state == GameState.StartGame) {
						numBounces = 0;
						
				} else if (state == GameState.Game) {
						numBounces = 0;
						OnStateUpdateHandler += () => {
								elapsedTime += Time.deltaTime;
						};
			
				} else if (state == GameState.End) {
						isPrepared = false;
						OnStateUpdateHandler += () => {
								if (Input.GetKeyDown (KeyCode.Equals)) {
										PrepareGame ();
								}
						};
//						OnStateUpdateHandler += () => {
//								if (Input.anyKeyDown || Input.GetMouseButtonDown (0)) {
//										PrepareGame ();
//								}
//								//if (Input.anyKeyDown || Input.GetMouseButtonDown (0))
//								//SetGameState (GameState.MainMenu);
//						};
//						StartCoroutine (Auto.Wait (0.5f, () => {
//								OnStateUpdateHandler += () => {
//										if (Input.anyKeyDown || Input.GetMouseButtonDown (0))
//												ResetGame ();};}));
//				
						
				}
				
				////debug.log ("Gamestate changing to: " + state);

		}

//#if UNITY_ANDROID
//		void OnApplicationFocus (bool pauseStatus)
//		{
//				if (pauseStatus) { 
//						Application.Quit ();
//				}
//		}
//
//	void OnApplicationPause()
//	{
//		Application.Quit();
//	}

//#endif
	
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.Comma)) {
						G.i.TriggerRemoveAds ();
				}
				if (Input.GetKey (KeyCode.P)) {
						if (Time.timeScale >= 0.3f) {

								Time.timeScale += 0.1f;
						}

				} else if (Input.GetKey (KeyCode.O)) {
						if (Time.timeScale <= 100) {

								Time.timeScale -= 0.1f;
						}
				} else if (Input.GetKeyDown (KeyCode.L))
						Time.timeScale = 1;
				Mathf.Clamp (Time.timeScale, 0.3f, 100);
				if (Input.GetKeyDown (KeyCode.R))
						ResetGame ();
				if (Input.GetKeyDown (KeyCode.Escape))
						Application.Quit ();
				if (OnStateUpdateHandler != null)
						OnStateUpdateHandler ();
				
				
		}

		public void StartGame ()
		{
			
				SetGameState (GameState.StartGame);
		}

		public void PrepareGame ()
		{
				if (!isPrepared) {
						numBounces = 0;
						isPrepared = true;
						if (glowBtn != null) {
								glowBtn.BroadcastMessage ("TurnOff", SendMessageOptions.DontRequireReceiver);
						}
						SetGameState (GameState.StartGame);
				}
		}

		public void ResetGame ()
		{
				isPrepared = false;
				numBounces = 0;

//				SetGameState (GameState.StartGame);
				Application.LoadLevel (Application.loadedLevel);
		}

		public void StartNewGame ()
		{
				ResetGame ();
				PrepareGame ();
		}

		public void EndGame ()
		{
				SetGameState (GameState.End);
		}

	
		public void FinishedStateChange ()
		{
				
				this.gameState = gameState;
		}
	

		static G _instance = null;
		public static G i {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(G)) as G;

								// nope, create a new one
//								if (!_instance) {
//										var obj = new GameObject ("G");
//										_instance = obj.AddComponent<G> ();
//								}
						}

						return _instance;
				}
		}


		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}
}
