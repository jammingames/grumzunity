﻿using UnityEngine;
using System.Collections;

public class SceneRef : MonoBehaviour
{

		public GameObject sceneGame, sceneStartGame, scenePlayerStart, enemyParent, maskLeft, maskRight;

		public SpriteMask lMask, rMask;
		// Use this for initialization
		void Awake ()
		{
				sceneGame = GameObject.Find ("SceneGame");
				sceneStartGame = GameObject.Find ("SceneGameStart");
				scenePlayerStart = GameObject.Find ("ScenePlayerStart");
				enemyParent = GameObject.Find ("Enemies");
				if (maskLeft == null)
						maskLeft = GameObject.Find ("maskLeft");
				if (maskRight == null)
						maskRight = GameObject.Find ("maskRight");

				if (maskLeft != null)
						lMask = maskLeft.GetComponent<SpriteMask> ();
				if (maskRight != null)
						rMask = maskRight.GetComponent <SpriteMask> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}



		static SceneRef _instance = null;
		public static SceneRef i {
				get {
						if (!_instance) {
								// check if a SceneRef is already available in the scene graph
								_instance = FindObjectOfType (typeof(SceneRef)) as SceneRef;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("SceneRef");
										_instance = obj.AddComponent<SceneRef> ();
								}
						}

						return _instance;
				}
		}


		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}

}
