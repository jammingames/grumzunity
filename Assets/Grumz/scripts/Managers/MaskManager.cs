﻿using UnityEngine;
using System.Collections;

public class MaskManager : MonoBehaviour
{
		public SpriteMask lMask, rMask, lPortInMask, lPortOutMask, rPortInMask, rPortOutMask;

		public float portPos = 0;
		// Use this for initialization
		
		public float height = 10;

		void Awake ()
		{
				G.i.OnSuperInvul += HandleOnSuperInvul;
		}

		void HandleOnSuperInvul (bool isStart, float duration)
		{
//			if (isStart)
//		{
//
//		}
		}
		public void SetPortalMaskPositions ()
		{
				Vector3 pos = rPortInMask.transform.position;
				pos.x = G.i.RIGHT_SCREEN_EDGE;
				rPortInMask.transform.position = pos;
				pos.x = G.i.LEFT_SCREEN_EDGE; 
				lPortInMask.transform.position = pos;
				

				lPortInMask.size = new Vector2 (Mathf.Abs (G.i.LEFT_SCREEN_EDGE) - portPos, height);
				lPortOutMask.size = new Vector2 (portPos, height);

				rPortInMask.size = new Vector2 (G.i.RIGHT_SCREEN_EDGE - portPos, height);
				rPortOutMask.size = new Vector2 (portPos, height);

				lPortInMask.update ();
				lPortOutMask.update ();
				rPortOutMask.update ();
				rPortInMask.update ();
				
		}



		static MaskManager _instance = null;
		public static MaskManager i {
				get {
						if (!_instance) {
								// check if a MaskManager is already available in the scene graph
								_instance = FindObjectOfType (typeof(MaskManager)) as MaskManager;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("MaskManager");
										_instance = obj.AddComponent<MaskManager> ();
								}
						}

						return _instance;
				}
		}


		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}
}
