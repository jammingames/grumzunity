using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum SpawnState
{
		NullState,
		Intro,
		Only1,
		First2,
		Only2,
		Only3,
		All3,
}

public class EnemySpawnManager : MonoBehaviour
{

		public float g1SpawnChance, g2SpawnChance, g3SpawnChance, g4SpawnChance;

		public GameObject prefab;
		public Transform spawnPos;
		public Transform parent;

		public Vector3 rotation;

		float scaleFactor = 1f;
		//public GameObject[] bosses;	
		public GameObject g1, g2, g3, g4;
		public GameObject g1Effect, g2Effect, g3Effect, g4Effect;
		public float spawnRate, spawnRate2, spawnRate3, spawnRate4, spawnRate5, spawnRate6, spawnRate7, spawnRate8, spawnRate9;
		float origSpawnRate;
		public float distanceFromLastSpawn;
		public int maxEnemies;
		public int currentEnemies;
		public float screenOffset = 0.5f;
		public Vector3 offset;
		public bool g3Spawned = false;

		public int leftG4Spawns, rightG4Spawns, g4Spawns;
		public int g3LeftSpawns, g3RightSpawns;
		//int g4Spawns;
//		int maxLeftG4Spawns = 3;
//		int maxRightG4Spawns = 3;
		int maxG4Spawns = 5;
		


		Vector3 lastSpawnPos = Vector3.zero;
//		SpawnAtPosition spawner;
		[HideInInspector]
		public List<float>
				spawnProbabilities;

		[HideInInspector]
		public List<GameObject>
				spawnPrefabs;
		float topBounds, botBounds;

		//bool canSpawn = true;
		bool tutorialFinished = false;
		public int numSpawns = 0;
		public int numDuplicateSpawns = 0;
		public List<Vector3> usedSpawnPositions;
		public List<SpawnPoint> spawnPoints;
		public int numAvailableSpawnPoints;
		
 

		public bool inTransition = false;

		bool maxedG4Spawn = false;


		
		public delegate void SpawnEvent (SpawnPoint spawnPoint,bool isHeld);
		public event SpawnEvent OnSpawnEnemy;

		public void TriggerSpawnEnemy (SpawnPoint spawnP, bool isHeld)
		{
				if (OnSpawnEnemy != null)
						OnSpawnEnemy (spawnP, isHeld);
		}
			

		// Use this for initialization
		void Awake ()
		{
				tutorialFinished = false;
				origSpawnRate = spawnRate;
				usedSpawnPositions = new List<Vector3> ();
				
				spawnPoints = new List<SpawnPoint> ();
				scaleFactor = 1;
				//spawner = GetComponent<SpawnAtPosition> ();
				spawnState = SpawnState.NullState;
				
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
				G.i.OnBounceIncrease += CheckBounces;
				G.i.OnTutorial += HandleOnTutorial;
				G.i.OnFireBomb += HandleOnFireBomb;
				//G.i.OnBossSpawn += HandleOnBossSpawn;

				OnSpawnStateChange += HandleOnSpawnStateChange;
				botBounds = G.i.BOT_SCREEN_EDGE + screenOffset;
				topBounds = G.i.TOP_SCREEN_EDGE - screenOffset;
		
				spawnProbabilities = new List<float> ();
				spawnProbabilities.Add (g1SpawnChance);
				spawnPrefabs = new List<GameObject> ();
				spawnPrefabs.Add (g1);
				g1.CreatePool (10);
				g2.CreatePool (3);
				g3.CreatePool (3);
				g4.CreatePool (4);
				g1Effect.CreatePool (10);
				g2Effect.CreatePool (5);
				g3Effect.CreatePool (5);
				g4Effect.CreatePool (8);

				
		}

		void HandleOnFireBomb ()
		{
				g4Spawns = 0;
		}

		void HandleOnTutorial (bool isStart)
		{
				if (isStart) {
						StopAllCoroutines ();
				} else if (!isStart) {
						tutorialFinished = true;
						SetState (SpawnState.Intro);
						StartCoroutine (SpawnEnemy ());
						StartCoroutine (SpawnEnemy ());
				}
		}

//		void OnDestroy ()
//		{
//				G.i.OnPlayerDeath -= HandleOnPlayerDeath;
//				G.i.OnBounceIncrease -= CheckBounces;
//				G.i.OnBossSpawn -= HandleOnBossSpawn;
//		}

		void HandleOnPlayerDeath ()
		{
				StopAllCoroutines ();
				gameObject.SetActive (false);
		}

		void HandleOnBossSpawn (bool isStart)
		{
				if (isStart == true)
						StopAllCoroutines ();
				else if (isStart == false) {
						StartCoroutine (SpawnEnemy ());
						StartCoroutine (SpawnEnemy ());
						//StartCoroutine (SpawnEnemy ());

				}
		}

		

		void OnEnable ()
		{
				StopAllCoroutines ();
				spawnRate = origSpawnRate;
				//StartCoroutine (FillSpawnList ());
				
				RemoveExtras ();

				if (tutorialFinished) {
						SetState (SpawnState.Intro);
						StartCoroutine (SpawnEnemy ());
						StartCoroutine (SpawnEnemy ());
				}
			
				//		StartCoroutine (SpawnEnemy ());
		}
		
	
		// Update is called once per frame
		void Update ()
		{
//				if (Input.GetKeyDown (KeyCode.H)) {
//						
//				}


				if (g4Spawns < maxG4Spawns && maxedG4Spawn) {
						maxedG4Spawn = false;
						////debug.log ("adding g4");
						spawnPrefabs.Add (g4);
						spawnProbabilities.Add (g4SpawnChance);
				}
				
				if (OnSpawnStateUpdateHandler != null)
						OnSpawnStateUpdateHandler ();
				if (Input.GetKeyDown (KeyCode.Alpha1)) {
						//				//		var go = 
						GameObject.Instantiate (g1, Vector3.zero, Quaternion.identity);
				}
				if (Input.GetKeyDown (KeyCode.Alpha2)) {
						//		var go = 
						GameObject.Instantiate (g2, Vector3.zero, Quaternion.identity);
				}
				if (Input.GetKeyDown (KeyCode.Alpha3)) {
						GameObject.Instantiate (g3, Vector3.right, Quaternion.identity);
				}
				if (Input.GetKeyDown (KeyCode.Alpha4)) {
						GameObject.Instantiate (g4, Vector3.zero, Quaternion.identity);
						
				}
//				if (Input.GetKeyDown (KeyCode.Alpha5)) {
//						SpawnPrefab (bosses [0]);
//						G.i.TriggerBoss (true);
//				}
		
		
		}

		public void RemoveG3 ()
		{

		}

		public void ReAddG3 ()
		{
				g3Spawned = false;
				if (!spawnPrefabs.Contains (g3))
						spawnPrefabs.Add (g3);
				if (!spawnProbabilities.Contains (g3SpawnChance))
						spawnProbabilities.Add (g3SpawnChance);
		}


		public System.Action<GameState> OnComplete;
		public SpawnState spawnState { get; private set; }
		public SpawnState nextState;
	
		public delegate void OnSpawnStateChangeHandler (SpawnState nextState);
		public event OnSpawnStateChangeHandler OnSpawnStateChange;
	
		public delegate void OnSpawnStateUpdate ();
		public event OnSpawnStateUpdate OnSpawnStateUpdateHandler;


	
		public void SetState (SpawnState spawnState)
		{
		
		
				if (OnSpawnStateChange != null) {
						OnComplete = null;
						OnSpawnStateChange (spawnState);
				}
				this.spawnState = spawnState;
		}
	
		IEnumerator DelayedSetState (float duration, SpawnState state)
		{
				yield return new WaitForSeconds (duration);
				SetState (state);
		}

			

		public void AddG2 ()
		{
				spawnPrefabs.Add (g2);
				spawnProbabilities.Add (g2SpawnChance);


		}

		public void AddG3 ()
		{
				spawnPrefabs.Add (g3);
				spawnProbabilities.Add (g3SpawnChance);
		}

		public void RemoveExtras ()
		{
				if (spawnPrefabs.Count > 0)
						spawnPrefabs.Clear ();
				spawnPrefabs.Add (g1);
				if (spawnProbabilities.Count > 0)
						spawnProbabilities.Clear ();
				spawnProbabilities.Add (g1SpawnChance);
		}

	
		public void HandleOnSpawnStateChange (SpawnState state)
		{
				if (state == spawnState)
						return;
				OnSpawnStateUpdateHandler = null;
				inTransition = true;
				if (state == SpawnState.Intro) {
						//spawnRate = 3;			
						if (spawnPrefabs.Count > 0)
								spawnPrefabs.Clear ();
						spawnPrefabs.Add (g1);
						spawnPrefabs.Add (g2);
						spawnPrefabs.Add (g3);
						
						if (spawnProbabilities.Count > 0)
								spawnProbabilities.Clear ();
						spawnProbabilities.Add (g1SpawnChance);
						spawnProbabilities.Add (g2SpawnChance);
						spawnProbabilities.Add (g3SpawnChance);
						//StartCoroutine (SpawnEnemy ());
			
				} else if (state == SpawnState.Only1) {
						if (spawnPrefabs.Count > 0)
								spawnPrefabs.Clear ();
						if (spawnPrefabs.Contains (g4))
								spawnPrefabs.Remove (g4);
						spawnPrefabs.Add (g1);
						spawnPrefabs.Add (g2);
						spawnPrefabs.Add (g3);
						if (spawnProbabilities.Count > 0)
								spawnProbabilities.Clear ();
						spawnProbabilities.Add (g1SpawnChance);
						spawnProbabilities.Add (g2SpawnChance);
						spawnProbabilities.Add (g3SpawnChance);

				} else if (state == SpawnState.Only2) {
						if (spawnPrefabs.Count > 0)
								spawnPrefabs.Clear ();
						spawnPrefabs.Add (g2);
						if (spawnProbabilities.Count > 0)
								spawnProbabilities.Clear ();
						spawnProbabilities.Add (g2SpawnChance);
				} else if (state == SpawnState.Only3) {
						if (spawnPrefabs.Count > 0)
								spawnPrefabs.Clear ();
						spawnPrefabs.Add (g3);
						if (spawnProbabilities.Count > 0)
								spawnProbabilities.Clear ();
						spawnProbabilities.Add (g3SpawnChance);
				} else if (state == SpawnState.First2) {
						if (spawnPrefabs.Count > 0)
								spawnPrefabs.Clear ();
						spawnPrefabs.Add (g1);
						spawnPrefabs.Add (g2);
						if (spawnProbabilities.Count > 0)
								spawnProbabilities.Clear ();
						spawnProbabilities.Add (g1SpawnChance);
						spawnProbabilities.Add (g2SpawnChance);
						StartCoroutine (SpawnEnemy ());
				} else if (state == SpawnState.All3) {
						if (spawnPrefabs.Count > 0)
								spawnPrefabs.Clear ();
						spawnPrefabs.Add (g1);
						spawnPrefabs.Add (g2);
						spawnPrefabs.Add (g3);
						if (spawnProbabilities.Count > 0)
								spawnProbabilities.Clear ();
						spawnProbabilities.Add (g1SpawnChance);
						spawnProbabilities.Add (g2SpawnChance);
						spawnProbabilities.Add (g3SpawnChance);

						StartCoroutine (SpawnEnemy ());
				}
			
				
		
				//	Debug.Log ("Changing Spawn state to: " + state);
		}
	
		
		IEnumerator SpawnEnemy ()
		{
				GameObject nextSpawn;

				nextSpawn = g1;
				//Vector3 spawnPos = GetSpawnPosition ();
				float tempSpawn = Random.Range (spawnRate / 6, spawnRate);
				tempSpawn += 0.1f;
		
				yield return new WaitForSeconds (tempSpawn);
				if (spawnPoints.Count > 0) {

		

						int nextSpawnIndex = GetNextSpawnPoint (spawnPoints);
						//	//debug.log ("next spawn index: " + nextSpawnIndex);
						while (spawnPoints[nextSpawnIndex].canSpawn == false) {
								spawnPoints.RemoveAt (nextSpawnIndex);
								nextSpawnIndex = GetNextSpawnPoint (spawnPoints);
								yield return null;
						
						}
						Vector3 spawnPos = spawnPoints [nextSpawnIndex].transform.position;
						//spawnPoints [nextSpawnIndex].UseSpawn ();
						
//						if (g3Spawned == true) {
//								spawnPrefabs.Remove (g3);
//								spawnProbabilities.Remove (g3SpawnChance);
//						
//						}
			
						if (spawnPrefabs.Count > 1) {
								if (g4Spawns >= maxG4Spawns) {
//										//debug.log ("removing g4");
										spawnPrefabs.Remove (g4);
										spawnProbabilities.Remove (g4SpawnChance);
										maxedG4Spawn = true;
								}
								
								nextSpawn = spawnPrefabs [Choose (spawnProbabilities)];
						} else
								nextSpawn = spawnPrefabs [0];


						

//						//debug.log ("Spawning:  " + nextSpawn);
						lastSpawnPos = spawnPos;
				
				
						//yield return null;

				
						//nextSpawn = g1;
				

						//SpawnProxy (spawnPos);
//				if (!usedSpawnPositions.Contains (spawnPos)) {
//						//numDuplicateSpawns++;
//						usedSpawnPositions.Add (spawnPos);
//				}
						numSpawns++;

						SpawnPrefab (nextSpawn, spawnPos, scaleFactor, spawnPoints [nextSpawnIndex]);
				}
				yield return new WaitForSeconds (tempSpawn);
				StartCoroutine (SpawnEnemy ());
				
		}

		int GetNextSpawnPoint (List<SpawnPoint> spawnPointList)
		{
				int index = (spawnPointList.Count > 0) ? spawnPointList.Count - 1 : 0;
				return Random.Range (0, index);
		}

		IEnumerator FillSpawnList ()
		{
						
				usedSpawnPositions.Clear ();
				Vector3 nextPos = GetSpawnPosition ();
				int numPositions = 14;

				while (numPositions > 0) {
						////debug.log ("test");
						nextPos = GetSpawnPosition ();
						if (!usedSpawnPositions.Contains (nextPos)) {
								numPositions--;
					
								usedSpawnPositions.Add (nextPos);
							
						}
						yield return null;
				}
		}

		void OnDrawGizmosSelected ()
		{
				Gizmos.color = Color.yellow;
				if (usedSpawnPositions.Count > 0) {
						for (int i = 0; i < usedSpawnPositions.Count; i++) {
								Gizmos.DrawSphere (usedSpawnPositions [i], 0.5f);
	
						}
				}
		}
	
		Vector3 GetSpawnPosition ()
		{
				Vector3 spawnPos = Vector3.zero;
				if (lastSpawnPos.y > topBounds - distanceFromLastSpawn) {
						spawnPos.y = Random.Range (botBounds, lastSpawnPos.y - distanceFromLastSpawn);
			
				} else if (lastSpawnPos.y < botBounds + distanceFromLastSpawn) {
						spawnPos.y = Random.Range (lastSpawnPos.y + distanceFromLastSpawn, topBounds);
				} else {
						if (Random.Range (2, 1001) % 2 == 0) 
								spawnPos.y = lastSpawnPos.y + distanceFromLastSpawn;
						else
								spawnPos.y = lastSpawnPos.y - distanceFromLastSpawn;
				}
				if (Random.Range (2, 1001) % 2 == 0) {
						spawnPos.x = -10;
			
				} else {
						spawnPos.x = 10;
				}
//				Vector3 tempPos = spawnPos;
//				for (int i = 0; i < usedSpawnPositions.Count; i++) {
//						if (usedSpawnPositions [i] == tempPos) {
//								//numDuplicateSpawns++;
//								//debug.log ("SAME SPAWN POS");
//								return GetSpawnPosition ();
//						}
//				}


				lastSpawnPos = spawnPos;
				return spawnPos;
		}


		int Choose (List<float> probs)
		{
				
				float total = 0;
		
				for (int i = 0; i< probs.Count; i++) {
						total += probs [i];
				}
		
				float randomPoint = Random.value * total;
		
				for (int i = 0; i < probs.Count; i++) {
						if (randomPoint < probs [i])
								return i;
						else
								randomPoint -= probs [i];
				}
		
				return probs.Count - 1;
		}



	
		void CheckBounces (int numBounces)
		{
				if (numBounces > 0 && numBounces <= 2) {
						SetState (SpawnState.Only1);
				} else if (numBounces == 5) {
						spawnRate = spawnRate2;
				} else if (numBounces == 10) {
						StartCoroutine (SpawnEnemy ());
						spawnRate = spawnRate3;
				} else if (numBounces == 15) {
						spawnPrefabs.Add (g4);
						spawnProbabilities.Add (g4SpawnChance);
						spawnRate = spawnRate4;
				} else if (numBounces == 20) {
						spawnRate = spawnRate5;
				} else if (numBounces == 30) {
						spawnRate = spawnRate6;
				} else if (numBounces == 35)
						spawnRate = spawnRate7;
				else if (numBounces == 45)
						spawnRate = spawnRate8;
				else if (numBounces == 65)
						spawnRate = spawnRate9;


//				if (numBounces == 10) {
//						SpawnPrefab (bosses [0]);
//						G.i.TriggerBoss (true);
//				} else if (numBounces == 30) {
//						SpawnPrefab (bosses [1]);
//						G.i.TriggerBoss (true);
//				} else if (numBounces == 60) {
//						SpawnPrefab (bosses [2]);
//						G.i.TriggerBoss (true);
//				} else if (numBounces == 100) {
//						SpawnPrefab (bosses [3]);
//						G.i.TriggerBoss (true);
//				} else if (numBounces == 150) {
//						SpawnPrefab (bosses [4]);
//						G.i.TriggerBoss (true);
//				}
//


		}
	
		static EnemySpawnManager _instance = null;
		public static EnemySpawnManager instance {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(EnemySpawnManager)) as EnemySpawnManager;
				
								// nope, create a new one
								
						}
			
						return _instance;
				}
		}
	
	
		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}


		void SpawnProxy (Vector3 pos)
		{
				GameObject go = new GameObject ("SpawnProxy"); 
				go.transform.position = pos;
				go.AddComponent<SpawnProxy> ();
		}

		public void SpawnPrefab ()
		{
		
				Vector3 pos;
				if (spawnPos != null) {
						pos = spawnPos.localPosition + offset;
			
			
				} else {
			
						pos = transform.localPosition + offset;
				}
				var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				go.transform.parent = SceneRef.i.sceneStartGame.transform;
				//var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				if (parent != null)
						go.transform.parent = parent;
		}
	
	
		public void SpawnPrefab (GameObject prefab, float scaleFactor)
		{
				//				if (prefab == null)
				//						prefab = UIManager.instance.characterPrefabs [UIManager.instance.spriteIndex];
				Vector3 pos;
				if (spawnPos != null) {
						pos = spawnPos.localPosition + offset;
			
			
				} else {
			
						pos = transform.localPosition + offset;
				}
				var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
//				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				go.transform.localScale *= scaleFactor;
				go.transform.parent = SceneRef.i.sceneStartGame.transform;
				if (parent != null)
						go.transform.parent = parent;
		}
	
	
		public void SpawnPrefab (GameObject prefab)
		{
				//				if (prefab == null)
				//						prefab = UIManager.instance.characterPrefabs [UIManager.instance.spriteIndex];
				Vector3 pos;
				if (spawnPos != null) {
						pos = spawnPos.localPosition + offset;
			
			
				} else {
			
						pos = transform.localPosition + offset;
				}
//				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				go.transform.parent = SceneRef.i.sceneStartGame.transform;
				if (parent != null)
						go.transform.parent = parent;
		}
	
		public void SpawnPrefab (GameObject prefab, Vector3 pos)
		{
		
				pos += offset;
		
//				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				go.transform.parent = SceneRef.i.sceneStartGame.transform;
				if (parent != null)
						go.transform.parent = parent;
		}	
	
		public void SpawnPrefab (GameObject prefab, Vector3 pos, float scaleFactor)
		{
		
				pos += offset;
		
//				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				go.transform.localScale *= scaleFactor;
				go.transform.parent = SceneRef.i.sceneStartGame.transform;
				if (parent != null)
						go.transform.parent = parent;
		}	
		public void SpawnPrefab (GameObject prefab, Vector3 pos, float scaleFactor, SpawnPoint spawnIndex)
		{
		
				pos += offset;
		
				//				var go = prefab.Spawn (pos, Quaternion.Euler (rotation));
				var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				go.transform.localScale *= scaleFactor;
				go.transform.parent = SceneRef.i.sceneStartGame.transform;
				if (!go.GetComponent<EnemySpawnHolder> ())
						go.AddComponent<EnemySpawnHolder> ();
				go.GetComponent<EnemySpawnHolder> ().HoldSpawn (spawnIndex);
//				go.GetComponent<EnemySpawnHolder> ().spawnPos.canSpawn = false;
//				spawnIndex.canSpawn = false;
				
				if (parent != null)
						go.transform.parent = parent;
		}

}
