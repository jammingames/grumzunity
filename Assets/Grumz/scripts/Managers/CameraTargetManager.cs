﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraTargetManager : MonoBehaviour
{
	
	
		SideScrollCameraFollow camFollow;
	
		public List<Transform> targets;
		

		
		int index;
	
		
	
		void Awake ()
		{
				G.i.OnStateChange += HandleOnStateChange;
				camFollow = Camera.main.GetComponent<SideScrollCameraFollow> ();
				
				
		}
	
		// Use this for initialization
		void Start ()
		{
				index = 0;
				//ChangeCamTarget (targets [0]);
		}
	
	
	
		public void HandleOnStateChange (GameState state)
		{
				if (G.i.gameState == GameState.Intro) {
						ChangeCamTarget (targets [0]);
				} else if (G.i.gameState == GameState.MainMenu) {
			
				} else if (G.i.gameState == GameState.Game) {
						ChangeCamTarget (targets [1]);
						//		ChangeCamTarget (player);
			
				} else if (G.i.gameState == GameState.End) {
						ChangeCamTarget (targets [2]);
						//		ChangeCamTarget ();
			
				}
		
		
		}
	
	
		public void ChangeCamTarget ()
		{
				if (index >= targets.Count - 1) {
						index = 0;
				} else {
						index++;
				}
				camFollow.ChangeTarget (targets [index], 1.0f, G.i.FinishedStateChange);
		}
	
	
	
		public void ChangeCamTarget (int targetIndex)
		{
				if (targets.Count > targetIndex)
						index = targetIndex;
		
				camFollow.ChangeTarget (targets [index], 1.0f, G.i.FinishedStateChange);
		}
	
	
	
		public void ChangeCamTarget (Transform target)
		{
				if (targets.Contains (target))
						index = targets.IndexOf (target);
				camFollow.ChangeTarget (target, 1.0f, G.i.FinishedStateChange);
		}
	

		static CameraTargetManager _instance = null;
		public static CameraTargetManager instance {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(CameraTargetManager)) as CameraTargetManager;
				
								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("CameraTargetManager");
										_instance = obj.AddComponent<CameraTargetManager> ();
								}
						}
			
						return _instance;
				}
		}
	
	
		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}
}
