﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PowerupSpawnManager : MonoBehaviour
{
	
		public float g1SpawnChance, g2SpawnChance;
	
		public GameObject g1, g2;
		public float spawnRate;
		public float distanceFromLastSpawn;
		public int maxEnemies;
		public int currentEnemies;
		public float offset = 0.5f;
		Vector3 lastSpawnPos = Vector3.zero;
		SpawnAtPosition spawner;
		List<float> spawnProbabilities;
		List<GameObject> spawnPrefabs;
		float topBounds, botBounds;

//		int index = 0;
	
	
		public bool inTransition = false;
		bool tutorialFinished = false;
	
	
		// Use this for initialization
		void Awake ()
		{
				tutorialFinished = false;
				spawner = GetComponent<SpawnAtPosition> ();
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
				G.i.OnPlayerInvul += HandleOnPlayerInvul;
				G.i.OnTutorial += HandleOnTutorial;

		
		}

		void HandleOnTutorial (bool isStart)
		{
				if (isStart) {
						StopAllCoroutines ();
				} else if (!isStart) {
						tutorialFinished = true;
						float tempSpawn = Random.Range (spawnRate, spawnRate * 2);
			
						StartCoroutine (Auto.Wait (tempSpawn, () => {
								StartCoroutine (SpawnPowerup ());}));
				}
			
		}

		
	
		void Start ()
		{
		
	
				botBounds = G.i.BOT_SCREEN_EDGE + offset;
				topBounds = G.i.TOP_SCREEN_EDGE - offset;
		

		}


		void OnEnable ()
		{

				spawnProbabilities = new List<float> ();
		
				spawnProbabilities.Add (g1SpawnChance);
				spawnProbabilities.Add (g2SpawnChance);
		
				spawnPrefabs = new List<GameObject> ();
				spawnPrefabs.Add (g1);
				spawnPrefabs.Add (g2);

				if (tutorialFinished) {
						float tempSpawn = Random.Range (spawnRate, spawnRate * 2);
			
						StartCoroutine (Auto.Wait (tempSpawn, () => {
								StartCoroutine (SpawnPowerup ());}));
				}
		}
	
	
		// Update is called once per frame
		void Update ()
		{


				if (Input.GetKeyDown (KeyCode.Alpha5))
						SpawnTestPowerup (0);
				if (Input.GetKeyDown (KeyCode.Alpha6))
						SpawnTestPowerup (1);
				
		}

	
		void HandleOnPlayerInvul (bool isStart)
		{
				if (isStart) {
						spawnProbabilities.Remove (g2SpawnChance);
						spawnPrefabs.Remove (g2);
				} else {
						spawnProbabilities.Add (g2SpawnChance);
						spawnPrefabs.Add (g2);
				}
		}


		void HandleOnPlayerDeath ()
		{
				StopAllCoroutines ();
				gameObject.SetActive (false);
		}


	
		void SpawnTestPowerup (int index)
		{
				GameObject nextSpawn;
				Vector3 spawnPos = Vector3.zero;
				if (lastSpawnPos.y > topBounds - distanceFromLastSpawn) {
						spawnPos.y = Random.Range (botBounds, lastSpawnPos.y - distanceFromLastSpawn);
			
				} else if (lastSpawnPos.y < botBounds + distanceFromLastSpawn) {
						spawnPos.y = Random.Range (lastSpawnPos.y + distanceFromLastSpawn, topBounds);
				} else {
						if (Random.Range (2, 1001) % 2 == 0) 
								spawnPos.y = lastSpawnPos.y + distanceFromLastSpawn;
						else
								spawnPos.y = lastSpawnPos.y - distanceFromLastSpawn;
				}
				if (Random.Range (2, 1001) % 2 == 0) {
						spawnPos.x = -10;
			
				} else {
						spawnPos.x = 10;
				}
		
		

				nextSpawn = spawnPrefabs [index];

		
				//List<float> spawnChance = new List<float> ();
		
		
				spawner.SpawnPrefab (nextSpawn, spawnPos);
				//				//debug.log ("Spawning:  " + nextSpawn + "at: " + spawnPos);
		
				lastSpawnPos = spawnPos;


		}

	


	
		IEnumerator SpawnPowerup ()
		{
				GameObject nextSpawn;
				Vector3 spawnPos = Vector3.zero;
				if (lastSpawnPos.y > topBounds - distanceFromLastSpawn) {
						spawnPos.y = Random.Range (botBounds, lastSpawnPos.y - distanceFromLastSpawn);
			
				} else if (lastSpawnPos.y < botBounds + distanceFromLastSpawn) {
						spawnPos.y = Random.Range (lastSpawnPos.y + distanceFromLastSpawn, topBounds);
				} else {
						if (Random.Range (2, 1001) % 2 == 0) 
								spawnPos.y = lastSpawnPos.y + distanceFromLastSpawn;
						else
								spawnPos.y = lastSpawnPos.y - distanceFromLastSpawn;
				}
				if (Random.Range (2, 1001) % 2 == 0) {
						spawnPos.x = -10;
			
				} else {
						spawnPos.x = 10;
				}
		
		
				if (spawnPrefabs.Count > 1)
						nextSpawn = spawnPrefabs [Choose (spawnProbabilities)];
				else
						nextSpawn = spawnPrefabs [0];
		
				//List<float> spawnChance = new List<float> ();
			
				
				spawner.SpawnPrefab (nextSpawn, spawnPos);
//				//debug.log ("Spawning:  " + nextSpawn + "at: " + spawnPos);
			
				lastSpawnPos = spawnPos;
				float tempSpawn = Random.Range (spawnRate, spawnRate * 6);
				yield return StartCoroutine (Auto.Wait (tempSpawn, () => {
						StartCoroutine (SpawnPowerup ());}));
		}
	
	
		int Choose (List<float> probs)
		{
		
				float total = 0;
		
				for (int i = 0; i< probs.Count; i++) {
						total += probs [i];
				}
		
				float randomPoint = Random.value * total;
		
				for (int i = 0; i < probs.Count; i++) {
						if (randomPoint < probs [i])
								return i;
						else
								randomPoint -= probs [i];
				}
		
				return probs.Count - 1;
		}
	
	
		public void ShieldSpawn (bool shouldSpawn)
		{
				if (shouldSpawn) {
						if (!spawnProbabilities.Contains (g1SpawnChance))
								spawnProbabilities.Add (g1SpawnChance);
						if (!spawnPrefabs.Contains (g1))
								spawnPrefabs.Add (g1);
//						if (spawnProbabilities.Count > 0) {
//
//								spawnProbabilities.Clear ();
//								spawnProbabilities.Add (g1SpawnChance);
//								spawnProbabilities.Add (g2SpawnChance);
//						}
				} else {
						if (spawnProbabilities.Count > 0 && spawnProbabilities.Contains (g1SpawnChance)) {

								spawnProbabilities.Remove (g1SpawnChance);
						}
						if (spawnPrefabs.Count > 0 && spawnPrefabs.Contains (g1))
								spawnPrefabs.Remove (g1);

				}
		}
	
		void CheckBounces (int numBounces)
		{
				
		
		}
	
		static PowerupSpawnManager _instance = null;
		public static PowerupSpawnManager instance {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(PowerupSpawnManager)) as PowerupSpawnManager;
				
								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("PowerupSpawnManager");
										_instance = obj.AddComponent<PowerupSpawnManager> ();
								}
						}
			
						return _instance;
				}
		}
	
	
		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}
	
}
