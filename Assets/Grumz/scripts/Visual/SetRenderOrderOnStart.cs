﻿using UnityEngine;
using System.Collections;
using k;

public class SetRenderOrderOnStart : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
				
				if (transform.position.x > 0) {
						GetComponent<SpriteRenderer> ().sortingLayerName = "Right";

				} else {
						GetComponent<SpriteRenderer> ().sortingLayerName = "Left";
				
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
