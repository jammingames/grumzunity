﻿using UnityEngine;
using System.Collections;

public class SetMaskOnStart : MonoBehaviour
{

		public Transform leftMask, rightMask;
		SpriteMask lMask, rMask;

		

		// Use this for initialization
		

		void Start ()
		{
				leftMask = SceneRef.i.maskLeft.transform;
				rightMask = SceneRef.i.maskRight.transform;
				lMask = SceneRef.i.lMask;
				rMask = SceneRef.i.rMask;
				if (transform.position.x < 0) {
						transform.parent = leftMask;
						lMask.updateSprites (transform);
				} else {
						transform.parent = rightMask;
						rMask.updateSprites (transform);
				}
				
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
