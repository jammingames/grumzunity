﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class AdjustImageWidthForScreenSize : MonoBehaviour
{
		
		public float factor;
		
		public float threshold = 340;


		// Use this for initialization
		void Start ()
		{
				RectTransform trans = GetComponent<RectTransform> ();
				float screenWidth = Screen.width;
				if (screenWidth <= threshold) {
						Vector2 v = trans.sizeDelta;
						v.x *= factor;
						trans.sizeDelta = v;
						print (trans.sizeDelta.x);
						//trans.sizeDelta
				}
#if UNITY_IPHONE
				Vector2 w = trans.sizeDelta;
				w.x *= (factor * 1.04f);
				trans.sizeDelta = w;
#endif
						
	
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
