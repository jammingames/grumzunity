using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlickerChildren : MonoBehaviour
{

		//When taking damage, wherever that is
		float flickerTimeout = 3; //flicker for 3 seconds when hit
	
	
		//In Update() somewhere




		// Awake is called when the script instance
		// is being loaded.
		void Update ()
		{
				flickerTimeout -= Time.deltaTime;
				foreach (Transform child in transform) {
				
						child.GetComponent<Flicker> ().animate = flickerTimeout > 0;
				}
		}
	
}
