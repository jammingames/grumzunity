﻿/*
	SetRenderQueue.cs
 
	Sets the RenderQueue of an object's materials on Awake. This will instance
	the materials, so the script won't interfere with other renderers that
	reference the same materials.
*/

using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Rendering/SetRenderQueue")]

public class SetRenderQueue : MonoBehaviour
{
	
		[SerializeField]
		protected int[]
				m_queues = new int[]{3000};
	
		protected void Awake ()
		{
				Material[] materials = new Material[2];
				materials [0] = GetComponent<Image> ().material;
				
				materials [0].renderQueue = m_queues [0];
				
		}
}