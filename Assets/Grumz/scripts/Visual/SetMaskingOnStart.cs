﻿using UnityEngine;
using System.Collections;

public class SetMaskingOnStart : MonoBehaviour
{


		Material mat;
		public float forwards = 0;
		// Use this for initialization
		void Start ()
		{
				mat = GetComponent<SpriteRenderer> ().material;
				mat.SetFloat ("_Forwards", forwards);
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
