﻿using UnityEngine;
using System.Collections;


public class AdjustSpriteWidthForScreenSize : MonoBehaviour
{
	
		public float factor;
	
		public float threshold = 340;
	
	
		// Use this for initialization
		void Start ()
		{
				//RectTransform trans = GetComponent<RectTransform> ();
#if UNITY_ANDROID
				float screenWidth = Screen.width;
				if (screenWidth <= threshold) {
						Vector3 v = transform.localScale;
						v.x *= factor;
						transform.localScale = v;
						
						//trans.sizeDelta
				}
#endif
		
		
		}
	
		// Update is called once per frame
		void Update ()
		{
		
		}
}
