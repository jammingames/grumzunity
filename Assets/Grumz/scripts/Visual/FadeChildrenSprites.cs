﻿
using UnityEngine;
using System.Collections;

public class FadeChildrenSprites : MonoBehaviour
{

		SpriteRenderer[] childSprites;

		void Awake ()
		{
				childSprites = GetComponentsInChildren<SpriteRenderer> ();
		}

		public void ToggleSprites (bool shouldEnable)
		{
				if (shouldEnable) {
						for (int i = 0; i < childSprites.Length; i++) {
								childSprites [i].enabled = false;
						}
				} else {
						for (int i = 0; i < childSprites.Length; i++) {
								childSprites [i].enabled = true;
						}
				}
		}

		public void ToggleFadeSprites (bool shouldEnable, float duration)
		{
				if (shouldEnable) {
						for (int i = 0; i < childSprites.Length; i++) {
								StartCoroutine (childSprites [i].FadeSprite (1, duration, 0, null));
						}
				} else {
						for (int i = 0; i < childSprites.Length; i++) {
								StartCoroutine (childSprites [i].FadeSprite (0, duration, 0, null));
						}
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
