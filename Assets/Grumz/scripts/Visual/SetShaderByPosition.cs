﻿using UnityEngine;
using System.Collections;
using k;

public class SetShaderByPosition : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
				GetComponent<Renderer> ().material.SetFloat ("_MaskPosition", 0);
				if (transform.position.x > 0)
						GetComponent<Renderer> ().material.SetFloat ("_Forwards", 1);
				else
						GetComponent<Renderer> ().material.SetFloat ("_Forwards", 0);
		}

}
