﻿using UnityEngine;
using System.Collections;

public class TrailShaderPosition : MonoBehaviour
{


		Material mat;
		// Use this for initialization
		void Start ()
		{
				mat = GetComponent<Renderer> ().material;
		}
	
		// Update is called once per frame
		void Update ()
		{
				mat.SetFloat ("_MaskPosition", 0);
				if (transform.position.x < 0f) {
						mat.SetFloat ("_Forwards", 0);
						//gameObject.layer = Layers.RIGHT;
				} else if (transform.position.x > 0f) {
						mat.SetFloat ("_Forwards", 1);
						//gameObject.layer = Layers.LEFT;
				}
		}
}
