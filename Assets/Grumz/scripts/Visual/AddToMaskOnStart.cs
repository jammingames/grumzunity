﻿using UnityEngine;
using System.Collections;

public class AddToMaskOnStart : MonoBehaviour
{

		public SpriteMask mask;
		
		public Transform objectToMask;

		// Use this for initialization
		void Awake ()
		{
				mask = GetComponentInChildren<SpriteMask> ();
		}

		void Start ()
		{
				mask.updateSprites (objectToMask);
//				StartCoroutine (Auto.Wait (0.1f, () => {
//						mask.updateMask ();
//				}));
		}

		public void SetMask (Transform trans)
		{
				objectToMask = trans;
				//mask.updateSprites()
				mask.updateSprites (objectToMask);
				print ("setting " + gameObject.name + " mask to: " + objectToMask);
				
		}

		void Update ()
		{
				//mask.updateSprites (objectToMask);
		}
		
}
