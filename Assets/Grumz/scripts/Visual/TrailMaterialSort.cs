﻿using UnityEngine;
using System.Collections;
using k;

public class TrailMaterialSort : MonoBehaviour
{

		void Start ()
		{
				//Change Foreground to the layer you want it to display on 
				//You could prob. make a public variable for this
				if (transform.position.x < 0f) {
						gameObject.layer = Layers.LEFT;
				} else if (transform.position.x > 0f) {
						gameObject.layer = Layers.RIGHT;
				}
				
				GetComponent<TrailRenderer> ().sortingLayerName = "Background";
				GetComponent<TrailRenderer> ().sortingOrder = -5;
				//renderer.sortingLayerName = "Background";
		}

}
