﻿using UnityEngine;
using System.Collections;

public class ParticleSort : MonoBehaviour
{
		public string layer;
		void Start ()
		{
				//Change Foreground to the layer you want it to display on 
				//You could prob. make a public variable for this
				if (layer == "")
						GetComponent<ParticleSystem> ().GetComponent<Renderer>().sortingLayerName = "MidBackground";
				else 
						GetComponent<ParticleSystem> ().GetComponent<Renderer>().sortingLayerName = layer;
		}
}
