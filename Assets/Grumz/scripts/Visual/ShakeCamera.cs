﻿using UnityEngine;
using System.Collections;

public class ShakeCamera : MonoBehaviour, IRequireUserInput
{
		public IUserInputProxy InputProxy { get; set; }

		// Transform of the camera to shake. Grabs the gameObject's transform
		// if null.
//		public Transform camTransform;
//	
//		// How long the object should shake for.
//		public float shake = 0f;
//	
//		// Amplitude of the shake. A larger value shakes the camera harder.
//		public float shakeAmount = 0.7f;
//		public float decreaseFactor = 1.0f;
//	
//		Vector3 originalPos;

		public Vector3 shakeAmt;
		public float duration;
	
		void Awake ()
		{

//				if (camTransform == null) {
//						camTransform = GetComponent (typeof(Transform)) as Transform;
//				}
		}
	
		void OnEnable ()
		{
				StartCoroutine (transform.Shake (shakeAmt, duration, null));
//				originalPos = camTransform.localPosition;
		}

		void Update ()
		{
				bool isDown = InputProxy.GetMouseButton (0);
				if (isDown)
						StartShake ();
				else {
						StopShake ();
				}
		}

		public void StartShake ()
		{
				StartCoroutine (transform.Shake (shakeAmt, duration, null));
				//		shak
		}

		public void StopShake ()
		{
				StopCoroutine (transform.Shake (shakeAmt, duration, null));
		}

}