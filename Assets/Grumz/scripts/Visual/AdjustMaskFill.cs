﻿using UnityEngine;
using System.Collections;

public class AdjustMaskFill : MonoBehaviour
{


		public Material mat;
		float edge;
		// Use this for initialization
		void Start ()
		{
				Time.timeScale = 0.3f;
				edge = -GetComponent<Renderer>().bounds.extents.x;
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (transform.position.x + edge <= 0)
						mat.SetFloat ("_MinX", 1 - transform.position.x);	
		}
}
