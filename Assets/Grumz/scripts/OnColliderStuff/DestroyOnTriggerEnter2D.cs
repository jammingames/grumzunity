﻿using UnityEngine;
using System.Collections;

public class DestroyOnTriggerEnter2D : MonoBehaviour
{
		public GameObject effect;
		public GameObject bombEffect;
		public AudioSource explosionSnd;
		
		public SpriteRenderer trail;
		//public bool hasDelay = false;
		public float delay = 1;
		
		
		public bool isPowerup = false;

		public Transform glowSprite;

		SpriteRenderer spr;

//		HandleChildrenRandomForce[] forceHandlers;
		
		// Use this for initialization

		void OnEnable ()
		{
				if (GetComponent<SpriteRenderer> ())
						spr = GetComponent<SpriteRenderer> ();
//				forceHandlers = GetComponentsInChildren<HandleChildrenRandomForce> (true);
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
		}

		void OnDisable ()
		{
				StopAllCoroutines ();
				if (G.i != null)
						G.i.OnPlayerDeath -= HandleOnPlayerDeath;
				StopAllCoroutines ();
		}

		void HandleOnPlayerDeath ()
		{
				if (trail != null) {
						StartCoroutine (trail.FadeSprite (0, 0.5f, 0, null));
				}
		
				StartCoroutine (Auto.Wait (1.5f, () => {

						gameObject.Recycle ();
				}));
//				Destroy (gameObject, 1.5f);

		}
		

		void OnTriggerEnter2D (Collider2D col)
		{

				////debug.log (gameObject.name + " collided with  " + col.name);
								


			
				
//				//debug.log (gameObject.name + "  collided with: " + col);
				if (col.GetComponent<EndGameOnTriggerEnter2D> () && !isPowerup) {
						
//						if (forceHandlers.Length > 0) {
//								for (int i = 0; i < forceHandlers.Length; i++)
//										forceHandlers [i].AddForces ();
//						}
						

						if (col.GetComponent<EndGameOnTriggerEnter2D> ().invulnerable) {
								if (explosionSnd != null)
										AudioManager.instance.PlayAudio (explosionSnd);
						}
//								//debug.log (gameObject.name + " Collided with invul player");		
//								if (col.gameObject.tag == "Player") {
						if (trail != null) {
								StartCoroutine (trail.FadeSprite (0, 0.5f, 0, null));
						}
						if (spr != null)
								spr.enabled = false;
						

										
						if (effect != null) {

								if (transform.localPosition.x > 0) {
										effect.Spawn (transform.position, transform.rotation);
										//Instantiate (effect, transform.position, transform.rotation);
								} else 
										effect.Spawn (transform.position, transform.rotation);
//										Instantiate (effect, transform.position, transform.rotation);
						}
						//}
						if (GetComponentInChildren<EnableImpact> ()) {
								GetComponentInChildren<EnableImpact> ().OnImpact ();
						}
								
						//}
						
						StartCoroutine (Auto.Wait (delay, () => {
//								//debug.log ("destroying: " + gameObject.name);
								gameObject.Recycle ();
//								Destroy (gameObject);
						}));
						
						
				} else if (col.tag == "bomb" && !isPowerup) {
						//		//debug.log (gameObject.name + " hit bomb");
						if (trail != null) {
								StartCoroutine (trail.FadeSprite (0, 0.5f, 0, null));
						}
						if (effect == null && bombEffect != null) {
								if (spr != null)
										spr.enabled = false;
				
								bombEffect.Spawn (transform.position, transform.rotation);
//								Instantiate (bombEffect, transform.position, transform.rotation);
								


								if (GetComponentInChildren<EnableImpact> ()) {
										GetComponentInChildren<EnableImpact> ().OnImpact ();
								}
								StartCoroutine (Auto.Wait (delay, () => {
										gameObject.Recycle ();
//										Destroy (gameObject);
								}));
						}
		
						if (effect != null) {
								if (spr != null)
										spr.enabled = false;
								
								effect.Spawn (transform.position, transform.rotation);
								//Instantiate (effect, transform.position, transform.rotation);
								if (GetComponentInChildren<EnableImpact> ()) {
										GetComponentInChildren<EnableImpact> ().OnImpact ();
								}
						}
						BroadcastMessage ("HitPlayer", SendMessageOptions.DontRequireReceiver);
						if (gameObject.activeInHierarchy) {

								StartCoroutine (Auto.Wait (delay, () => {
										gameObject.Recycle ();
//								Destroy (gameObject);
								}));
						} else {
								gameObject.Recycle ();
						}
				}

				if (col.GetComponent<EndGameOnTriggerEnter2D> ()) {
			
						BroadcastMessage ("HitPlayer", SendMessageOptions.DontRequireReceiver);
						return;
			
			
				}
//				//debug.log ("destroying object at: " + G.i.elapsedTime + delay);
		}
}
