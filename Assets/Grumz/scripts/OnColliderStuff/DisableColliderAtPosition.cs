﻿using UnityEngine;
using System.Collections;

public class DisableColliderAtPosition : MonoBehaviour
{

		public Vector3 position;
		public Collider2D col;
		public Vector3 boundsPos;

		bool rightSide;

		// Use this for initialization
		void Start ()
		{
				col = GetComponent<Collider2D> ();
				if (col.bounds.center.x > 0)
						rightSide = true;
				
		}
	
		// Update is called once per frame
		void Update ()
		{
				boundsPos = col.bounds.min;
				if (rightSide && col.bounds.center.x <= 0) {
						col.enabled = false;

						enabled = false;
						GetComponent<RigidBodyTowardsTransform> ().enabled = false;
				} else if (!rightSide && col.bounds.center.x >= 0) {
						col.enabled = false;

						enabled = false;
						GetComponent<RigidBodyTowardsTransform> ().enabled = false;
				}
		}
}
