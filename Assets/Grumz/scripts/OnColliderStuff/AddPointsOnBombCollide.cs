﻿using UnityEngine;
using System.Collections;
using k;

public class AddPointsOnBombCollide : MonoBehaviour
{

		bool hasCollided = false;
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		void OnTriggerEnter2D (Collider2D col)
		{
				if (col.tag == Tags.BOMB && hasCollided == false) {
						G.i.TriggerBounceAdd (1);
						hasCollided = true;
				}
		}
}
