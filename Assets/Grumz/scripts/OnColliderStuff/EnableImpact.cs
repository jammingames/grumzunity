﻿using UnityEngine;
using System.Collections;

public class EnableImpact : MonoBehaviour
{

		Rigidbody2D[] rBodies;

		// Use this for initialization
		void Start ()
		{
				rBodies = GetComponentsInChildren<Rigidbody2D> ();
		}
	
		

		public void OnImpact ()
		{
				////debug.log ("on impact trigger");
				for (int i = 0; i < rBodies.Length; i++) {
						rBodies [i].isKinematic = false;
						//		Destroy (rBodies [i].gameObject, 3);
				}
		}
}
