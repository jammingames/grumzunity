﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnableTimeAttackUIOnGameOver : MonoBehaviour
{

		public GameObject timeAttackObj;
		bool shouldEnable = false;
		// Use this for initialization
		void Awake ()
		{
//				ScoreManager.instance.OnTimeAttackScore += HandleOnTimeAttackScore;
//				G.i.OnStateChange += HandleOnStateChange;
				timeAttackObj.SetActive (false);
		}

		void Start ()
		{
				timeAttackObj.SetActive (false);
		}

		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == GameState.End && shouldEnable)
						timeAttackObj.SetActive (true);
				else if (nextState != GameState.End)
						timeAttackObj.SetActive (false);

		}

		void HandleOnTimeAttackScore (float time)
		{
				print ("TIME ATTACK SCORE ACHIEVED");
				var t = timeAttackObj.GetComponentsInChildren<Text> (true);
				for (int i = 0; i< t.Length; i++) {
						t [i].text = time.ToString ();
						string timeString = string.Format ("{0:0.0}s", t);
						if (i == 0)
								t [i].text = timeString;
						else {
								timeString = string.Format ("{0:0.0}", t);
								t [i].text = timeString;
						}
		
				}
				shouldEnable = true;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
