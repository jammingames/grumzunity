﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeAndDisableOnRemoveAds : MonoBehaviour
{

		public float duration = 0.3f;
		public float delay = 0;
		[Range(0,1)]
		public float
				target = 1;
		Graphic img;
		//		Image img;

		void Awake ()
		{
				G.i.OnRemoveAds += HandleOnRemoveAds;
		}

		void HandleOnRemoveAds ()
		{
		
	
				//if (GetComponent<gra> ())
				img = GetComponent<Graphic> ();
				Color col = img.color;
				col.a = 0;
				img.color = col;
				StartCoroutine (Auto.Wait (delay, () => {
						//if (img2 != null)
						//		StartCoroutine (img2.FadeImageAlpha (0, 1, duration, null));
						if (img != null)
								StartCoroutine (img.FadeImageAlpha (img.color.a, target, duration, () => {
										gameObject.SetActive (false);
								}));
				}));
		}
	



		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
