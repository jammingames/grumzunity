﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour
{
		public Text scoreText;
		public Text timeText, boldTimeText;
		GameObject timeObj;
		
		void OnEnable ()
		{
				
				scoreText.text = "0";
				G.i.OnBounceIncrease += OnBounce;
				scoreText.text = G.i.numBounces.ToString ();
				//string timeLabel = G.i.elapsedTime.ToString ("F1");
				if (timeText != null) {
						timeObj = timeText.transform.parent.gameObject;
						ScoreManager.instance.OnTimeAttackScore += HandleOnTimeAttackScore;
						timeObj.SetActive (false);
						
				}
		}

		void HandleOnTimeAttackScore (float t)
		{
				if (timeText != null) {
						
						timeObj.SetActive (true);
						string timeString = string.Format ("{0:0.0}s", t);
						timeText.text = timeString;
						timeString = string.Format ("{0:0.0}", t);
//						TimeSpan timeSpan = TimeSpan.FromSeconds (t);
//						int newMS = timeSpan.Milliseconds - (timeSpan.Milliseconds / 10);
//						string timeString = string.Format ("{0:D1}:{1:D2}:{2:G2}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds);
						if (boldTimeText != null)
								boldTimeText.text = timeString;
						
				
				}
				
		}

//	
//	IEnumerator ChangeColorMofo (int counter)
//	{
//		while (counter > 0) {
//			yield return StartCoroutine (ChangeColor ());
//			counter--;
//		}
//	}
//	
//	IEnumerator ChangeColor ()
//	{
//		StartCoroutine (scoreText.LerpImageColor (timeText.color, endColor, duration / 2, null));
//		StartCoroutine (Auto.Wait ((duration / 2), () => {
//			StartCoroutine (scoreText.LerpImageColor (scoreText.color, origColor, duration / 2, null));
//		}));
//	}
//
//

		
		void Update ()
		{
				//string timeLabel = G.i.elapsedTime.ToString ("F1");
				//timeText.text = timeLabel;
				scoreText.text = ScoreManager.instance.totalScore.ToString ();

				if (Input.GetKeyDown (KeyCode.F)) {
						HandleOnTimeAttackScore (G.i.elapsedTime);
				}
		}


		void OnBounce (int numBounces)
		{
				//scoreText.text = numBounces.ToString ();
		}
}
