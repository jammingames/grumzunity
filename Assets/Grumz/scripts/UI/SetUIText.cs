﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SetUIText : MonoBehaviour
{


		public Text tScore, tHighScore;
		// Use this for initialization

		string scoreString, bestString;
		bool highScore = false;
//		public List<Selectable> selectors;
		

		

		void Awake ()
		{
				ScoreManager.instance.OnHighScore += HandleOnHighScore;
				
//				print (Selectable.allSelectables);
		
		}

		void HandleOnHighScore (bool isNewScore, int newScore)
		{
				highScore = isNewScore;

		}

		void OnEnable ()
		{
				if (highScore) {
						scoreString = "NEW BEST\n" + ScoreManager.instance.bestScore.ToString ();
						bestString = "";
				} else {
						scoreString = "SCORE " + ScoreManager.instance.totalScore.ToString ();
						bestString = "BEST " + ScoreManager.instance.bestScore.ToString ();
				}
//				selectors = Selectable.allSelectables;
//				for (int i = 0; i < selectors.Count; i++) {
//						print ("\nSELECTOR: " + selectors [i].name);
//						print (selectors [i].FindSelectableOnUp () + " ON UP");
//						print (selectors [i].FindSelectableOnDown () + " ON DOWN");
//				}
//
				
				tScore.text = scoreString;
				if (tHighScore != null)
						tHighScore.text = bestString;
				
				//string label = "You Scored: " + G.i.numBounces + "\nHigh Score: " + PlayerPrefs.GetInt ("highScore");
				//tMesh.text = label;
		}

		void OnDisable ()
		{
				int score = 0;
				tScore.text = "SCORE " + 0;
				if (ScoreManager.instance != null && tHighScore != null)
						tHighScore.text = "BEST " + ScoreManager.instance.bestScore;
		}

		
}
