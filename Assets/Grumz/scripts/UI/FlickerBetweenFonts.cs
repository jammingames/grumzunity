﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Reflection;
using System;

public class FlickerBetweenFonts : MonoBehaviour
{

		public Color origNormColor, endNormColor, origBoldColor, endBoldColor;
		public Text normalText, boldText;
		public float duration;
		public int counter = 3;
		// Use this for initialization
		int origCounter;

		public bool shouldEnable = true;
		void Awake ()
		{
			
				origCounter = counter;
		}
	
		// Update is called once per frame
		
		void OnEnable ()
		{
				if (shouldEnable) {

						counter = origCounter;
						normalText.color = origNormColor;
						boldText.color = origBoldColor;
				
						StartCoroutine (ChangeColor ());
				}
		}

		IEnumerator ChangeColor ()
		{
				print ("changeColorMofo");
				
				
				while (counter > 0) {
						counter--;
						yield return StartCoroutine (BlinkColors ());
				}
				yield return StartCoroutine (Auto.Wait (1));
				
				gameObject.SetActive (false);
				
		}

		IEnumerator BlinkColors ()
		{
				print ("starting color blink");
				StartCoroutine (normalText.LerpImageColor (normalText.color, endNormColor, duration / 2, null));
				StartCoroutine (boldText.LerpImageColor (boldText.color, endBoldColor, duration / 2, null));
		
		
				//				scoreText.color = endColor;
				yield return StartCoroutine (Auto.Wait (duration / 2));
				//						scoreText.color = origColor;
				StartCoroutine (boldText.LerpImageColor (boldText.color, origBoldColor, duration / 2, null));
				StartCoroutine (normalText.LerpImageColor (normalText.color, origNormColor, duration / 2, null));
			
				yield return StartCoroutine (Auto.Wait (duration / 2));
				
		}

}
