﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AdjustHorizontalLayout : MonoBehaviour
{

		Vector3 origScale;
		public float duration;
		public Vector2 target;
		//Vector2 origPos;
		public EaseType easer;
		//	public HorizontalLayoutGroup hGroup;
		

		RectTransform rTrans;

		
		//public Animation animClip;
	
		public GameState desiredState;

	
	
		void Awake ()
		{
				G.i.OnStateChange += HandleOnStateChange;
				rTrans = GetComponent<RectTransform> ();
				//	origPos = rTrans.anchoredPosition;
		
		}

		void Start ()
		{
				//enabled = false;
				
		}


		void Update ()
		{

		}

		public void Test ()
		{
				//if (state == desiredState) {
				//		enabled = true;
				//G.i.SetGameState (GameState.StartGame);
				StartCoroutine (rTrans.MoveRectTo (target, duration, easer, () => {

						G.i.SetGameState (GameState.Game);
				}));
				
		}
	
	
		void HandleOnStateChange (GameState state)
		{
				if (state == desiredState) {
						StartCoroutine (rTrans.MoveRectTo (target, duration, easer, () => {
				
								G.i.SetGameState (GameState.Game);
						}));
				} else {
						//StartCoroutine (rTrans.MoveRectTo (origPos, duration, easer, null));
						enabled = false;
				}
		}
	
	
		void Disable ()
		{
				//		gameObject.SetActive (false);
		}
	

}
