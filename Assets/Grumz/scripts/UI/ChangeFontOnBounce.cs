﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ChangeFontOnBounce : MonoBehaviour
{

		public Color origColor, endColor;
		public Text scoreText;
		public float duration;

		
		

		// Use this for initialization
		void Start ()
		{
				//origColor = scoreText.color;
				G.i.OnBounceIncrease += HandleOnBounceIncrease;
				G.i.OnBounceAdd += HandleOnBounceIncrease;
		}

		void OnEnable ()
		{
				scoreText.color = origColor;
		}

		void ChangeColorMofo ()
		{
//				print ("changeColorMofo");
				gameObject.SetActive (true);
				StartCoroutine (scoreText.LerpImageColor (scoreText.color, endColor, duration / 2, null));
				
			
//				scoreText.color = endColor;
				StartCoroutine (Auto.Wait ((duration / 2), () => {
//						scoreText.color = origColor;
						StartCoroutine (scoreText.LerpImageColor (scoreText.color, origColor, duration / 2, null));

				}));
		}

		void HandleOnBounceIncrease (int numBounces)
		{
				ChangeColorMofo ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.H))
						ChangeColorMofo ();
				
		}
}
