﻿using UnityEngine;
using System.Collections;

public class SetTextmesh : MonoBehaviour
{


		TextMesh tMesh;
		// Use this for initialization

		void Awake ()
		{
				tMesh = GetComponent<TextMesh> ();

		}
		void OnEnable ()
		{
				if (!PlayerPrefs.HasKey ("highScore"))
						PlayerPrefs.SetInt ("highScore", G.i.numBounces);
				else if (PlayerPrefs.GetInt ("highScore") < G.i.numBounces)
						PlayerPrefs.SetInt ("highScore", G.i.numBounces);
				
				string label = "You Scored: " + G.i.numBounces + "\nHigh Score: " + PlayerPrefs.GetInt ("highScore");
				tMesh.text = label;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
