﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour
{




		public delegate void UIEvent ();
		public event UIEvent OnPlayButton;
	
		public void TriggerPlayButton ()
		{
				if (OnPlayButton != null)
						OnPlayButton ();
		}

		
		static UIManager _instance = null;
		public static UIManager instance {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(UIManager)) as UIManager;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("UIManager");
										_instance = obj.AddComponent<UIManager> ();
								}
						}

						return _instance;
				}
		}


		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}
}
