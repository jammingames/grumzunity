﻿using UnityEngine;
using System.Collections;

public class LerpShieldColor : MonoBehaviour
{

		public EaseType standardEaser, powerDownEaser, zombieEaser, grumzyEaser;
		public Color[] col;
		public Color targetCol;
		public Color startCol;
		int index;
		SpriteRenderer render;
		public float duration;
		LerpColorTowards colorer;
//		bool dontDoItBub = false;
		float t = 0;
		int currentState = 7;
		HeatState currentHeatState = HeatState.NullState;

		bool hasShield = false;
		

		//public float delay;
		// Use this for initialization
		void Awake ()
		{
				render = GetComponent<SpriteRenderer> ();
				
				G.i.OnPlayerShield += HandleOnPlayerShield;
				
				if (GetComponent<LerpColorTowards> ())
						colorer = GetComponent<LerpColorTowards> ();
				else
						colorer = gameObject.AddComponent<LerpColorTowards> ();
				
				colorer.SetColorParams (render.color, Color.white, 2f);
				colorer.enabled = false;
				startCol = Color.white;
				startCol.a = 0;
				render.color = startCol;
		}

		void HandleOnPlayerShield (bool isStart)
		{
				if (isStart) {
						print ("enabling lerp shield");
						enabled = true;
						hasShield = true;

						float randomTime = 1.8f * (Random.value + 0.2f);
						targetCol = col [0];
						colorer.enabled = true;
//						colorer.SetColorParams (render.color, targetCol, randomTime);
						colorer.ChangeColor (targetCol, randomTime);
					
						//	colorer.ChangeColor (targetCol, randomTime);
			
						StartCoroutine (Auto.Wait (randomTime + 0.05f, () => {
				
								if (G.i.isOverheated) {
										t = 0.1f;
										targetCol = col [3];
								} else if (G.i.heatupPercent >= 0.8f && G.i.heatupPercent < 1) {
										t = 0.1f;
										targetCol = col [2];
					
								} else if (G.i.heatupPercent >= 0.2f && G.i.heatupPercent < 0.8f) {
										t = 0.1f;
										targetCol = col [1];
								}

								colorer.ChangeColor (targetCol, t);
								G.i.OnHeating += HandleOnHeating;
								G.i.Overheated += HandleOverheated;
				
						}));
//						dontDoItBub = false;
				} else if (!isStart) {
//						dontDoItBub = true;
						hasShield = false;
						G.i.OnHeating -= HandleOnHeating;
						G.i.Overheated -= HandleOverheated;
						colorer.ResetColors ();
						colorer.enabled = false;
						render.color = Color.white;

						enabled = false;
						
				}
		}


		void OnEnable ()
		{
//				if (G.i.isOverheated) {
//						targetCol = col [3];
//				} else if (G.i.heatupPercent < 0.2f) {
//						targetCol = col [0];
//				} else if (G.i.heatupPercent >= 0.8f && G.i.heatupPercent < 1) {
//						targetCol = col [2];
//				} else if (G.i.heatupPercent >= 0.2f && G.i.heatupPercent < 0.8f) {
//						targetCol = col [1];
//				}
				//targetCol = col [5];
				//startCol = col [5];
				//startCol.a = 0;
				//render.color = startCol;
//				render.color = startCol;
//				//debug.log (render.color);
				
				startCol = Color.white;
				startCol.a = 0;
				render.color = startCol;
//

		}

		void OnDisable ()
		{
//				startCol = col [0];
//				//render.color = startCol;
//				startCol.a = 0;
//				render.color = startCol;
//				targetCol = col [0];
				if (G.i != null) {
						G.i.OnHeating -= HandleOnHeating;
						//G.i.OnHeatUp -= HandleOnHeatUp;
						G.i.Overheated -= HandleOverheated;
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
				
		}

	
		void HandleOverheated (bool isStart)
		{
				//if (!dontDoItBub) {

				if (isStart == false) {
//								StopAllCoroutines ();
//								render.color = col [0];
//								StartCoroutine (render.FadeSpriteColor (render.color, col [0], 0.3f, 0.01f, null));
				} else {
//								StopAllCoroutines ();
//								StartCoroutine (render.FadeSpriteColor (render.color, col [3], 0.3f, 0.01f, null));
				}
				//}

		}	

		void HandleOnHeating (HeatState state)
		{
				if (gameObject.activeInHierarchy == true && hasShield) {
						//		if (!dontDoItBub) {
			
						if (state == currentHeatState && state != HeatState.Standard)
								return;
			
						if (state == HeatState.Heating) {
								t = G.i.heatupTime - (G.i.heatupTime * G.i.heatupPercent);
								Powerdown (t);
						} else if (state == HeatState.Overheat) {
								t = 0.5f;
								Grumzy (t);
						} else if (state == HeatState.Standard) {
								t = G.i.timeToCool * G.i.heatupPercent;
								Standard (t);
						} else if (state == HeatState.FinishedOverheat) {
								Standard (0.1f);
						} else if (state == HeatState.Zombie) {
								t = G.i.heatupTime - (G.i.heatupTime * G.i.heatupPercent);
								Zombie (t);
						}
			
						//		}
						currentHeatState = state;
				}
	
		}


		void HandleOnHeatUp (int state)
		{
				if (gameObject.activeSelf == true) {
						//		if (!dontDoItBub) {

						if (state == currentState && state != 2)
								return;

						if (state == 0) {
								t = G.i.heatupTime - (G.i.heatupTime * G.i.heatupPercent);
								Powerdown (t);
						} else if (state == 1) {
								t = 0.5f;
								Grumzy (t);
						} else if (state == 2 || state == 3) {
								t = G.i.timeToCool * G.i.heatupPercent;
								Standard (t);
						} else if (state == 4) {
								Standard (0.1f);
						} else if (state == 5) {
								t = G.i.heatupTime - (G.i.heatupTime * G.i.heatupPercent);
								Zombie (t);
						}
				
						//		}
				}
				currentState = state;
		}


		void Standard (float time)
		{
				colorer.ChangeColor (col [0], time, standardEaser);
		}
		void Powerdown (float time)
		{
				colorer.ChangeColor (col [1], time, powerDownEaser);
		}
		void Invul (float time)
		{
				colorer.ChangeColor (col [5], time, standardEaser);
		}
		void Zombie (float time)
		{
				colorer.ChangeColor (col [2], time, zombieEaser);
		}
		void Grumzy (float time)
		{
				colorer.ChangeColor (col [3], time, grumzyEaser);
		}
		void DisableTrail (float time)
		{
				colorer.ChangeColor (col [5], time, standardEaser);
		}
}
