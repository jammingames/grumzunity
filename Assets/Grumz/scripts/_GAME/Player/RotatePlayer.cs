﻿using UnityEngine;
using System.Collections;

public class RotatePlayer : MonoBehaviour
{
		public EaseType heatupEase, coolDownEase, returnEase;
		public float speed = 10;
		
		public float rotation;
		
		public float heatUpRotation;
		public float powerDownRotation;
		public float overHeatRotation;
		
		
		Color origColor;
		Color fadedColor;
		float t = 0;

		//	float targetRotation = 25;

		float origRotation;

		bool canMove = true;
//		float tTarget = 0;
		bool isInvul = false;

		RotateTowards rotator;
//		int currentState = 2;
		//HeatState currentHeatState = HeatState.NullState;


		void Awake ()
		{
				G.i.OnHeating += HandleOnHeating;
			


				origRotation = rotation;
				//	targetRotation = rotation;
				
				G.i.OnPlayerDeath += this.HandleOnPlayerDeath;
				G.i.OnStateChange += HandleOnStateChange;
				G.i.OnPlayerInvul += HandleOnPlayerInvul;
				canMove = false;
				this.enabled = false;
		}

		void HandleOnPlayerInvul (bool isStart)
		{
				if (canMove) {

						if (isStart) {
								isInvul = true;
								rotator.ChangeRotation (origRotation, 0.2f, returnEase);
						} else 
								isInvul = false;
				}
		}



		void OnEnable ()
		{

				if (GetComponent<RotateTowards> ())
						rotator = GetComponent<RotateTowards> ();
				else
						rotator = gameObject.AddComponent<RotateTowards> ();
				
				rotator.SetRotation (origRotation, speed);
				t = G.i.timeToCool / 3;
				rotator.ChangeRotation (origRotation, t, returnEase);
				//rotator.Stop ();
//				StartCoroutine (Auto.Wait (0.8f, () => {
//						rotator.SetRotation (origRotation, speed);
//
//				}));
		

//				//debug.log (zombieSprite);
				
//				origColor = Color.white;
//				fadedColor = Color.white;
//				fadedColor.a = 0;
//				zombieSprite.color = fadedColor;
//				//debug.log (origColor);
//				//debug.log (fadedColor);
				
		}


		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == GameState.Game) {
						this.enabled = true;
						canMove = true;
						rotator.Start ();
				}
		}


		

	
		// Update is called once per frame
		void Update ()
		{
				
				
				
		}


		void HandleOnPlayerDeath ()
		{

				rotator.Stop ();
				
				this.enabled = false;
		}



	




	
		void HandleOnHeating (HeatState state)
		{
				if (canMove) {
						if (!isInvul) {
				
//								if (currentHeatState != state)
//													//debug.log ("rotating to state " + state);
								if (!gameObject.activeSelf)
										return;
								if (state == HeatState.Heating) {
										// t == current % towards fully heated
										t = G.i.heatupTime - (G.i.heatupTime * G.i.heatupPercent);
										rotator.ChangeRotation (heatUpRotation, t, heatupEase);
					
								} else if (state == HeatState.Overheat) {
										//t == instantly set to 0 and begin change towards overheat
										rotator.SetRotation (0, speed);
										t = ((G.i.heatupTime - (G.i.heatupTime * (1 - G.i.heatupPercent))) / G.i.increaseFactor) - (0.322222f * G.i.heatupTime / G.i.timeToCool);
										rotator.ChangeRotation (overHeatRotation, t, heatupEase);
					
								} else if (state == HeatState.Standard) {
//										if (rotator.rotation < 0) {
//												//t == quickly return to normal rotation
//												t = G.i.timeToCool / 3;
//												rotator.ChangeRotation (origRotation, t, returnEase);
//						
//										} else {
										//t = % towards normal from current heat
										t = G.i.timeToCool * G.i.heatupPercent;
						
										rotator.ChangeRotation (origRotation, t, coolDownEase);
						
										//}
								} else if (state == HeatState.FinishedOverheat) {
										//	t = G.i.timeToCool / 3;
										//	rotator.ChangeRotation (origRotation, t, returnEase);

								} else if (state == HeatState.Zombie) {
										//t == move towards zero
										t = G.i.heatupTime - (G.i.heatupTime * G.i.heatupPercent);
										rotator.ChangeRotation (0.5f, t, heatupEase);
								} else if (state == HeatState.GrumzyZombie) {
										t = 0.3222f * (G.i.heatupTime / G.i.increaseFactor);
										rotator.ChangeRotation (-0.5f, t, heatupEase);
								}
				
						} else {
								rotator.ChangeRotation (origRotation, 0.1f);
						}
						//	currentHeatState = state;
				}
		}
		

//
//		void HandleOnHeatUp (int state)
//		{
//				if (canMove) {
//						if (!isInvul) {
//
////								if (currentState != state)
////								//debug.log ("rotating to state " + state);
//								if (!gameObject.activeSelf)
//										return;
//								if (state == 0) {
//										//targetRotation = heatUpRotation;
//										t = G.i.heatupTime - (G.i.heatupTime * G.i.heatupPercent);
//										rotator.ChangeRotation (heatUpRotation, t, heatupEase);
//
//								} else if (state == 1) {
//										rotator.SetRotation (0, speed);
//										rotator.ChangeRotation (overHeatRotation, 0.1f);
//
//								} else if (state == 2 || state == 3) {
//										if (rotator.rotation < 0) {
//												t = G.i.timeToCool / 3;
//												rotator.ChangeRotation (origRotation, t, returnEase);
//												
//										} else {
//												t = G.i.timeToCool * G.i.heatupPercent;
//								
//												rotator.ChangeRotation (origRotation, t, coolDownEase);
//
//										}
//						
//								} else if (state == 5) {
//										t = G.i.heatupTime - (G.i.heatupTime * G.i.heatupPercent);
//										rotator.ChangeRotation (0.5f, t, heatupEase);
//								}
//						} else {
//								rotator.ChangeRotation (origRotation, 0.1f);
//						}
//						currentState = state;
//				}
//		}
}
