﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;



[System.Flags]
public enum HeatState
{
		NullState = 1 << 0,
		Heating = 1 << 1,
		Standard = 1 << 2,
		FinishedOverheat = 1 << 3, //just after grumzy
		Overheat = 1 << 4,
		GrumzyZombie = 1 << 5,
		Zombie = 1 << 6,
}


public class PlayerMovement : MonoBehaviour, IActorPhysicsProxy
{

//	overheat start √ - might use “flyby"
//overheat over √ heatup_ouch
//heating up while held √ heatup_long, heatup_short, heat_short2
//speed up when not selected ? bouncing_up-06.wav
//death explosion √ explode_shatter + explode_wave
//ping pongs (possibly third selection sound, semitone diff) select3
		public float skinWidth;
		//public float pitchStart, pitchMid, pitchEnd;
		//public float increasePitchAmt;
		//float currentPitch, targetPitch;
		
		//public float rotation, speed;

		public float moveSpeed = 1f;
		float originalSpeed;
		public float delayTime = 3f;
		public float time;
		public int numBounces;
		public float[] increasePerBounce;
		public int[] bounceIncreaseLevels;
		public float heightOffset;
		bool isHeating = false;
		AudioSource currentSnd;
		int currentBounceIndex = 0;

		float topBounds, botBounds, leftBounds, rightBounds;
		float direction = 1f; 
		[HideInInspector]
		public bool
				canMove = true;
		[HideInInspector]
		public bool
				inputDown = false;
		
		bool heated = false;
		float increaseFactor = 2;
		LerpColor colors;
		//float duration = 0.15f;
//		float t = 0;
		bool hasInvul = false;
		[HideInInspector]
		public Collider2D
				col;

		bool pressedLastFrame = false;
		bool audioIsPlaying = false;
		public AudioSource bounceSnd, heatingSnd, overheatSnd, overheatOverSnd, speedUpSnd, deathSnd, overHeatTapSnd;
		bool canPlayOverheatTap = false;
		public AudioSource[] bounceSnds;
//		public AudioClip bounceSnd, heatingSnd, overheatSnd, overheatOverSnd, speedUpSnd, deathSnd;
//		public AudioClip[] bounceSnds;
		public int index = 0;
		public float newTargetSpeed, targetSpeed;

		Vector3 origPos;
		//float origSpeed;
		float lastSpeed;
		bool isStandard = true;
//		bool isReversed = false;
		bool hasChangedDirection = false;
		bool changeSpeed = false;
		bool cantInput = true;
		float increaseAmmount = 0.1f;
		
		string audioPitch = "heatupPitch";
		public AudioMixer mixer;
		
		float volIncreaseAmmount = 0.01f;
		bool changeVolume = false;
		float targetVolume, currentVolume;
		bool tutorialDone = false;
		public bool canCollide = true;

		float factor = (0.32222222f);
//		bool tutorialFinished = false;

		/*
		 * if target changes before current target reached, while target= 0
		 * wait for current 0 target before re-assigning target
		 * 
		 * */
		

		void Awake ()
		{
//				tutorialFinished = false;
				originalSpeed = moveSpeed;
				//	origSpeed = moveSpeed;
				origPos = transform.localPosition;
				numBounces = 0;
				
				G.i.Overheated += HandleOverheated;


				G.i.playerObj = gameObject;
				G.i.heatupTime = delayTime;
//				G.i.OnPlayerLeaveDirection += HandleOnPlayerLeaveDirection;
				
				col = GetComponent<Collider2D> ();

				G.i.OnTutorial += HandleOnTutorial;

				
		}

		void HandleOnTutorial (bool isStart)
		{
				if (!isStart) {
						G.i.OnBounceIncrease += CheckBounces;
						numBounces = 0;
						tutorialDone = true;
				}
		}

		void HandleOnPlayerInvul (bool isStart)
		{	
				if (isStart) {
						hasInvul = isStart;
			
						canPlayOverheatTap = false;
			
						heated = false;
						increaseFactor = 2;
						inputDown = true;
				} else {
						hasInvul = isStart;
						time = delayTime;

						canPlayOverheatTap = false;

						heated = false;
						increaseFactor = 2;
						inputDown = true;

				}
		}

		void HandleOnPlayerDeath ()
		{
				heatingSnd.Stop ();
		}



		void Start ()
		{
				botBounds = G.i.BOT_SCREEN_EDGE + heightOffset;
				leftBounds = G.i.LEFT_SCREEN_EDGE + heightOffset;
				rightBounds = G.i.RIGHT_SCREEN_EDGE - heightOffset;
				topBounds = G.i.TOP_SCREEN_EDGE - heightOffset;
				
		}
		
		void OnEnable ()
		{

				G.i.OnPlayerInvul += HandleOnPlayerInvul;
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
				ResetValues ();
				if (tutorialDone) {
						G.i.OnBounceIncrease += CheckBounces;
						numBounces = 0;
				}
			
		}


		void OnDisable ()
		{
				if (G.i != null) {
			

						G.i.OnPlayerInvul -= HandleOnPlayerInvul;
						G.i.OnPlayerDeath -= HandleOnPlayerDeath;
				}
		}
		
		void ResetValues ()
		{
				moveSpeed = originalSpeed;
				targetSpeed = originalSpeed;
				numBounces = 0;
				currentBounceIndex = 0;
				direction = 1;
				transform.localPosition = origPos;
				time = delayTime;
				heatingSnd.volume = 0;
				targetVolume = 0;
				cantInput = true;
				isHeating = false;
				isStandard = false;
				heated = false;
				inputDown = false;
				canPlayOverheatTap = false;
				StartCoroutine (Auto.Wait (1f, () => {
						cantInput = false;
				}));

		}

		void CheckCollisions ()
		{
				if (col.bounds.center.y < botBounds || col.bounds.center.x < leftBounds || col.bounds.center.x > rightBounds || col.bounds.center.y > topBounds) {
						DirectionChange ();
						ClampPositionToScreen ();
				}
		}


		void ClampPositionToScreen ()
		{
				Vector3 pos = transform.position;
				pos = new Vector3 (Mathf.Clamp (pos.x, leftBounds, rightBounds), Mathf.Clamp (pos.y, botBounds, topBounds), 0);
				transform.position = pos;
		}


		void DirectionChange ()
		{
//					//debug.log ("changing direction");
				if (!hasChangedDirection) {
						hasChangedDirection = true;

						bounceSnds [index].Play ();
						//						AudioManager.instance.PlayAudio (bounceSnds [index]);
						if (index < bounceSnds.Length - 1) {
								index++;
				
				
						}
			
						direction *= -1f;
						if (tutorialDone) {
								numBounces += 1;
								G.i.TriggerBounceIncrease (numBounces);

						}
						G.i.TriggerPlayerChange (true);
						StartCoroutine (Auto.Wait (0.3f, () => {
								hasChangedDirection = false;
						}));
				}
		}


		

		
		void Update ()
		{
				
				if (hasInvul)
						time = delayTime * 0.5f;
//
//				if (Input.anyKeyDown) {
//						StartCoroutine (mixer.ChangeAudioParam (audioPitch, pitchStart, pitchMid, 0.1f, EaseType.QuadInOut, null));
//				}

//
				currentVolume = heatingSnd.volume;
				if (changeVolume && currentVolume < targetVolume) {
						currentVolume += volIncreaseAmmount;
				} else if (changeVolume && currentVolume > targetVolume) {
						currentVolume -= (volIncreaseAmmount * 5);
				}
				currentVolume = Mathf.Clamp (currentVolume, 0.01f, 0.99f);
				heatingSnd.volume = currentVolume;


				if (changeSpeed && moveSpeed < targetSpeed) {
						moveSpeed += increaseAmmount;
				} else if (changeSpeed && moveSpeed > targetSpeed) {
						moveSpeed -= increaseAmmount;
				}
				if (moveSpeed == targetSpeed)
						changeSpeed = false;
				if (!canMove && !audioIsPlaying) {
						if (AudioManager.instance.canPlay) {
								//heatingSnd.volume = 0;
								//heatingSnd.
							
								StartCoroutine (heatingSnd.ChangeVolume (heatingSnd.volume, 0f, 0.1f, () => {
										heatingSnd.Stop ();
										heatingSnd.Play ();
										ChangeVolume (1);
								}));
				
				

						}
						G.i.TriggerPlayerTap ();
						
						audioIsPlaying = true;
						
						
				} else if (canMove && heatingSnd != null) {

						ChangeVolume (0);

				}
				if (canCollide && canMove && !cantInput) {

						CheckCollisions ();

				}
				if (Input.anyKeyDown && canPlayOverheatTap) {

						overHeatTapSnd.Play ();
						canPlayOverheatTap = false;
				}


				/*
			 * Heating 0
			 * Overheat 1
		     * Cooling 2
			 * HeatingMore 3
		 	 * ResetHeat 4
			 * Zombie 5
			 * */

				//rotate faster and faster towards "grumzyzombie"
				//rotate towards zero during "grumzyZombie"
				//overheat rotate duration ==  .... SOMETHING WITH MATH???????????

				if (canMove) {
						transform.Translate (0, moveSpeed * direction * Time.deltaTime, 0);
						if (time < delayTime) {
								if (isHeating) {
//					//debug.log ("CALLING NUMBER 3");
										isHeating = false;
										//	G.i.TriggerHeating (3);
										G.i.TriggerOnHeating (HeatState.Standard);
								}

								if (heated) {
										float t = (delayTime - time) / delayTime;
										if (t <= factor) {
												G.i.TriggerOnHeating (HeatState.GrumzyZombie);
										}

								}

								time += Time.deltaTime * increaseFactor; //cooling down
						} else if (time >= delayTime) {
								if (!isStandard) {
										isStandard = true;
										//	G.i.TriggerHeating (2);
										G.i.TriggerOnHeating (HeatState.Standard);
								}
						}
				} else if (!canMove && time > 0) {		//heating up
						if (!isHeating) {
								index = 0;
								isStandard = false;
								isHeating = true;
								//G.i.TriggerHeating (0);
								G.i.TriggerOnHeating (HeatState.Heating);
						}
						//transform.Rotate (Vector3.forward, delayTime / time);
						
						time -= Time.deltaTime;

						float fac = factor * delayTime;
//						//debug.log ("FACTOR!!!!! " + factor);
						if (time <= fac) {
								//G.i.TriggerHeating (5);
								G.i.TriggerOnHeating (HeatState.Zombie);
						}
				}

				if (time <= 0) {				//overloaded, recharge
						if (!heated) {
								StartCoroutine (Auto.Wait (0.1f, () => {
										canPlayOverheatTap = true;}));
								
								isHeating = false;
								//G.i.TriggerHeating (1);
								G.i.TriggerOnHeating (HeatState.Overheat);
								G.i.TriggerOverheated (true);
								
								heated = true;
						}
						increaseFactor = 1.5f;
						inputDown = false;
						canMove = true;
				} else if (time >= delayTime) {		//can input after overload
						if (heated) {
								time = delayTime;
								canPlayOverheatTap = false;
								//G.i.TriggerHeating (4);
								G.i.TriggerOnHeating (HeatState.FinishedOverheat);
								G.i.TriggerOverheated (false);
								heated = false;
								increaseFactor = 2;
								inputDown = true;
						}
				} else {
						inputDown = false;
				}
	



				if (G.i.numBounces < numBounces)
						G.i.numBounces = numBounces;
				else
						numBounces = G.i.numBounces;


				float percent = 1 - (time / delayTime);
				percent = Mathf.Clamp (percent, 0.01f, 1);
				float timeLeft = (delayTime / increaseFactor);
				G.i.heatupPercent = percent;
				G.i.timeToCool = timeLeft;
				G.i.increaseFactor = increaseFactor;

				Vector3 pos = transform.localPosition;
				pos.x = 0;
				transform.localPosition = pos;
			
				
		}


		public void ChangeSpeed (float newSpeed)
		{
				changeSpeed = true;
				targetSpeed = newSpeed;
		}
		
		public void ChangeVolume (float newVolume)
		{
				changeVolume = true;
				targetVolume = newVolume;
		}


		void HandleOverheated (bool isHeated)
		{
				if (isHeated) {
						//Debug.Log ("WTF????");
						if (AudioManager.instance.canPlay) {
								AudioManager.instance.PlayAudio (overheatSnd);
						}
				} else {
						if (AudioManager.instance.canPlay)
								AudioManager.instance.PlayAudio (overheatOverSnd);
				}
		}
		

		void CheckBounces (int numBounces)
		{
				if (tutorialDone) {

						if (currentBounceIndex < bounceIncreaseLevels.Length && numBounces >= bounceIncreaseLevels [currentBounceIndex]) {
								ChangeSpeed (moveSpeed + increasePerBounce [currentBounceIndex]);
								//moveSpeed += increasePerBounce [currentBounceIndex];
								currentBounceIndex++;
						}
				}
		}
		
		public void HandleMouseInput (bool isDown)
		{

				if (cantInput)
						return;
				//	if (isReversed && isDown)
				//			ChangeDirection ();		
				if (!isDown)
						audioIsPlaying = false;
				if (inputDown && isDown) {
						////debug.log ("heating");
						
						canMove = false;
				} else if (!inputDown && !isDown) {
						canMove = true;
				}
				if (isDown && !heated) {
						
						//		AudioSource.PlayClipAtPoint (heatingSnd, transform.position);
//						//debug.log ("heating");		
						canMove = false;
				}
				if (!pressedLastFrame && isDown)
						pressedLastFrame = true;
				else if (pressedLastFrame && !isDown) {
						pressedLastFrame = false;
				}
		}

		public void HandleInput (float axis, float throttle, bool isDown)
		{
		}

}
