﻿using UnityEngine;
using System.Collections;

public class TrailController : MonoBehaviour
{

		
		public float duration;
		

		public SpriteRenderer sprGlow, zombieGlow;
		public SpriteRenderer standardRender, powerdownRender, invulRender, grumzyRender;
		FadeTowards standardFade, pdFade, invFade, grumFade, glowFade, zombieFade;
		ScaleTowards scaler;
		Sprite currentSprite, nextSprite;
		Vector3 origScale;
		public Vector3 targetScale;
		public Vector3 startRotation;
		public Vector3 startScale;
		public EaseType easer, easer2, trailEaser, bounceEaser;
//		Quaternion origRotation;
		float currentSpeed;
		public float speed1, speed2, speed3;
		public bool isInvul;
		bool shouldFadeGlow = true;

		int currentState;
		HeatState currentHeatState;

		BounceCoroutine bouncey;


		float time = 5.2f;
		float startTime = 5.2f;
		bool shouldCount;
		bool wasCounting = false;


		void Awake ()
		{
//				bouncey = GetComponent<BounceCoroutine> ();
				StartFadeTowards ();
				//		Standard ();

				if (GetComponent<ScaleTowards> ())
						scaler = GetComponent<ScaleTowards> ();
				else
						scaler = gameObject.AddComponent<ScaleTowards> ();

				origScale = transform.localScale;

				//origRotation = transform.localRotation;
				
			
//				bouncey.enabled = false;

		}



		void OnEnable ()
		{

				//StopAllCoroutines ();
				currentState = 6;
				currentSpeed = speed1;
				if (origScale.y < 0)
						origScale.y *= -1;
				transform.localScale = targetScale;
				transform.localRotation = Quaternion.Euler (startRotation);
				DisableTrail ();
				scaler.SetScaleParams (origScale, startScale, origScale, 0.1f);
				//	Standard ();
//				StartCoroutine (transform.ScaleTo (startScale, 0.1f, easer, () => {
				scaler.ChangeScale (startScale, 0.1f, easer);
				StartCoroutine (Auto.Wait (1.45f, () => {
						G.i.OnHeating += HandleOnHeating;
						//G.i.OnHeatUp += HandleOnHeatUp;
						G.i.OnBounceIncrease += HandleOnBounceIncrease;
						G.i.OnPlayerInvul += HandleOnPlayerInvul;
						G.i.OnPlayerChangeDirection += HandleOnPlayerChangeDirection;
						G.i.OnPlayerDeath += HandleOnPlayerDeath;
						G.i.OnPlayerShield += HandleOnPlayerShield;
				}));
				StartCoroutine (Auto.Wait (1.6f, () => {
						Standard ();
						
						scaler.ChangeScale (origScale, 0.5f, easer);
//								StartCoroutine (transform.ScaleTo (origScale, 0.5f, easer, () => {
//					
//								}));
				}));

				//Standard ();
//				}));


//				StartCoroutine (Auto.Wait (1.5f, () => {
//						bouncey.enabled = true;
//				}));
				
		}

		void HandleOnPlayerShield (bool isStart)
		{
				if (isStart) {
						shouldFadeGlow = false;
						zombieFade.ChangeColor (0, duration / 6);
						glowFade.ChangeColor (0, duration / 6);
				} else 
						shouldFadeGlow = true;
				
		}


		void OnDisable ()
		{
//				bouncey.enabled = false;
				//StopAllCoroutines ();
				Color resetCol = standardRender.color;
				resetCol.a = 1;
				standardRender.color = resetCol;
				resetCol.a = 0;
				sprGlow.color = resetCol;
				zombieGlow.color = resetCol;
				grumzyRender.color = resetCol;
				powerdownRender.color = resetCol;
				invulRender.color = resetCol;
				transform.localScale = Vector3.one;





				
				if (G.i != null) {
						G.i.OnBounceIncrease -= HandleOnBounceIncrease;
						G.i.OnPlayerInvul -= HandleOnPlayerInvul;
						G.i.OnPlayerChangeDirection -= HandleOnPlayerChangeDirection;
						G.i.OnPlayerDeath -= HandleOnPlayerDeath;
						G.i.OnHeating -= HandleOnHeating;
						//G.i.OnHeatUp -= HandleOnHeatUp;
				}
		}


		// Use this for initialization
		void Start ()
		{

				//currentSpeed = speed1;
		}

		void HandleOnPlayerDeath ()
		{
				
				DisableTrail ();
				//		RevertScale ();
		}
	
		void HandleOnPlayerInvul (bool isStart)
		{

				if (isStart) {
						//StopAllCoroutines ();
						Invul ();
						isInvul = true;
						FadeInvul (3.8f);
				} else if (!isStart) {
						//StopAllCoroutines ();
						Standard ();
						isInvul = false;
				}
		}

		void FadeInvul (float duration)
		{
				shouldCount = true;
				//print ("starting duration timer: " + duration);
				//StartCoroutine (Auto.Wait (duration, () => {
				//		Standard ();
				//}));
		}

	
		void HandleOnHeating (HeatState state)
		{

				if (state == currentHeatState && state != HeatState.Standard)
						return;

				if (!isInvul && state != HeatState.Standard)
						StopAllCoroutines ();
				if (state == HeatState.Heating) {
						//render.sprite = powerDownTrailSprite;
						if (!isInvul)
								Powerdown ();
						//						standardRender.enabled = false;
						//						invulRender.enabled = false;
						//						powerdownRender.enabled = true;
						scaler.ChangeScale (targetScale, 2.8f, trailEaser);
						//						StartCoroutine (transform.ScaleTo (targetScale, 1.75f, trailEaser, null));
				} else if (state == HeatState.Overheat) {
						if (!isInvul)
								Grumzy ();
						//render.sprite = zombieTrailSprite;
						transform.localScale = origScale;
						StartCoroutine (Auto.Wait (0.4f, () => {
								//render.sprite = grumzyTrailSprite;
								transform.localScale = targetScale;
								RevertScale ();
						}));
				} else if (state == HeatState.Standard) {
						if (!isInvul)
								Standard ();
						//	render.sprite = standardTrailSprite;
						if (transform.localScale != origScale) {
								//								StartCoroutine (transform.ScaleTo (origScale, 0.3f, null));
								RevertScale ();
						}
				} else if (state == HeatState.FinishedOverheat) {
						if (!isInvul)
								Standard ();
						//	render.sprite = standardTrailSprite;
						if (transform.localScale != origScale) {
								//								StartCoroutine (transform.ScaleTo (origScale, 0.3f, null));
								RevertScale ();
						}
				} else if (state == HeatState.Zombie) {
						if (!isInvul)
								Zombie ();
			
				}
		
				currentHeatState = state;
		}
	
		void HandleOnHeatUp (int state)
		{
//				//debug.log (state + " is being fired");
//				//debug.log (transform.localScale);
				if (state == currentState && state != 2)
						return;
				////debug.log ("player moving to state: " + state);
				if (!isInvul && state != 2)
						StopAllCoroutines ();
				if (state == 0) {
						//render.sprite = powerDownTrailSprite;
						if (!isInvul)
								Powerdown ();
//						standardRender.enabled = false;
//						invulRender.enabled = false;
//						powerdownRender.enabled = true;
						scaler.ChangeScale (targetScale, 2.8f, trailEaser);
//						StartCoroutine (transform.ScaleTo (targetScale, 1.75f, trailEaser, null));
				} else if (state == 1) {
						if (!isInvul)
								Grumzy ();
						//render.sprite = zombieTrailSprite;
						transform.localScale = origScale;
						StartCoroutine (Auto.Wait (0.4f, () => {
								//render.sprite = grumzyTrailSprite;
								transform.localScale = targetScale;
								RevertScale ();
						}));
				} else if (state == 2) {
						if (!isInvul)
								Standard ();
						//	render.sprite = standardTrailSprite;
						if (transform.localScale != origScale) {
//								StartCoroutine (transform.ScaleTo (origScale, 0.3f, null));
								RevertScale ();
						}
				} else if (state == 3) {
						if (!isInvul)
								Standard ();
						if (transform.localScale != origScale) {
//								StartCoroutine (transform.ScaleTo (origScale, 0.3f, null));
								RevertScale ();
						}
				} else if (state == 4) {
						if (!isInvul)
								Standard ();
						//	render.sprite = standardTrailSprite;
						if (transform.localScale != origScale) {
//								StartCoroutine (transform.ScaleTo (origScale, 0.3f, null));
								RevertScale ();
						}
				} else if (state == 5) {
						if (!isInvul)
								Zombie ();

				}

				currentState = state;
		}

		void RevertScale ()
		{
				scaler.ChangeScale (origScale, 0.3f, easer2);
		}
	
		void HandleOnPlayerChangeDirection (bool isStart)
		{

				StopAllCoroutines ();
				Vector3 newY = new Vector3 (transform.localScale.x, -transform.localScale.y, transform.localScale.z);
				targetScale.y *= -1;
				origScale.y *= -1;
				transform.localScale = newY;
//				transform.localScale = targetScale;
//				StartCoroutine (transform.ScaleTo (origScale, currentSpeed, bounceEaser, null));
				scaler.ChangeScale (origScale, currentSpeed, bounceEaser);
		}
	


		void HandleOnBounceIncrease (int numBounces)
		{
				
				float speed = speed1;
				if (numBounces < 8) {
						speed = speed1;
				} else if (numBounces >= 8 && numBounces < 13) {
						speed = speed2;

				} else if (numBounces >= 13) {
						speed = speed3;

				}
				currentSpeed = speed;
		}
		
	
		// Update is called once per frame
		void Update ()
		{
				if (shouldCount) {
						
						if (time >= 0) {
								time -= Time.deltaTime;

						} else if (time < 0) {
								shouldCount = false;
								time = startTime;
								ReturnToNormal ();
						}
				}
			
		}
	#region coroutineFades

//		void Grumzy ()
//		{
//				//debug.log ("grumzy");
//
//				StartCoroutine (sprGlow.FadeSprite (0, duration / 2, 0, null));
//				StartCoroutine (grumzyRender.FadeSprite (1, duration / 2, 0, null));
//				StartCoroutine (standardRender.FadeSprite (0, duration / 2, 0, null));
//				StartCoroutine (powerdownRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (invulRender.FadeSprite (0, duration, 0, null));
//		}
//
//
//		void Zombie ()
//		{
//
//				StartCoroutine (standardRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (sprGlow.FadeSprite (0, duration, 0, null));
//				StartCoroutine (grumzyRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (powerdownRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (invulRender.FadeSprite (0, duration, 0, null));
//		}
//


	
//	
//		void Powerdown ()
//		{
//				//debug.log ("powerdown");
////			
//				StartCoroutine (sprGlow.FadeSprite (1, duration * 2, 0, null));
//				StartCoroutine (standardRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (powerdownRender.FadeSprite (1, duration / 2, 0, null));
//		}
//
//		void Standard ()
//		{
//				//debug.log ("standard");
////				
//				StartCoroutine (zombieGlow.FadeSprite (0, duration / 2, 0, null));
//				StartCoroutine (standardRender.FadeSprite (1, duration / 2, 0, null));
//				StartCoroutine (sprGlow.FadeSprite (0, duration, 0, null));
//				StartCoroutine (grumzyRender.FadeSprite (0, duration / 2, 0, null));
//				StartCoroutine (powerdownRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (invulRender.FadeSprite (0, duration, 0, null));
//
//		}
//
//		void Invul ()
//		{
////				
//				StartCoroutine (sprGlow.FadeSprite (0, duration / 2, 0, null));
//				StartCoroutine (invulRender.FadeSprite (1, duration / 2, 0, null));
//				StartCoroutine (grumzyRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (powerdownRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (standardRender.FadeSprite (0, duration, 0, null));
//		}
//
//		void DisableTrail ()
//		{
////				
//				StartCoroutine (sprGlow.FadeSprite (0, duration / 2, 0, null));
//				StartCoroutine (standardRender.FadeSprite (0, duration / 2, 0, null));
//				StartCoroutine (grumzyRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (powerdownRender.FadeSprite (0, duration, 0, null));
//				StartCoroutine (invulRender.FadeSprite (0, duration, 0, null));
//		}
#endregion

	#region updateFades

		void StartFadeTowards ()
		{
				standardFade = standardRender.gameObject.AddComponent<FadeTowards> ();
				pdFade = powerdownRender.gameObject.AddComponent<FadeTowards> ();
				invFade = invulRender.gameObject.AddComponent<FadeTowards> ();
				grumFade = grumzyRender.gameObject.AddComponent<FadeTowards> ();
				glowFade = sprGlow.gameObject.AddComponent<FadeTowards> ();
				zombieFade = zombieGlow.gameObject.AddComponent<FadeTowards> ();

		}

		void ReturnToNormal ()
		{
				//print ("firing normalizer trail");
				if (shouldFadeGlow) {
			
						zombieFade.ChangeColor (0, duration / 2);
						glowFade.ChangeColor (0, duration);
				}
				standardFade.ChangeColor (1, duration * 5);
				grumFade.ChangeColor (0, duration / 2);
				pdFade.ChangeColor (0, duration);
				invFade.ChangeColor (0, duration * 1.5f);
		}
		void Standard ()
		{
//				print ("firing standard trail");
				if (shouldFadeGlow) {
			
						zombieFade.ChangeColor (0, duration / 2);
						glowFade.ChangeColor (0, duration);
				}
				standardFade.ChangeColor (1, duration);
				grumFade.ChangeColor (0, duration / 2);
				pdFade.ChangeColor (0, duration);
				invFade.ChangeColor (0, duration);
		}
		void Powerdown ()
		{
				if (shouldFadeGlow) {
			
						glowFade.ChangeColor (1, duration * 2);
						zombieFade.ChangeColor (0, duration / 2);
				}
				grumFade.ChangeColor (0, duration / 2);
				standardFade.ChangeColor (0, duration);
				pdFade.ChangeColor (1, duration / 2);
				invFade.ChangeColor (0, duration);
		}
		void Invul ()
		{
				//print ("IS INVUL");
				if (shouldFadeGlow) {
			
						zombieFade.ChangeColor (0, duration / 2);
						glowFade.ChangeColor (0, duration / 2);
				}
				grumFade.ChangeColor (0, duration);
				standardFade.ChangeColor (0, duration);
				pdFade.ChangeColor (0, duration);
				invFade.ChangeColor (1, duration / 2);
		}
		void Zombie ()
		{
				if (shouldFadeGlow) {
			
						grumFade.ChangeColor (0, duration);
						zombieFade.ChangeColor (1, duration);
				}
				glowFade.ChangeColor (0, duration * 4);
				standardFade.ChangeColor (0, duration);
				pdFade.ChangeColor (0, duration * 4);
				invFade.ChangeColor (0, duration);
		}
		void Grumzy ()
		{
				if (shouldFadeGlow) {
			
						zombieFade.ChangeColor (0, duration * 2);
						glowFade.ChangeColor (0, duration / 2);
				}
				grumFade.ChangeColor (1, duration * 3);
				standardFade.ChangeColor (0, duration / 2);
				pdFade.ChangeColor (0, duration);
				invFade.ChangeColor (0, duration);
		}
		void DisableTrail ()
		{
				if (shouldFadeGlow) {

						zombieFade.ChangeColor (0, duration / 2);
						glowFade.ChangeColor (0, duration / 2);
				}
				grumFade.ChangeColor (0, duration / 2);
				standardFade.ChangeColor (0, duration / 2);
				pdFade.ChangeColor (0, duration / 2);
				invFade.ChangeColor (0, duration / 2);
		}
	#endregion
}

