﻿using UnityEngine;
using System.Collections;

public class PlaySoundAfterDelay : MonoBehaviour
{

		public float delay = 1.8f;
		public AudioSource audioToPlay;
		// Use this for initialization
		void OnEnable ()
		{
				StartCoroutine (Auto.Wait (delay, () => {
						audioToPlay.Play ();
				}));
		}
}
