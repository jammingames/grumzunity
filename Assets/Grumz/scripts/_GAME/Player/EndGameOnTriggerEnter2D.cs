using UnityEngine;
using System.Collections;
using k;

public class EndGameOnTriggerEnter2D : MonoBehaviour
{

		public GameObject playerEffect;
		public GameObject playerSprite, playerLines;
		public GameObject shieldEffect;
		public GameObject bombEffect;
		public GameObject shieldExplosionPrefab;
		
		FadeChildrenSprites fadeLines;
		
		bool delayBomb = false;
		bool bombDisabled = false;
		public bool isOverheated = false;
		
		
		public float collisionDelay = 0.5f;

		
		public bool hasShield = false;
		public bool invulnerable = false;
		public float duration = 2;
		
		public bool hasBomb = false;
		bool hasDied = false;
		
		public MoveChildRigidbody2D mcSprites;

		
		float slowSpeed, origSpeed;
		
		
		
		
		
		public GameObject origGObj, invulGObj;
		PlayerMovement player;
		public MoveChildRigidbody2D mover;
		Collider2D _col;
		public AudioSource activateShieldAudio, activateInvulAudio, removeInvulAudio, invulBuzz, removeShieldAudio, dieAudio, dieAudio2, bombUpSnd;
		
		float invulVolume;
		bool canDie = true;


		void Awake ()
		{
				if (GetComponent<Collider2D> ())
						_col = GetComponent<Collider2D> ();
				if (GetComponent<PlayerMovement> ()) {
						//
						player = GetComponent<PlayerMovement> ();
						//						player.enabled = false;
				}
				fadeLines = playerLines.GetComponent<FadeChildrenSprites> ();
		}

	
		void ResetPlayer ()
		{
				hasBomb = false;
				hasDied = false;
				bombEffect.SetActive (false);
				hasShield = false;
				shieldEffect.SetActive (false);
				shieldExplosionPrefab.SetActive (false);
				invulGObj.SetActive (false);
				invulnerable = false;
				_col.enabled = true;
				origGObj.SetActive (true);
				invulVolume = invulBuzz.volume;
				
		}
	

		

		void OnEnable ()
		{
				ResetPlayer ();
				TogglePlayerSprite (1.6f);	
				
				G.i.OnStateChange += HandleOnStateChange;
				G.i.Overheated += HandleOverheated;
				G.i.OnSuperInvul += SuperInvul;
						
		
		}

		void TogglePlayerSprite (float delay)
		{
				playerSprite.SetActive (false);
				StartCoroutine (Auto.Wait (delay, () => {
						playerSprite.SetActive (true);
						fadeLines.ToggleFadeSprites (false, 0.01f);
				}));
		}

		

		void HandleOverheated (bool isStart)
		{

				isOverheated = isStart;
				if (isStart && hasBomb) {
						bombDisabled = true;
						HasBomb (false);
				}
				if (!isStart && bombDisabled) {
						HasBomb (true);
						bombDisabled = false;
						isOverheated = false;
				}
		}

		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == GameState.StartGame) {
						gameObject.SetActive (true);
						origGObj.SetActive (false);
						origGObj.SetActive (true);
				}
		}

		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.Q)) {
						canDie = !canDie;
						Debug.Log ("godMode = " + !canDie);
				}
				if (Input.GetKeyDown (KeyCode.D)) {
						canDie = true;
						CollideDeath (true, Vector3.zero);
				}

				if (Input.GetKeyDown (KeyCode.W)) {
						if (!hasShield)
								HasShield (true);
						HasBomb (!bombEffect.activeSelf);
						
				}
				if (Input.GetKeyDown (KeyCode.E)) {
						HasShield (true);
				}
				if (Input.GetKeyDown (KeyCode.I)) {
						HasInvul (true);
				}
		
				

		}


	
		void OnTriggerEnter2D (Collider2D col)
		{
//				//debug.log ("collided with " + col.name);
				if (col.tag != "world") {
						
						
						if (col.tag == "shield" && !hasShield && !hasBomb) {
								HasShield (true);
			
						} else if (col.tag == "shield" && hasShield && !hasBomb && !G.i.superInvul && !invulnerable) {
								HasBomb (true);
						} else if (col.tag == "invulnerable" && !invulnerable && !hasShield) {
								HasInvul (true);
						
						} else if (hasShield && !invulnerable && col.tag != "shield" && col.tag != "invulnerable") {
								HasShield (false);
						} else if (hasBomb && hasShield && isOverheated && !invulnerable && col.tag != Tags.SHIELD && col.tag != Tags.INVULNERABLE) {
								HasShield (false);
								bombDisabled = false;
								HasBomb (false);
						} else if (!invulnerable && !hasShield && canDie) {
//								Explode (col.transform.position);
								//debug.log ("died to:  " + col.name);
								CollideDeath (true, col.transform.position);
			
						} else if (!invulnerable && !hasShield && !canDie) {
								CollideDeath (false, col.transform.position);
						} else if (invulnerable && col.tag != "shield" && col.tag != "invulnerable") {
								G.i.TriggerBounceAdd (2);
						} else if (col.tag == "invulnerable" && hasShield && !G.i.superInvul) {
								////debug.log ("SUPER INVINCINIBNINIBALBKALSKGLAKTTYYYY");
								G.i.TriggerSuperInvul (true, 8);
								
						} else if (invulnerable && !hasShield && col.tag == "shield" && !isOverheated) {
								Debug.Log ("no shield added");
						} else if (invulnerable && hasShield && col.tag == "shield" && !isOverheated) {
								Debug.Log ("no bomb added");
						} else if (invulnerable && hasShield && hasBomb && col.tag == "shield" && !isOverheated) {
								Debug.Log ("no way youre getting trip bomb");
						}
						
						
				}
		}

		void DisableColliderForDelay ()
		{
		
				_col.enabled = false;
				StartCoroutine (Auto.Wait (collisionDelay, () => {
						_col.enabled = true;
			
				}));
		}

		void DisableColliderForDelay (float t)
		{
		
				_col.enabled = false;
				StartCoroutine (Auto.Wait (t, () => {
						_col.enabled = true;
			
				}));
		}

		void RemoveShield ()
		{
				shieldExplosionPrefab.transform.position = transform.position;
				shieldExplosionPrefab.SetActive (true);
		}

		void ActivateBombHalo ()
		{
				DisableColliderForDelay (1);
				bombEffect.SetActive (true);
		}

		void DeactivateBombHalo ()
		{
				bombEffect.BroadcastMessage ("Disable", SendMessageOptions.DontRequireReceiver);
		}

		public void HasBomb (bool isGood)
		{
				if (isGood) {
						DisableColliderForDelay ();
						bombUpSnd.Play ();
						hasBomb = true;
						PowerupSpawnManager.instance.ShieldSpawn (true);
						ActivateBombHalo ();
				} else {
						PowerupSpawnManager.instance.ShieldSpawn (false);
						hasBomb = false;
						DeactivateBombHalo ();
						
				}
		}

		void HasShield (bool getShield)
		{
				if (getShield && !hasShield) {
						DisableColliderForDelay ();
						G.i.TriggerPlayerShield (true);
						AudioManager.instance.PlayAudio (activateShieldAudio);
						hasShield = true;
						shieldEffect.SetActive (true);
						//mover.DisableCollider ();
						//_col.enabled = false;
//						if (!invulnerable) {
//								G.i.TriggerPlayerInvul (false);
//								invulGObj.SetActive (false);
//						}
						
				} else if (!getShield) {
						RemoveShield ();
						
						

						G.i.TriggerPlayerShield (false);
						AudioManager.instance.PlayAudio (removeShieldAudio);
//						StartCoroutine (Auto.Wait (0.05f, () => {

						//shieldEffect.SetActive (false);
//						}));
						StartCoroutine (Auto.Wait (0.25f, () => {
								//mover.EnableCollider ();
								////_col.enabled = true;
								hasShield = false;
						}));

				}
		}
		
	
	
		void SuperInvul (bool isInvul, float length)
		{
				if (isInvul) {

						G.i.TriggerPlayerInvul (true);
						//_col.enabled = false;
						//mover.DisableCollider ();
						AudioManager.instance.PlayAudio (activateInvulAudio);
						StartCoroutine (Auto.Wait (activateInvulAudio.clip.length, () => {
								invulBuzz.volume = invulVolume;
								AudioManager.instance.PlayAudio (invulBuzz);
						}));
						shieldEffect.SetActive (false);
						invulGObj.SetActive (true);

						if (hasBomb) {
								delayBomb = true;
								DeactivateBombHalo ();
								hasBomb = false;
						}
			

						invulnerable = true;
						StartCoroutine (Auto.Wait (length + 0.9f, () => {
								StartCoroutine (invulBuzz.ChangeVolume (invulBuzz.volume, 0, 0.2f, null));
								AudioManager.instance.PlayAudio (removeInvulAudio);
						}));
						StartCoroutine (Auto.Wait (length + 1.2f, () => {
								G.i.TriggerSuperInvul (false, 0);
						}));
				} else if (!isInvul) {


						HasInvul (false);
				}

		}

		void HasInvul (bool hasInvul)
		{

				if (hasInvul) {
						DisableColliderForDelay ();
						G.i.TriggerPlayerInvul (true);
						//_col.enabled = false;
						//mover.DisableCollider ();
						AudioManager.instance.PlayAudio (activateInvulAudio);
						shieldEffect.SetActive (false);
						if (hasBomb) {
								delayBomb = true;
								DeactivateBombHalo ();
								hasBomb = false;
						}
						invulGObj.SetActive (true);
						

						invulnerable = true;
						StartCoroutine (Auto.Wait (duration * 0.9f, () => {
								AudioManager.instance.PlayAudio (removeInvulAudio);
						}));
						StartCoroutine (Auto.Wait (duration, () => {
								HasInvul (false);
						}));
				} else {

						G.i.TriggerPlayerInvul (false);
						invulnerable = false;
						invulGObj.SetActive (false);
						//_col.enabled = true;
						//mover.EnableCollider ();
					
						if (hasShield) {
								shieldEffect.SetActive (true);

						}
						if (delayBomb) {
								hasBomb = true;
								delayBomb = false;
								ActivateBombHalo ();
						}

						

				}
		}

		void CollideDeath (bool canDie, Vector3 hitPos)
		{
				if (canDie) {
						
						
						if (!hasDied) {
								_col.enabled = false;
								
								playerSprite.SetActive (false);
								fadeLines.ToggleFadeSprites (true, 0.01f);
//								playerEffect.transform.position = hitPos;
//								playerEffect.SetActive (true);
								hasDied = true;
								dieAudio.pitch = 1;
								AudioManager.instance.PlayAudio (dieAudio);
								StartCoroutine (dieAudio.ChangePitch (dieAudio.pitch, 0.8f, 1f, EaseType.QuadIn, null));
			

//								mcSprites.FadeEnd ();
								if (hitPos.x < transform.position.x && hitPos.y > transform.position.y) {
										G.i.TriggerOnCollision (HitPosition.TopLeft);
								} else if (hitPos.x > transform.position.x && hitPos.y > transform.position.y) {
										G.i.TriggerOnCollision (HitPosition.TopRight);
								} else if (hitPos.x < transform.position.x && hitPos.y < transform.position.y) {
										G.i.TriggerOnCollision (HitPosition.BotLeft);
								} else if (hitPos.x > transform.position.x && hitPos.y < transform.position.y) {
										G.i.TriggerOnCollision (HitPosition.BotRight);
								} else
										G.i.TriggerOnCollision (HitPosition.Center);
								
								G.i.TriggerPlayerDeath ();
								player.enabled = false;
								
								StartCoroutine (Auto.Wait (2.0f, () => {
										player.enabled = true;
										gameObject.SetActive (false);
//										Debug.Log ("PLAYER SCORE WAS:   " + G.i.numBounces);
										G.i.SetGameState (GameState.End);
								}));
						}
				} else {

						StartCoroutine (Auto.Wait (0.1f, () => {
								origGObj.SetActive (false);
								origGObj.SetActive (true);
						}));
				}
		}


}
