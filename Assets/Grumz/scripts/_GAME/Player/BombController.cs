﻿using UnityEngine;
using System.Collections;
using k;

public class BombController : MonoBehaviour
{

		
		public int numFrames;
		public float duration;
		public EaseType easer;
		public AudioSource kabooooooooom;
		
		public bool isFirstTap;
		int origFrames;
		bool hasDropped = false;
		public GameObject bombObject;
		public SpriteRenderer bombRender;
		Vector3 origScale;
		Vector3 targetScale;
		
		Transform parent;
		
//		int timesBombed = 0;

		EndGameOnTriggerEnter2D endTrigger;
		
		void Awake ()
		{
				endTrigger = GetComponent<EndGameOnTriggerEnter2D> ();
				
				parent = G.i.playerObj.transform;
				bombRender = bombObject.GetComponentInChildren<SpriteRenderer> ();
				origScale = bombObject.transform.localScale;
				targetScale = new Vector3 (10, 10, 1);
				origFrames = numFrames;

				G.i.OnSuperInvul += HandleOnSuperInvul;
				G.i.OnPlayerInvul += HandleOnPlayerInvul;
				
		}

		void HandleOnPlayerInvul (bool isStart)
		{
				if (isStart) {
						StartCoroutine (Auto.Wait ((7 * 0.9f), () => {
								DropDaBomb ();
						}));
				}
		}

		void HandleOnSuperInvul (bool isStart, float duration)
		{
				if (isStart) {

						StartCoroutine (Auto.Wait ((duration * 0.9f), () => {
								DropDaBomb ();
						}));
				}
		}

		// Use this for initialization
		void OnEnable ()
		{	
				StopAllCoroutines ();
				bombObject.transform.localScale = origScale;
				hasDropped = false;
				bombRender.color = Color.white;
				
				
				
				numFrames = origFrames;
				isFirstTap = false;
				bombObject.SetActive (false);


				
		}
		
		// Update is called once per frame
		void Update ()
		{
				if (endTrigger.hasBomb && !endTrigger.invulnerable) {

						if (Input.anyKeyDown && !isFirstTap) {
								isFirstTap = true;
								
						} else if (isFirstTap && numFrames > 0) {
								numFrames--;
						
								if (Input.anyKeyDown) {
										DropDaBomb ();
										numFrames = origFrames;
										isFirstTap = false;
								
								}
								if (numFrames <= 0) {
										isFirstTap = false;
										numFrames = origFrames;
								}
						}
				}

				if (Input.GetKeyDown (KeyCode.B)) {
						DropDaBomb ();
				}
		}


		void OnTriggerEnter2D (Collider2D col)
		{
				if (endTrigger.hasBomb && col.gameObject.layer == Layers.ENEMY && !endTrigger.invulnerable && !endTrigger.isOverheated)
						DropDaBomb ();
		}

		void DropDaBomb ()
		{
				if (!hasDropped) {
						if (kabooooooooom != null)
								AudioManager.instance.PlayAudio (kabooooooooom);
						PowerupSpawnManager.instance.ShieldSpawn (false);
						
						//endTrigger.hasBomb = false;
						
						hasDropped = true;
						//bombObject.transform.position = parent.position;
						bombObject.transform.localScale = Vector3.one;
						bombObject.transform.localPosition = Vector3.zero;
						bombObject.SetActive (true);
						bombObject.transform.parent = null;
						//	endTrigger.bombEffect.SetActive (false);
//						StartCoroutine (bombRender.FadeSprite (1, 0, duration / 2, 0.1f, null));

						endTrigger.HasBomb (false);
						StartCoroutine (bombRender.FadeSprite (1, 0, duration / 2, duration / 2, () => {
//										bombObject.SetActive (false);
						}));
						StartCoroutine (bombObject.transform.ScaleTo (targetScale, duration, easer, () =>
						{
								G.i.TriggerBombFire ();
								hasDropped = false;
								bombObject.transform.parent = parent;
								bombObject.transform.localScale = origScale;
								bombObject.transform.localPosition = Vector3.zero;
								StartCoroutine (bombRender.FadeSprite (0, 1, 0.0001f, 0, null));
								bombObject.SetActive (false);
								
						}));
				}
		}
}
