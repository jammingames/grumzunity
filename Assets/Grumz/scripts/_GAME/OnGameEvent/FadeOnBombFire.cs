﻿using UnityEngine;
using System.Collections;

public class FadeOnBombFire : MonoBehaviour
{
	
	
		public float duration, delay;
	
		SpriteRenderer spr;
	
		public bool randomize;
	
	
		void Awake ()
		{
		
				spr = GetComponent<SpriteRenderer> ();
		}
	
		// Use this for initialization
		void OnEnable ()
		{
				G.i.OnFireBomb += HandleOnFireBomb;
		}

		void HandleOnFireBomb ()
		{
				
				if (randomize) {
						duration *= (Random.value + 0.1f);
				}
				if (spr != null) {

						StartCoroutine (spr.FadeSprite (spr.color.a, 0, duration, delay, null));	
				}
		}

	
		void OnDestroy ()
		{
				StopAllCoroutines ();
				if (G.i != null)
						G.i.OnFireBomb -= HandleOnFireBomb;
		}


	
		void OnDisable ()
		{
				StopAllCoroutines ();
				if (G.i != null)
						G.i.OnFireBomb -= HandleOnFireBomb;
		}
	

}
