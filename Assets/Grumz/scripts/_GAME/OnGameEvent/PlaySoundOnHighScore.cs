﻿using UnityEngine;
using System.Collections;

public class PlaySoundOnHighScore : MonoBehaviour
{


		public AudioSource congrationSnd;
		// Use this for initialization
		void Awake ()
		{
				ScoreManager.instance.OnHighScore += HandleOnHighScore;
		}

		void OnEnable ()
		{
				StartCoroutine (Auto.Wait (0.2f, () => {
			
						AudioManager.instance.PlayAudio (congrationSnd);
				}));
		}

		void HandleOnHighScore (bool isNewScore, int newScore)
		{
				if (isNewScore && !gameObject.activeInHierarchy)
						gameObject.SetActive (true);
				if (isNewScore) {
						StartCoroutine (Auto.Wait (0.2f, () => {

								AudioManager.instance.PlayAudio (congrationSnd);
						}));
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
