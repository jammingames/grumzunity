﻿using UnityEngine;
using System.Collections;

public class ScaleOnPlayerDeath : MonoBehaviour
{
	
		Vector3 origScale;
		public float duration;
		public Vector3 targetScale;
		public Vector3 offScale;
		public EaseType easer;
	
	
	

		public GameState onState;
		//public bool MoveBeforeDisable = false;
	
	
		void OnEnable ()
		{
				transform.localScale = origScale;
		}
	
		// Use this for initialization
		void Awake ()
		{
				//				targetScale.y = transform.localScale.y;
				//				targetScale.z = transform.localScale.z;
				origScale = transform.localScale;
				G.i.OnStateChange += HandleOnStateChange;
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
				//enabled = false;
		}

		void HandleOnPlayerDeath ()
		{
				StartCoroutine (transform.ScaleTo (offScale, duration, easer, null));
		}
	
	
		void HandleOnStateChange (GameState state)
		{
				if (state == onState)
						StartCoroutine (transform.ScaleTo (targetScale, duration, easer, null));
		
		}
	
		public void Scale ()
		{
				targetScale.y = transform.localScale.y;
				targetScale.z = transform.localScale.z;
				origScale = transform.localScale;
				StartCoroutine (transform.ScaleTo (targetScale, duration, easer, () => {
						//enabled = false;
				}));
		}
	
		public void Scale (bool orig)
		{
				StartCoroutine (transform.ScaleTo (origScale, duration, easer, null));
		}
	
		void Disable ()
		{
				gameObject.SetActive (false);
		}
}
