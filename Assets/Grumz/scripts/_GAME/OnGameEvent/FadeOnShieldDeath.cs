﻿using UnityEngine;
using System.Collections;

public class FadeOnShieldDeath : MonoBehaviour
{


		public float duration, delay;
		public EaseType easer;

		SpriteRenderer spr;

		public bool randomize;


		void Awake ()
		{

				spr = GetComponent<SpriteRenderer> ();
		}

		// Use this for initialization
		void OnEnable ()
		{
				G.i.OnPlayerShield += HandleOnPlayerShield;
				;
		}

		void HandleOnPlayerShield (bool isStart)
		{
				if (!isStart) {
						Color bleh = spr.color;
						bleh.a = 1;
						spr.color = bleh;
						if (randomize) {
				
								duration *= (Random.value + 0.1f);
//								//debug.log (duration);
						}
						StartCoroutine (spr.FadeSprite (spr.color.a, 0, duration, delay, null));
				} else if (isStart) {

				}
		}
	

		void OnDisable ()
		{

				
				if (G.i != null)
						G.i.OnPlayerShield -= HandleOnPlayerShield;
		}

		
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
