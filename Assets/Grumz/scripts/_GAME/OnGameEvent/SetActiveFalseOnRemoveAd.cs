﻿using UnityEngine;
using System.Collections;

public class SetActiveFalseOnRemoveAd : MonoBehaviour
{

		// Use this for initialization
		void Awake ()
		{
				G.i.OnRemoveAds += HandleOnRemoveAds;
		}

		void HandleOnRemoveAds ()
		{
				gameObject.SetActive (false);
		}

}
