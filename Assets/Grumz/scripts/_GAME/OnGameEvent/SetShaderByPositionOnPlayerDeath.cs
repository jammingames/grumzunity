﻿using UnityEngine;
using System.Collections;

public class SetShaderByPositionOnPlayerDeath : MonoBehaviour
{

		public Shader defaultShader, newMaskingShader;
		SpriteRenderer spr;
		// Use this for initialization
		void Start ()
		{
				spr = GetComponent<SpriteRenderer> ();

			
		}
		void OnEnable ()
		{
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
				spr.material.shader = defaultShader;
		}


		void OnDisable ()
		{
				if (G.i != null) {
						G.i.OnPlayerDeath -= HandleOnPlayerDeath;
				}
		}
		void HandleOnPlayerDeath ()
		{
				spr.material.shader = newMaskingShader;
				GetComponent<Renderer> ().material.SetFloat ("_MaskPosition", 0);
				if (transform.position.x > 0)
						GetComponent<Renderer> ().material.SetFloat ("_Forwards", 1);
				else
						GetComponent<Renderer> ().material.SetFloat ("_Forwards", 0);
		}
		
}

