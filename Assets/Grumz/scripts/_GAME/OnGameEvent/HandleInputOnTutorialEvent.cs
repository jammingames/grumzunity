﻿using UnityEngine;
using System.Collections;

public class HandleInputOnTutorialEvent : MonoBehaviour
{


		bool canInput = false;
//		bool isInputting = false;
		public float duration = 0;
		public float timeToHold = 0.5f;

		void Awake ()
		{
				G.i.OnTutorial += HandleOnTutorial;
		}

		void HandleOnTutorial (bool isStart)
		{
				canInput = isStart;
				if (!isStart) {
						duration = 0;
				}

		}
	
		// Update is called once per frame
		void Update ()
		{
				if (canInput) {
						if (Input.anyKey || Input.GetMouseButton (0)) {
								duration += Time.deltaTime;
						}
						if (duration >= timeToHold) {
								canInput = false;
								G.i.TriggerStartTutorial (false);
						}


				}

		}
}
