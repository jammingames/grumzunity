﻿using UnityEngine;
using System.Collections;
//using UnityEngine.UI;

public class AdjustSortingOnGameStart : MonoBehaviour
{

		
		
		void Awake ()
		{
				
				G.i.OnStateChange += HandleOnStateChange;
		}

		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == GameState.Game) {
						
						GetComponent<SpriteRenderer> ().sortingLayerID = 0;
						G.i.OnStateChange -= HandleOnStateChange;
						
				}
		}
		
}
