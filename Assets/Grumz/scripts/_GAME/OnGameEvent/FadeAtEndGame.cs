﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeAtEndGame : MonoBehaviour
{


	

		
		public float duration = 1f;
		SpriteRenderer spr;
		Image img;
		
		public bool isImage = false;


		void OnEnable ()
		{
				if (GetComponent<Image> ()) {
						img = GetComponent<Image> ();
						StartCoroutine (img.FadeImageAlpha (img.color.a, 1, 0.01f, null));
				}
				if (GetComponent<SpriteRenderer> ()) {
						spr = GetComponent<SpriteRenderer> ();
						StartCoroutine (spr.FadeSprite (spr.color.a, 1, 0.01f, 0, null));
				}



		}

		public void FadeSprite ()
		{
				//var sprite;
				if (GetComponent<Image> ()) {
						img = GetComponent<Image> ();
						StartCoroutine (img.FadeImageAlpha (img.color.a, 0, duration, null));
				}

				if (GetComponent<SpriteRenderer> ()) {
						spr = GetComponent<SpriteRenderer> ();
						float start = spr.color.a;
						StartCoroutine (spr.FadeSprite (start, 0, duration, 0, null));
								
				}
			
			
			
		}


}
