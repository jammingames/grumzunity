﻿using UnityEngine;
using System.Collections;

public class FadeLineRendererOnPlayerDeath : MonoBehaviour
{


		public float duration = 0.5f;
		public EaseType easer;
		LineRenderer line;
		Material mat;

		void Awake ()
		{
				line = GetComponent<LineRenderer> ();
				mat = line.material;
		}

		// Use this for initialization
		void OnEnable ()
		{
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
		}

		void HandleOnPlayerDeath ()
		{
				StartCoroutine (mat.FadeColor (1, 0, duration, null));
	
		}
	
		// Update is called once per frame
		void OnDisable ()
		{
				G.i.OnPlayerDeath -= HandleOnPlayerDeath;
		}
}
