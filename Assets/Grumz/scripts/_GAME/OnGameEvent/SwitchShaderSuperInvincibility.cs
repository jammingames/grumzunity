﻿using UnityEngine;
using System.Collections;

public class SwitchShaderSuperInvincibility : MonoBehaviour
{
	
		public SpriteMask maskLeft, maskRight;
		

		void OnEnable ()
		{
				
				G.i.OnSuperInvul += HandleOnSuperInvul;
//				//debug.log (transform.position.x);
				
				
		}

		void OnDisable ()
		{
				if (G.i != null) {

						G.i.OnSuperInvul -= HandleOnSuperInvul;
				}
		}

		// Use this for initialization
		void Awake ()
		{
				

		}

		void HandleOnSuperInvul (bool isStart, float length)
		{
				if (isStart) {
						maskLeft.enabled = false;
						maskRight.enabled = false;
//						if (origX < 0 && transform.position.x < 0) {
//								render.material = defaultMat;
//								////debug.log ("changing to default mat");
//						} else if (origX > 0 && transform.position.x > 0) {
//								////debug.log ("changing to default mat");
//								render.material = defaultMat;
//						}
				} else {
						maskLeft.enabled = true;
						maskRight.enabled = true;
//						render.material = maskMat;
//						render.material.SetFloat ("_MaskPosition", 0);
//						if (origX < 0) {
//								render.material.SetFloat ("_Forwards", 0);
//						} else
//								render.material.SetFloat ("_Forwards", 1);
				}
				
		}

		
	
		// Update is called once per frame
		void Update ()
		{
			
		}
}
