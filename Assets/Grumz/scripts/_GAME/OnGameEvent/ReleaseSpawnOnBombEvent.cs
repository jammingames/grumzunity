﻿using UnityEngine;
using System.Collections;

public class ReleaseSpawnOnBombEvent : MonoBehaviour
{

		SpawnPoint spawn;

		// Use this for initialization
		void Awake ()
		{
				spawn = GetComponent<SpawnPoint> ();
				G.i.OnFireBomb += HandleOnFireBomb;
		}

		void HandleOnFireBomb ()
		{
				spawn.ResetSpawnPoint ();

		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
