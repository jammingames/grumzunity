﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeOnPlayerDeath : MonoBehaviour
{


		public float duration, delay;
		public EaseType easer;
		float factor = 0.5f;
		public SpriteRenderer spr;
		Graphic img;
		public bool isChild;
		
		Color tempCol = Color.white;
		public bool randomize;


		void Awake ()
		{
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
				if (GetComponent<SpriteRenderer> ())
						spr = GetComponent<SpriteRenderer> ();
				if (GetComponent<Graphic> ())
						img = GetComponent<Graphic> ();

				if (isChild) {
						if (GetComponentInChildren<SpriteRenderer> ())
								spr = GetComponent<SpriteRenderer> ();
						if (GetComponentInChildren<Graphic> ())
								img = GetComponent<Graphic> ();
				}
		}

		// Use this for initialization
		void OnEnable ()
		{
//				if (isChild) {
//						if (GetComponentInChildren<SpriteRenderer> ())
//								spr = GetComponent<SpriteRenderer> ();
//						if (GetComponentInChildren<Text> ())
//								img = GetComponent<Graphic> ();
//				}
				
		}

		void OnDisable ()
		{
				StopAllCoroutines ();
				if (img != null) {
			
						tempCol = img.color;
						tempCol.a = 1;
						img.color = tempCol;
				}
				if (spr != null) {
						tempCol = spr.color;
						tempCol.a = 1;
						spr.color = tempCol;
				}
		}

		void OnDestroy ()
		{
				if (G.i != null)
						G.i.OnPlayerDeath -= HandleOnPlayerDeath;

		}

		void HandleOnPlayerDeath ()
		{
				if (gameObject.activeInHierarchy) {


						if (randomize) {
			
								duration *= (Random.value + 0.3f);
								////debug.log (duration);
						}
						if (img != null) {
			
								tempCol = img.color;
								tempCol.a = 1;
								img.color = tempCol;

								StartCoroutine (Auto.Wait (delay, () => {

										StartCoroutine (img.FadeImageAlpha (img.color.a, 0, duration * factor, null));
								}));
						}
						if (spr != null) {
								tempCol = spr.color;
								tempCol.a = 1;
								spr.color = tempCol;
								StartCoroutine (spr.FadeSprite (spr.color.a, 0, duration * factor, delay, null));
						}
				
				}
			
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
