﻿using UnityEngine;
using System.Collections;

public class EnableOrDisableOnHighScore : MonoBehaviour
{
		public bool shouldDisable = false;
		public bool disableAtStart = true;


		public float delay;


		// Use this for initialization
		void Awake ()
		{
				ScoreManager.instance.OnHighScore += HandleOnHighScore;
				if (disableAtStart)
						gameObject.SetActive (false);
		}

		void HandleOnHighScore (bool isNewScore, int newScore)
		{
				

				if (isNewScore) {

						if (shouldDisable) {
								print ("DISABLING THING");
								gameObject.SetActive (false);
						} else {
								gameObject.SetActive (true);
						}
				
				} else if (!isNewScore) {
						if (shouldDisable) {
								gameObject.SetActive (true);
						} else {
								gameObject.SetActive (false);
						}
				}
					
				
		}


}
