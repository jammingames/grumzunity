﻿using UnityEngine;
using System.Collections;

public class ChangeSortingOnShield : MonoBehaviour
{

		SpriteRenderer spr;
		int origLayer;
		int higherLayer = 2;


		void Awake ()
		{
				spr = GetComponent<SpriteRenderer> ();
				origLayer = spr.sortingOrder;
				G.i.OnPlayerShield += HandleOnPlayerShield;
		}

		void HandleOnPlayerShield (bool isStart)
		{
				if (isStart) {
						spr.sortingOrder = higherLayer;
				} else {
						spr.sortingOrder = origLayer;
				}
		}

		
}
