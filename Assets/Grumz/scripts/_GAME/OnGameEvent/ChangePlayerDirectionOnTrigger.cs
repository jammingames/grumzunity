﻿using UnityEngine;
using System.Collections;

public class ChangePlayerDirectionOnTrigger : MonoBehaviour
{
		//bool hasCollided = false;
		public RotateDiagonally rotator;

		// Use this for initialization
		void Start ()
		{
				
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		int CheckQuadrant (Vector3 pos)
		{
				if (pos.x < 0 && pos.y > 0)
						return 0;
				else if (pos.x > 0 && pos.y > 0)
						return 1;
				else if (pos.x < 0 && pos.y < 0)
						return 0;
				else if (pos.x > 0 && pos.y < 0)
						return 1;
				return 4;
		}

		void RotateFromQuadrant (int quadrant)
		{
				if (quadrant == 1) {
						StartCoroutine (rotator.SwingOpen ());
				} else if (quadrant == 0) {
						StartCoroutine (rotator.SwingClosed ());
				}
		}

		void OnTriggerEnter2D (Collider2D col)
		{
				//if (!hasCollided) {
				//		hasCollided = true;
				if (col.GetComponent<PlayerMovement> ()) {
						////debug.log ("collided with player");
//						col.GetComponent<PlayerMovement> ().ChangeDirection ();
						RotateFromQuadrant (CheckQuadrant (transform.localPosition));
				}
				//}
		}

}
