﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeGraphicOnTutorialEnd : MonoBehaviour
{

		
		Graphic img;
		
		
		[Range(0, 1)]
		public float
				target;
		public float duration = 0.3f;
		public float delay = 0;
		public bool disableAtEnd = true;
		
		void Awake ()
		{
				img = GetComponent<Graphic> ();
				G.i.OnTutorial += HandleOnTutorial;
		}

		void HandleOnTutorial (bool isStart)
		{
				if (!isStart) {
						StartCoroutine (Auto.Wait (delay, () => {
								if (img != null)
										StartCoroutine (img.FadeImageAlpha (img.color.a, target, duration, () => {
												if (disableAtEnd) {
														gameObject.SetActive (false);
												}
										}));
						}));
				}
		}
		

		
		void OnDisable ()
		{
				Color col = img.color;
				col.a = 1;
				img.color = col;
			
		}
		
		
		
		


}
