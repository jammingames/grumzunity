﻿using UnityEngine;
using System.Collections;

public class EnableOrDisableOnTutorialEvent : MonoBehaviour
{

		public bool shouldEnable = true;



		void Awake ()
		{
				G.i.OnTutorial += HandleOnTutorial;
				if (gameObject.activeInHierarchy == shouldEnable)
						gameObject.SetActive (!shouldEnable);
		}

		void HandleOnTutorial (bool isStart)
		{
				if (isStart) {
						gameObject.SetActive (shouldEnable);
				}
		}

		


}
