﻿using UnityEngine;
using System.Collections;
using k;

public class Enemy2Controller : MonoBehaviour
{
	
	
	
		public float maxVelocityChange = 0.3f;
		public float speed = 0.03f;
		public float initForce = 10f;
		public float maxSpeed = 3;
		public EaseType easer1;
		public float duration1, delay1;
		public EaseType easer2;
		public float duration2, delay2;
		public Transform upTar, downTar;

		public SpriteRenderer _renderer;
		public float origX;

		public AudioSource splitSnd;
		

//		bool travelForwards;
		Vector3 origScale;
		Vector3 prepPosition;
		Vector3 targetPos;
		MoveToMid[] children;
//		Lasers[] lasers;
	
		float leftEdge, rightEdge;
		Collider2D col;
//		Vector3 origPos;
		Material mat;

		[HideInInspector]
		public EnemySpawnHolder
				spawnHolder;
		
		void Awake ()
		{

				spawnHolder = GetComponent<EnemySpawnHolder> ();
				//	mat = _renderer.material;
				origX = transform.localPosition.x;
				//lasers = GetComponentsInChildren<Lasers> ();
				children = GetComponentsInChildren<MoveToMid> ();
				foreach (MoveToMid child in children) {
						child.gameObject.SetActive (false);
				}
//				foreach (Lasers l in lasers) {
//						l.SetStats (maxVelocityChange, speed, initForce, maxSpeed);		
//						l.gameObject.SetActive (false);
//				}
				leftEdge = G.i.LEFT_SCREEN_EDGE;
				rightEdge = G.i.RIGHT_SCREEN_EDGE;
				origScale = transform.localScale;
				col = GetComponent<Collider2D> ();

				//origPos = transform.localPosition;
				if (GetComponent<DestroyOnTriggerEnter2D> () && AudioManager.instance != null) {
						GetComponent<DestroyOnTriggerEnter2D> ().explosionSnd = AudioManager.instance.g1Death;
				}

//				G.i.OnPlayerDeath += HandleOnPlayerDeath;

		}

//		void HandleOnPlayerDeath ()
//		{
//				StopAllCoroutines ();
//		}

		void HandleKillAll (bool isStart)
		{
				gameObject.Recycle ();
		}
	
	
		void OnEnable ()
		{
				float pos = transform.localPosition.x;
				if (G.i.g2Enemies.Count > 0) {
						pos = -G.i.g2Enemies [0].origX;
				}
					
				//EnemySpawnManager.instance.enemies.
				leftEdge = G.i.LEFT_SCREEN_EDGE;
				rightEdge = G.i.RIGHT_SCREEN_EDGE;
		
				StartCoroutine (PositionAtScreenEdge (pos));
		

		}

		public void HitPlayer ()
		{
				enabled = false;
				
		}

		void OnDisable ()
		{
				StopAllCoroutines ();
				//transform.localPosition = origPos;
			
		}

		IEnumerator PositionAtScreenEdge (float pos)
		{	
				
				//mat.SetFloat ("_MaskPosition", 0);	
				
				if (pos > 0) {
//						mat.SetFloat ("_Forwards", 1);
					
						//travelForwards = false;
						transform.position = new Vector3 (rightEdge + col.bounds.size.x + 1, transform.localPosition.y, transform.localPosition.z);
						prepPosition = new Vector3 ((rightEdge - 0.5f), transform.localPosition.y, transform.localPosition.z);
						targetPos = new Vector3 (leftEdge - col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
						transform.localScale = new Vector3 (-1 * origScale.x, origScale.y, origScale.z);
						
				} else {
						//		mat.SetFloat ("_Forwards", 0);
						//travelForwards = true;
					
						//	transform.localScale = new Vector3 (origScale.x, origScale.y, origScale.z);
						transform.position = new Vector3 (leftEdge - col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
//						//debug.log ("My local pos is " + transform.localPosition);
						prepPosition = new Vector3 ((leftEdge + 0.5f), transform.localPosition.y, transform.localPosition.z);
						//position = (Quaternion.Euler( angleX, angleY, angleZ ) transform.forward ).normalized range;
						targetPos = new Vector3 (rightEdge + col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
						//foreach (MoveToMid child in children) {
						//child.gameObject.transform.localScale = new Vector3 (-1 * origScale.x, -1*origScale.y, origScale.z);
						//}
				}
				yield return new WaitForEndOfFrame ();
				yield return StartCoroutine (MoveToAndFire (delay1, delay2, prepPosition, targetPos));
		}

		public void FireLasers ()
		{
				if (_renderer != null) {
						_renderer.enabled = false;
				}
				if (children [0] != null)
						children [0].spawnHolder = this.spawnHolder;
				foreach (MoveToMid child in children) {
						AudioManager.instance.PlayAudio (splitSnd);
						//SoundKit.instance.playOneShot (splitSnd);
						child.gameObject.SetActive (true);
				}

		}
	
	
	
		IEnumerator MoveToAndFire (float delay1, float delay2, Vector3 pos1, Vector3 pos2)
		{
				//audio.pl
//				AudioManager.instance.PlayAudio (appearanceSnd);
				//SoundKit.instance.playSound (appearanceSnd);
				//appearanceSnd.
				yield return StartCoroutine (transform.MoveTo (pos1, duration1, easer1, null));
				yield return StartCoroutine (Auto.Wait (delay1));
				FireLasers ();
//				StartCoroutine (transform.MoveTo (pos1, duration1, easer1, () => {
//						StartCoroutine (Auto.Wait (delay1, () => {
//								FireLasers ();}));	
//				}));
		
		
				yield return null;
		}
}
