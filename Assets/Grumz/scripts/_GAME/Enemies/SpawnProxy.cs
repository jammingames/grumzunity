﻿using UnityEngine;
using System.Collections;

public class SpawnProxy : MonoBehaviour
{


		public float timeToHoldSpawnPos = 8.0f;
		float t = 0;
		Vector3 pos;

		// Use this for initialization
		void Start ()
		{
				timeToHoldSpawnPos = 8;
				transform.parent = null;
				pos = transform.position;
				EnemySpawnManager.instance.usedSpawnPositions.Add (pos);
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (t < timeToHoldSpawnPos) {
						t += Time.deltaTime;
				} else {
						EnemySpawnManager.instance.usedSpawnPositions.Remove (pos);
						Destroy (gameObject);
				}


		}

		
}
