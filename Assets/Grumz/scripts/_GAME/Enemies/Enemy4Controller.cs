﻿using UnityEngine;
using System.Collections;
using k;

public class Enemy4Controller : MonoBehaviour
{
	


		
		public EaseType easer1;
		public float duration1, delay1;
		public EaseType easer2;
		public float duration2, delay2;
		public float angle;


		bool travelForwards;
		Vector3 origScale;
		Vector3 leftPrepPosition, rightPrepPosition;
		Vector3 leftTargetPos, rightTargetPos;

		float leftEdge, rightEdge, topEdge, botEdge;
		
		Collider2D col;
		public GameObject sideMoveObj;
		GameObject leftObj, rightObj;

		public AudioClip appearanceSnd, travelSnd;

		Vector3 origPos;
		void Awake ()
		{
				leftEdge = G.i.LEFT_SCREEN_EDGE;
				rightEdge = G.i.RIGHT_SCREEN_EDGE;
				topEdge = G.i.TOP_SCREEN_EDGE;
				botEdge = G.i.BOT_SCREEN_EDGE;
				origScale = transform.localScale;
				col = GetComponent<Collider2D> ();
				origPos = transform.localPosition;
		}

		
	
		void OnEnable ()
		{

				if (GetComponent<DestroyOnTriggerEnter2D> ()) {
						GetComponent<DestroyOnTriggerEnter2D> ().explosionSnd = AudioManager.instance.g1Death;
				}

				if (GetComponentInChildren<DestroyOnTriggerEnter2D> ())
						GetComponentInChildren<DestroyOnTriggerEnter2D> ().explosionSnd = AudioManager.instance.g1Death;
				StartCoroutine (PositionAtScreenEdge ());
				
		
		
		
		}

		void OnDisable ()
		{
				transform.localPosition = origPos;
		}

		IEnumerator PositionAtScreenEdge ()
		{	
				leftObj = GameObject.Instantiate (sideMoveObj, new Vector3 (leftEdge, topEdge, 0), Quaternion.identity) as GameObject;
				rightObj = GameObject.Instantiate (sideMoveObj, new Vector3 (rightEdge, botEdge, 0), Quaternion.identity) as GameObject;
				
				//leftObj.layer = Layers.LEFT;
				//rightObj.layer = Layers.LEFT;
						
				rightObj.transform.localScale = new Vector3 (-1 * origScale.x, origScale.y, origScale.z);
				
				
				leftPrepPosition = new Vector3 ((leftEdge), transform.localPosition.y, transform.localPosition.z);
				rightPrepPosition = new Vector3 ((rightEdge), transform.localPosition.y, transform.localPosition.z);
						
//				
				rightTargetPos = new Vector3 (leftEdge - col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
				leftTargetPos = new Vector3 (rightEdge + col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
				
				
				
				yield return new WaitForEndOfFrame ();
				StartCoroutine (MoveTo (rightObj.transform, delay1, delay2, rightPrepPosition, rightTargetPos));
				yield return StartCoroutine (MoveTo (leftObj.transform, delay1, delay2, leftPrepPosition, leftTargetPos));
				
		}

		void OnBecameVisible ()
		{
				//		AudioSource.PlayClipAtPoint (appearanceSnd, transform.position);
		}
	
		IEnumerator MoveTo (Transform trans, float delay1, float delay2, Vector3 pos1, Vector3 pos2)
		{
			
				//Vector3 target = pos2.GetPosByAngle (-pos2.x, angle);		
				StartCoroutine (Auto.Wait (duration1 - (duration1 / 1.6f), () => {
						AudioManager.instance.PlayAudio (appearanceSnd);
				}));				
				StartCoroutine (trans.MoveTo (pos1, duration1, easer1, () => {

						StartCoroutine (Auto.Wait (delay1, () => {
								AudioManager.instance.PlayAudio (travelSnd);
								Vector2 force = (trans.localPosition - pos2) / duration2;
								trans.GetComponent<Rigidbody2D> ().velocity = -force;

						}));
						//StartCoroutine (transform.MoveTo (pos2, duration2, easer2, null));
				}));
				
	
				yield return null;
		}
}
