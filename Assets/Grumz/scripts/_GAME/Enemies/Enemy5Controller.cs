﻿using UnityEngine;
using System.Collections;
using k;

public class Enemy5Controller : MonoBehaviour
{
	
	
	
	
		public EaseType easer1;
		public EaseType entranceEaser;
		public float duration1, delay1;
		public EaseType easer2;
		public float duration2, delay2;

		Vector3 prepPos2;
	
//		bool travelForwards;
		Vector3 origScale;
		//Vector3 origPos;
		Vector3 prepPosition;
		Vector3 targetPos;
	
		float leftEdge, rightEdge;
		Collider2D col;
		bool isFired = false;
		Transform boosterRocket;
		public SpriteRenderer mainRender, boosterRender, projectileRender;
		public AudioSource appearanceSnd, travelSnd, appearanceSnd2;
		//Material mat;
//		TrailRenderer trail;

		DestroyAfterDelay destroyer;

		bool stillExists = true;
		
		[HideInInspector]
		public EnemySpawnHolder
				spawnHolder;

		void Awake ()
		{
			
				spawnHolder = GetComponent<EnemySpawnHolder> ();
				boosterRocket = boosterRender.transform;
				//projectile = projectileRender.transform;
				//		trail = GetComponentInChildren<TrailRenderer> ();
				//mat = GetComponentInChildren<Renderer> ().material;
				leftEdge = G.i.LEFT_SCREEN_EDGE;
				rightEdge = G.i.RIGHT_SCREEN_EDGE;
				col = GetComponent<Collider2D> ();
				origScale = transform.localScale;
				//	origPos = transform.localPosition;
	
		}

		
	
		void OnEnable ()
		{
				
				if (GetComponent<DestroyAfterDelay> ())
						destroyer = GetComponent<DestroyAfterDelay> ();
				else
						destroyer = gameObject.AddComponent<DestroyAfterDelay> ();
				destroyer.delay = 3;
				destroyer.enabled = false;
				if (EnemySpawnManager.instance != null)
						EnemySpawnManager.instance.g4Spawns += 1;
				
				Color col = projectileRender.color;
				col.a = 0;
				projectileRender.color = col;
				boosterRender.color = col;
				
//				projectileRender.enabled = false;
//				boosterRender.enabled = false;
				StartCoroutine (StartGame ());
				
		}

		void HandleOnFireBomb ()
		{
				stillExists = false;
				boosterRender.enabled = false;
				StopAllCoroutines ();
				if (G.i != null) {
						G.i.OnPlayerTap -= HandleOverheated;
						G.i.OnFireBomb -= HandleOnFireBomb;
				}
		
		}
		void OnDisable ()
		{
				boosterRender.enabled = false;
//				transform.localPosition = origPos;
				//spawnHolder.ReleaseSpawn ();f
				if (G.i != null) {
						G.i.OnFireBomb -= HandleOnFireBomb;
						G.i.OnPlayerTap -= HandleOverheated;
				}

				StopAllCoroutines ();
		}

		void OnDestroy ()
		{
				if (G.i != null) {
						G.i.OnFireBomb -= HandleOnFireBomb;
						G.i.OnPlayerTap -= HandleOverheated;
				}
				StopAllCoroutines ();
		}
	
		IEnumerator StartGame ()
		{
				isFired = false;
				StartCoroutine (PositionAtScreenEdge ());
				//yield return new WaitForSeconds (1);
				StartCoroutine (Auto.Wait (0.55f, () => {

						AudioManager.instance.PlayAudio (appearanceSnd2);
				}));
				if (stillExists)
						yield return new WaitForSeconds (1);
				//AudioManager.instance.PlayAudio (appearanceSnd);
				if (stillExists) {
						G.i.OnFireBomb += HandleOnFireBomb;
						G.i.OnPlayerTap += HandleOverheated;
				}
		}

		void HandleOverheated ()
		{
				if (!isFired) {
						destroyer.enabled = true;
						////debug.log ("FIRE THE LASERS");
						isFired = true;
						if (mainRender != null)
								mainRender.enabled = false;
						if (projectileRender != null)
								projectileRender.enabled = true;
						if (boosterRender != null)
								boosterRender.enabled = true;
						if (projectileRender != null)
								StartCoroutine (projectileRender.FadeSprite (1, 0.2f, 0, null));
						if (boosterRender != null)
								StartCoroutine (boosterRender.FadeSprite (1, 0.1f, 0, null));
						if (boosterRocket != null)
								boosterRocket.parent = null;
						Vector3 tarPos;
						if (transform.localPosition.x > 0)
								tarPos = Vector3.left;
						else
								tarPos = Vector3.right;
						if (stillExists) {

								if (boosterRocket != null)
										StartCoroutine (MoveTo (boosterRocket, delay1, delay2, prepPosition, boosterRocket.position - tarPos));
								StartCoroutine (MoveTo (delay1, delay2, prepPosition, targetPos));	
						}
				}
		}


		IEnumerator PositionAtScreenEdge ()
		{	
				//	mat.SetFloat ("_MaskPosition", 0);
				if (transform.localPosition.x > 0) {
						//			mat.SetFloat ("_Forwards", 1);
						//gameObject.layer = Layers.RIGHT;
						//		travelForwards = false;
						transform.position = new Vector3 (rightEdge + 1 + (col.bounds.size.x * 2), transform.localPosition.y, transform.localPosition.z);
						prepPosition = new Vector3 ((rightEdge - 0.5f), transform.localPosition.y, transform.localPosition.z);
						prepPos2 = prepPosition;
						prepPos2.x += 0.3f;
						targetPos = new Vector3 (leftEdge - col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
						transform.localScale = new Vector3 (-1 * origScale.x, origScale.y, origScale.z);
				} else {
						//			mat.SetFloat ("_Forwards", 0);
						//	travelForwards = true;
						//gameObject.layer = Layers.LEFT;
						//	transform.localScale = new Vector3 (origScale.x, origScale.y, origScale.z);
						transform.position = new Vector3 (leftEdge - 1 - (col.bounds.size.x * 2), transform.localPosition.y, transform.localPosition.z);
						//						//debug.log ("My local pos is " + transform.localPosition);
						prepPosition = new Vector3 ((leftEdge + 0.5f), transform.localPosition.y, transform.localPosition.z);
						prepPos2 = prepPosition;
						prepPos2.x -= 0.3f;
						targetPos = new Vector3 (rightEdge + col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
				}
				yield return new WaitForEndOfFrame ();
				StartCoroutine (transform.MoveTo (prepPosition, duration1, easer1, () => {
						StartCoroutine (Auto.Wait (0.1f, () => {

								StartCoroutine (transform.MoveTo (prepPos2, duration1 / 2, entranceEaser, () => {
										StartCoroutine (transform.MoveTo (prepPosition, duration1 / 2, entranceEaser, null));
								}));
						}));
				}));
				//yield return StartCoroutine (MoveTo (delay1, delay2, prepPosition, targetPos));
		}
		

	
		void OnBecameVisible ()
		{
				//		AudioSource.PlayClipAtPoint (appearanceSnd, transform.position);
		}

		IEnumerator MoveTo (Transform trans, float delay1, float delay2, Vector3 pos1, Vector3 pos2)
		{
		
		
						
		
		
				StartCoroutine (Auto.Wait (delay1, () => {
						//AudioManager.instance.PlayAudio (travelSnd);
						if (trans != null) {

								Vector2 force = (trans.localPosition - pos2) / duration2;
								trans.GetComponent<Rigidbody2D> ().velocity = -force;
						}
			
				}));
				//StartCoroutine (transform.MoveTo (pos2, duration2, easer2, null));
		
		
		
				yield return null;
		}

	
		IEnumerator MoveTo (float delay1, float delay2, Vector3 pos1, Vector3 pos2)
		{
		
		
//				StartCoroutine (Auto.Wait (duration1 - (duration1 / 1.6f), () => {
//						
//				}));				
				
			
				StartCoroutine (Auto.Wait (delay1, () => {
						
						AudioManager.instance.PlayAudio (travelSnd);
						Vector2 force = (transform.localPosition - pos2) / duration2;
						GetComponent<Rigidbody2D> ().velocity = -force;
						spawnHolder.ReleaseSpawn ();
						EnemySpawnManager.instance.g4Spawns -= 1;
				
				}));
				//StartCoroutine (transform.MoveTo (pos2, duration2, easer2, null));
				
		
		
				yield return null;
		}
}
