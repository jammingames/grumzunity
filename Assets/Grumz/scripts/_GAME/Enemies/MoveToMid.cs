﻿using UnityEngine;
using System.Collections;
using k;

public class MoveToMid : MonoBehaviour
{


		public float angle = 45;
		public float duration = 1f;
		public EaseType easer;
		public Vector3 target;

		//public Transform transTarget;
		
		public AudioSource fireSnd;
		public SpriteRenderer trail;
		public SpriteRenderer glow;
		Material mat;

		[HideInInspector]
		public EnemySpawnHolder
				spawnHolder;

		void Start ()
		{

				if (GetComponent<DestroyOnTriggerEnter2D> ()) {
						GetComponent<DestroyOnTriggerEnter2D> ().explosionSnd = AudioManager.instance.g1Death;
				}

				StartCoroutine (trail.FadeSprite (1, 0, 0.05f, 0, null));
				mat = GetComponentInChildren<SpriteRenderer> ().material;
				mat.SetFloat ("_MaskPosition", 0);

//				if (angle < 0) {
//						transTarget = transform.parent.GetComponent<Enemy2Controller> ().upTar;
//				} else 
//						transTarget = transform.parent.GetComponent<Enemy2Controller> ().downTar;
//				Vector3 origScale = transform.localScale;
				if (transform.position.x > 0) {
						mat.SetFloat ("_Forwards", 1);
						StartCoroutine (Auto.Wait (duration, Move));
						//StartCoroutine (transform.RotateTo (transTarget.rotation, duration, easer, Move));
						//gameObject.layer = Layers.RIGHT;
						//transform.Rotate (Vector3.forward, -angle);
				} else {
						mat.SetFloat ("_Forwards", 0);
						angle *= -1;
						StartCoroutine (Auto.Wait (duration, Move));
						//StartCoroutine (transform.RotateTo (transTarget.rotation, duration, easer, Move));
						//transform.localScale = new Vector3 (-1 * origScale.x, -1 * origScale.y, origScale.z);
						//gameObject.layer = Layers.LEFT;
						//transform.Rotate (Vector3.forward, -angle);
				}
		}

		public void HitPlayer ()
		{
				enabled = false;
				//trail.Recycle ();
		}


		void OnDisable ()
		{
				StopAllCoroutines ();
		}

		void Move ()
		{
				StartCoroutine (Auto.Wait (1.4f, () => {
				
						if (spawnHolder != null)
								spawnHolder.ReleaseSpawn ();
						if (trail != null)
								StartCoroutine (trail.FadeSprite (0, 1, 0.2f, 0, null));
						target = transform.position.GetPosByAngle (-transform.position.x, angle);
						StartCoroutine (transform.MoveTo (target, 1.5f, easer, null));
						if (fireSnd != null)
								AudioManager.instance.PlayAudio (fireSnd);
						if (glow != null) {
				
								StartCoroutine (glow.FadeSprite (1, 0, 0.2f, 0, null));
						}
				}));
		}
}
