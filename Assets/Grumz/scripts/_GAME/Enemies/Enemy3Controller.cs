using UnityEngine;
using System.Collections;
using k;

public class Enemy3Controller : MonoBehaviour
{
	
//	
//	appearance √ A_hello, truncated
//		enemy into portal √ C_portal_in
//			portal moves √ C_portal_move
//			enemy exit √ C_portal_out
//			movement in/out √ C_move series	



		/*

spawn two sprites, 
*/
		public GameObject portalObject, portalObject2, lineObject;	
		public Transform spriteObject, spriteRealObject;
		public SpriteRenderer sprite, trailProxy, trailReal;
		//public Material mat;
		public EaseType easer1;
		public float duration1, delay1;
		public EaseType easer2;
		public float duration2, delay2;
		public EaseType easer3;
		public float duration3, delay3;
		public float lineDuration;
		public float xOffset = 0;
		public float yOffset = 0;
		public float colliderBounds = 0.5865395f;
		[HideInInspector]
		public float
				origX;
		

		//SpriteRenderer _renderer;

		
		float direction;
//		TrailRenderer trail;
		
		float lineSize;
		//	bool travelForwards;
		Vector3 origScale;
		Vector3 prepPosition, firstPos, secondPos, targetPos;
		int forwards = 0;

		public float distFromEdge;
		//Material mat;
		
		public AudioSource appearanceSnd, portalInSnd, portalMoveSnd, portalOutSnd, moveSnd;
		float leftEdge, rightEdge;
//		Collider2D col;
//		Vector3 origPos;


		[HideInInspector]
		public EnemySpawnHolder
				spawnHolder;
		

		void Awake ()
		{
				spawnHolder = GetComponent<EnemySpawnHolder> ();
				//sprite.enabled = false;
				//	mat = GetComponentInChildren<SpriteRenderer> ().material;
				//	origPos = transform.localPosition;
//				GetComponent<TrailRenderer> ().enabled = false;
//				StartCoroutine (mat.ChangeFill (0, 1, 0.01f, null));
				//	trail = GetComponentInChildren<TrailRenderer> ();
				

				//trail = GetComponent<TrailRenderer>();
				//trail.enabled = false
				origX = transform.localPosition.x;
				//_renderer = GetComponentInChildren<SpriteRenderer> ();
				leftEdge = G.i.LEFT_SCREEN_EDGE;
				rightEdge = G.i.RIGHT_SCREEN_EDGE;
				origScale = transform.localScale;

//				col = GetComponent<Collider2D> ();

				
//				G.i.KillAll += HandleKillAll;
				if (EnemySpawnManager.instance != null) {

						EnemySpawnManager.instance.g3Spawned = true;
				}

		}

		void HandleKillAll (bool isStart)
		{
				gameObject.Recycle ();
		}

	
	
		void OnEnable ()
		{

				if (GetComponent<DestroyOnTriggerEnter2D> ()) {
						GetComponent<DestroyOnTriggerEnter2D> ().explosionSnd = AudioManager.instance.g1Death;
				}
				float pos = transform.localPosition.x;
				if (G.i.g2Enemies.Count > 0) {
						pos = -G.i.g2Enemies [0].origX;
				}
				leftEdge = G.i.LEFT_SCREEN_EDGE;
				rightEdge = G.i.RIGHT_SCREEN_EDGE;
		
				//AudioManager.instance.PlayAudio (appearanceSnd);
				StartCoroutine (PositionAtScreenEdge (pos));
		
				

		
		
		}

		void OnDisable ()
		{
//				if (EnemySpawnManager.instance != null) {
//						EnemySpawnManager.instance.ReAddG3 ();
//				}
				//			transform.localPosition = origPos;
				
		}


		IEnumerator PositionAtScreenEdge (float pos)
		{		
				Vector3 portalPosition;
				Vector3 portalEndPosition;
				//float distFromCeil;
				if (transform.localPosition.y > 0) {
						distFromEdge = Vector2.Distance (transform.localPosition, new Vector2 (transform.localPosition.x, G.i.TOP_SCREEN_EDGE));
						direction = -1;
				} else {
						distFromEdge = Vector2.Distance (transform.localPosition, new Vector2 (transform.localPosition.x, G.i.BOT_SCREEN_EDGE));
						direction = 1;
				}
				float ySpritePos = 0;
				//direction = (distFromEdge < yOffset) ? -1 : 1;
				if (pos > 0) {
						//gameObject.layer = Layers.RIGHT;


						float firstX = rightEdge - xOffset;
						float firstY = transform.localPosition.y + Random.Range (yOffset / 2, yOffset) * direction;
						float origTransformYPos = transform.localPosition.y;
						transform.position = new Vector3 (rightEdge + colliderBounds, firstY, transform.localPosition.z);
						spriteObject.position = new Vector3 (rightEdge + colliderBounds, origTransformYPos, transform.localPosition.z);
						ySpritePos = spriteObject.position.y;
						prepPosition = new Vector3 ((rightEdge), transform.localPosition.y, transform.localPosition.z);
						firstPos = new Vector3 (firstX, spriteObject.position.y, transform.localPosition.z);
						secondPos = new Vector3 (firstX, firstY, transform.localPosition.z);
						portalPosition = firstPos;
						portalPosition.x = firstPos.x + (colliderBounds / 2);

						portalEndPosition = secondPos;
						portalEndPosition.x = secondPos.x + (colliderBounds / 2);
						firstPos.y = transform.localPosition.y;
						firstPos.x = firstPos.x - colliderBounds;
						secondPos.x = secondPos.x + (colliderBounds * 1.5f);
						targetPos = new Vector3 (leftEdge - colliderBounds, firstY, transform.localPosition.z);

						transform.localScale = new Vector3 (-1 * origScale.x, origScale.y, origScale.z);
			
						spriteObject.parent = MaskManager.i.rPortInMask.transform;
						MaskManager.i.rPortInMask.updateSprites (spriteObject);
						transform.parent = MaskManager.i.rPortOutMask.transform;
						MaskManager.i.rPortOutMask.updateSprites (transform);

//						mat.SetFloat ("_MaskPosition", portalPosition.x);	
//						sprite.material.SetFloat ("_MaskPosition", portalPosition.x);	
//						trailProxy.material.SetFloat ("_MaskPosition", portalPosition.x);
//						trailReal.material.SetFloat ("_MaskPosition", portalPosition.x);
//						forwards = 1;
//						sprite.material.SetFloat ("_Forwards", forwards);	
//						trailProxy.material.SetFloat ("_Forwards", forwards);	
//						trailReal.material.SetFloat ("_Forwards", 0);	
//						mat.SetFloat ("_Forwards", 0);	
						//travelForwards = false;



				} else {
						//travelForwards = true;
						//	gameObject.layer = Layers.LEFT;
						transform.localScale = new Vector3 (1, origScale.y, origScale.z);
						float firstX = leftEdge + xOffset;
						float firstY = transform.localPosition.y + Random.Range (yOffset / 2, yOffset) * direction;
						float origTransformYPos = transform.localPosition.y;
						transform.position = new Vector3 (leftEdge - colliderBounds, firstY, transform.localPosition.z);
						spriteObject.position = new Vector3 (leftEdge - colliderBounds, origTransformYPos, transform.localPosition.z);
						ySpritePos = spriteObject.position.y;
						prepPosition = new Vector3 ((leftEdge), transform.localPosition.y, transform.localPosition.z);
						firstPos = new Vector3 (firstX, spriteObject.position.y, transform.localPosition.z);
						secondPos = new Vector3 (firstX, firstY, transform.localPosition.z);
						portalPosition = firstPos;
						portalPosition.x = firstPos.x - (colliderBounds / 2);
						portalEndPosition = secondPos;
						portalEndPosition.x = secondPos.x - (colliderBounds / 2);
						firstPos.y = transform.localPosition.y;
						firstPos.x = firstPos.x + colliderBounds;
						secondPos.x = secondPos.x - (colliderBounds * 1.5f);
						targetPos = new Vector3 (rightEdge + colliderBounds, firstY, transform.localPosition.z);

//
						spriteObject.parent = MaskManager.i.lPortInMask.transform;
						MaskManager.i.lPortInMask.updateSprites (spriteObject);
						transform.parent = MaskManager.i.lPortOutMask.transform;
						MaskManager.i.lPortOutMask.updateSprites (transform);
			
			
//						mat.SetFloat ("_MaskPosition", portalPosition.x);
//						sprite.material.SetFloat ("_MaskPosition", portalPosition.x);
						
//						trailProxy.material.SetFloat ("_MaskPosition", portalPosition.x);
//						trailReal.material.SetFloat ("_MaskPosition", portalPosition.x);
						//		forwards = 0;
//						sprite.material.SetFloat ("_Forwards", forwards);	
//						mat.SetFloat ("_Forwards", 1);
//						trailProxy.material.SetFloat ("_Forwards", forwards);	
//						trailReal.material.SetFloat ("_Forwards", 1);	

				}
//				print (portalPosition + " is portal 1 pos");
//				print (portalEndPosition + " is portal 2 pos");
				if (MaskManager.i.portPos == 0) {
						MaskManager.i.portPos = Mathf.Abs (portalPosition.x);
						MaskManager.i.SetPortalMaskPositions ();
				}
				
				
				yield return new WaitForEndOfFrame ();
			
				//	float yPos = Random.Range (G.i.BOT_SCREEN_EDGE, G.i.TOP_SCREEN_EDGE);
				//	secondPos = new Vector3 (targetPos.x, Mathf.Clamp (yPos, G.i.BOT_SCREEN_EDGE, G.i.TOP_SCREEN_EDGE), 0);
//				//debug.log (prepPosition.x);
//				//debug.log (firstPos.x);
				StartCoroutine (MoveToAndTeleport (delay1, delay2, delay3, prepPosition, firstPos, secondPos, targetPos, portalPosition, portalEndPosition));
				prepPosition.y = ySpritePos;
				firstPos.y = ySpritePos;
				secondPos.y = ySpritePos;
				targetPos.y = ySpritePos;
				StartCoroutine (MoveToPositions (spriteObject, prepPosition, firstPos, targetPos, delay1, delay2));
		}


		IEnumerator MoveToPositions (Transform tr, Vector3 pos1, Vector3 pos2, Vector3 pos3, float delay1, float delay2)
		{
				yield return StartCoroutine (Auto.Wait (delay1));
//				print ("pos1" + pos1);
				yield return StartCoroutine (tr.MoveTo (pos1, duration1, null));
				yield return StartCoroutine (Auto.Wait (delay2));
//				print ("pos2" + pos2);
				yield return StartCoroutine (tr.MoveTo (pos2, duration2, null));
//				print ("pos3" + pos3);
				yield return StartCoroutine (tr.MoveTo (pos3, duration3, null));
		}

		/*
		 * delay 1
		 * move pos1 by duration1
		 * delay 2, 
		 * move pos2 by duration 2
		 * move pos4 by duration3
		 * */
		IEnumerator MoveToAndTeleport (float delay1, float delay2, float delay3, Vector3 pos1, Vector3 pos2, Vector3 pos3, Vector3 pos4, Vector3 portalPos, Vector3 portalEndPos)
		{
				
//				AudioManager.instance.PlayAudio (moveSnd);
				yield return StartCoroutine (Auto.Wait (delay1));
//				StartCoroutine (mat.ChangeFill (1, 0, duration2 / 2, null));
				StartCoroutine (Auto.Wait (0.3f, () => {
						AudioManager.instance.PlayAudio (portalInSnd);
						GameObject obj = GameObject.Instantiate (portalObject, portalPos, transform.rotation) as GameObject;
//						obj.GetComponent<AddToMaskOnStart> ().SetMask (spriteObject);
						Vector3 scale = new Vector3 (-1, 1, 1);
						obj.transform.localScale = scale;
			
						

				}));
				StartCoroutine (transform.MoveTo (pos1, duration1, easer1, () => {
						spawnHolder.ReleaseSpawn ();
						//	StartCoroutine (Auto.Wait (duration1 / 1.5f, () => {
						//	}));
						
						//GameObject obj2 = 
						GameObject obj2 = GameObject.Instantiate (portalObject2, portalEndPos, transform.rotation) as GameObject;
//						obj2.GetComponent<AddToMaskOnStart> ().SetMask (spriteObject);
						//trail.enabled = false;
						//obj.transform.parent = transform;
						StartCoroutine (Auto.Wait (delay2, () => {
								StartCoroutine (Auto.Wait (0.325f, () => {

										AudioManager.instance.PlayAudio (portalOutSnd);	
								}));
								StartCoroutine (transform.MoveTo (pos2, duration2, easer2, () => {
										
//								if (forwards == 1) {
//										//debug.log ("flipped to 0");
//										forwards = 0;
//										sprite.material.SetFloat ("_Forwards", forwards);
//								} else if (forwards == 0) {
//										forwards = 1;
//										//debug.log ("flipped to 1");
//										sprite.material.SetFloat ("_Forwards", forwards);	
//								}
										
										//_renderer.enabled = false;
//										StartCoroutine (mat.ChangeFill (0, 1, 0.5f, null));
										//GetComponent<TrailRenderer> ().enabled = false;
//										StartCoroutine (Auto.Wait ((duration3 / 6), () => {
//												mat.SetFloat ("_Forwards", forwards);						
//												mat.SetFloat ("_MaskPosition", 0);	
//												trailReal.material.SetFloat ("_Forwards", forwards);						
//												trailReal.material.SetFloat ("_MaskPosition", 0);	
//										}));
										StartCoroutine (transform.MoveTo (pos4, duration3, easer2, () => {
												
										}));
//								transform.position = pos3;
//								StartCoroutine (transform.MoveTo (pos3, lineDuration, easer2, () => {
//										//debug.log ("done moving to pos 3");
//										//		GetComponent<TrailRenderer> ().enabled = false;
//										
//										trail.enabled = true;
//										AudioManager.instance.PlayAudio (portalOutSnd);
//										AudioManager.instance.PlayAudio (moveSnd);
//										//transform.position = pos3;
//										//_renderer.enabled = true;
//										
//								}));
					

								}));
						

						}));
				}));
						


//								FireLasers ();}));	
			
						
		
		
				yield return null;
		}


	
		IEnumerator CreateLine (Vector3 atPos, Vector3 target, float duration, System.Action OnComplete)
		{
				AudioManager.instance.PlayAudio (portalMoveSnd);
				float goHeight = lineObject.GetComponent<Renderer> ().bounds.size.y * transform.localScale.y;
				int numObjects = (int)(Vector2.Distance (target, atPos) / goHeight);
				float t = duration / numObjects;
				for (int i = 1; i <=numObjects; i++) {
						//		GameObject obj = 
						GameObject.Instantiate (lineObject, new Vector3 (atPos.x, (atPos.y + goHeight * i * direction), atPos.z), Quaternion.identity);
						//obj.transform.parent = transform;
						yield return new WaitForSeconds (t);
				}
				if (OnComplete != null)
						OnComplete ();
				yield return null;
		}

}
