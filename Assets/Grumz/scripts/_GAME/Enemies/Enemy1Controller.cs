﻿using UnityEngine;
using System.Collections;
using k;

public class Enemy1Controller : MonoBehaviour
{
	



		public EaseType easer1;
		public float duration1, delay1;
		public EaseType easer2;
		public float duration2, delay2;

		public Vector2 maxSpeed;

		//Material mat;


//		bool travelForwards;
		Vector3 origScale;
		Vector3 prepPosition;
		Vector3 targetPos;

		float leftEdge, rightEdge;
		
		Collider2D col;
		public AudioSource travelSnd;
		
//		Vector3 origPos;
		

		[HideInInspector]
		public EnemySpawnHolder
				spawnHolder;



		void Awake ()
		{
				spawnHolder = GetComponent<EnemySpawnHolder> ();
				//		origPos = transform.localPosition;
				leftEdge = G.i.LEFT_SCREEN_EDGE;
				rightEdge = G.i.RIGHT_SCREEN_EDGE;
				origScale = transform.localScale;
				col = GetComponent<Collider2D> ();
				//	mat = GetComponentInChildren<Renderer> ().material;
//				G.i.OnPlayerDeath += HandleOnPlayerDeath;
//				G.i.OnStateChange += HandleOnStateChange;
				if (GetComponent<DestroyOnTriggerEnter2D> ()) {
						GetComponent<DestroyOnTriggerEnter2D> ().explosionSnd = AudioManager.instance.g1Death;
				}
		}

		void HandleOnStateChange (GameState nextState)
		{
//				if (nextState == GameState.End) {
//						if (gameObject != null)
//								Destroy (gameObject);
//				}
		}

		void HandleOnPlayerDeath ()
		{
				//gameObject.SetActive (false);
		}

		void HandleKillAll (bool isStart)
		{
		}



	
		void OnEnable ()
		{
				
				StartCoroutine (PositionAtScreenEdge ());

		
		}

		void OnDisable ()
		{

				
		}
		
		public void HitPlayer ()
		{
				enabled = false;
		}


		void Update ()
		{
				maxSpeed = GetComponent<Rigidbody2D> ().velocity;
		}






		IEnumerator PositionAtScreenEdge ()
		{	
				
				//		mat.SetFloat ("_MaskPosition", 0);
				if (transform.localPosition.x > 0) {
						//				mat.SetFloat ("_Forwards", 1);
						//gameObject.layer = Layers.RIGHT;
						//	travelForwards = false;
						transform.position = new Vector3 (rightEdge + 1 + (col.bounds.size.x * 2), transform.localPosition.y, transform.localPosition.z);
						prepPosition = new Vector3 ((rightEdge - 0.5f), transform.localPosition.y, transform.localPosition.z);
						targetPos = new Vector3 (leftEdge - col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
						transform.localScale = new Vector3 (-1 * origScale.x, origScale.y, origScale.z);
				} else {
						//	travelForwards = true;
						//		mat.SetFloat ("_Forwards", 0);
						//gameObject.layer = Layers.LEFT;
						//	transform.localScale = new Vector3 (origScale.x, origScale.y, origScale.z);
						transform.position = new Vector3 (leftEdge - 1 - (col.bounds.size.x * 2), transform.localPosition.y, transform.localPosition.z);
//						//debug.log ("My local pos is " + transform.localPosition);
						prepPosition = new Vector3 ((leftEdge + 0.5f), transform.localPosition.y, transform.localPosition.z);
						targetPos = new Vector3 (rightEdge + col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
				}
				yield return new WaitForEndOfFrame ();
				
				yield return StartCoroutine (MoveTo (delay1, delay2, prepPosition, targetPos));
		}

		void OnBecameVisible ()
		{
				//		AudioSource.PlayClipAtPoint (appearanceSnd, transform.position);
		}
	
		IEnumerator MoveTo (float delay1, float delay2, Vector3 pos1, Vector3 pos2)
		{
			

//				StartCoroutine (Auto.Wait (duration1 - (duration1 / 1.6f), () => {
//						AudioManager.instance.PlayAudio (appearanceSnd);
//				}));				
				StartCoroutine (transform.MoveTo (pos1, duration1, easer1, () => {

						StartCoroutine (Auto.Wait (delay1, () => {
								if (AudioManager.instance != null) {

										AudioManager.instance.PlayAudio (travelSnd);
								}
								//GetComponent<EnemySpawnHolder>();
								Vector2 force = (transform.localPosition - pos2) / duration2;
								GetComponent<Rigidbody2D> ().velocity = -force;

								spawnHolder.ReleaseSpawn ();
						}));
						//StartCoroutine (transform.MoveTo (pos2, duration2, easer2, null));
				}));
	
				yield return null;
		}
}
