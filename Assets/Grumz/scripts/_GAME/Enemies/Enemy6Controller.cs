﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using k;

public class Enemy6Controller : MonoBehaviour
{
	

		public List <ControlPoint> points;
		public float totalTime;
		public float delay = 1;
		public EaseType easer;
		List<float> distances;
		List<float> times;
		float totalDistance = 0;
//		Collider2D col;
		//Vector3 origPos; 
		float leftEdge;
		void Awake ()
		{

				leftEdge = G.i.LEFT_SCREEN_EDGE;
				distances = new List<float> ();
				//origPos = transform.localPosition;
				//	col = GetComponent<Collider2D> ();
		}


		void CalculateTime ()
		{
				
				for (int i = 0; i < points.Count; i++) {
						if ((i + 1) < (points.Count - 1)) {
								distances.Add (Vector3.Distance (points [i].target.position, points [i + 1].target.position));
								totalDistance += Vector3.Distance (points [i].target.position, points [i + 1].target.position);
						}
				}

		}


		void AddTime ()
		{

				for (int i = 0; i <points.Count; i++) {
						////debug.log (i);
						points [i].duration = (distances [i] / totalDistance) * totalTime;
				}
		}
		
	
		void OnEnable ()
		{	
				totalDistance = 0; 
				//stuff
				transform.position = points [0].target.position;
//				CalculateTime ();
//				AddTime ();
				StartCoroutine (MoveOnPath ());
		}

		void OnDisable ()
		{
				G.i.TriggerBoss (false);
		}

		IEnumerator MoveOnPath ()
		{
				transform.position = new Vector3 (leftEdge - 2, points [0].target.position.y, points [0].target.position.z);
				Vector3 prepPosition = new Vector3 ((leftEdge + 0.5f), points [0].target.position.y, points [0].target.position.z);
				yield return StartCoroutine (transform.MoveTo (prepPosition, 1f, easer, null));
				yield return new WaitForSeconds (delay);
				for (int i = 0; i <points.Count; i ++) {

						yield return StartCoroutine (transform.MoveTo (points [i].target.position, points [i].duration, points [i].easers, null));
				}
				////debug.log ("done boss mode");
				G.i.TriggerBoss (false);
//				gameObject.Recycle ();
				Destroy (gameObject);
	
		}

}
