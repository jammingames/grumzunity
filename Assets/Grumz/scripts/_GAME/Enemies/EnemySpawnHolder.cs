﻿using UnityEngine;
using System.Collections;
using k;

public class EnemySpawnHolder : MonoBehaviour
{


	

		public SpawnPoint
				spawnPos;


		
		public void HoldSpawn (SpawnPoint newSpawnPos)
		{
				
				spawnPos = newSpawnPos;
				//spawnPos.canSpawn = false;
				if (EnemySpawnManager.instance != null)
						EnemySpawnManager.instance.TriggerSpawnEnemy (this.spawnPos, true);
		}

		public void ReleaseSpawn ()
		{
				if (EnemySpawnManager.instance != null)
						EnemySpawnManager.instance.TriggerSpawnEnemy (spawnPos, false);
		}

		void OnTriggerEnter2D (Collider2D col)
		{
				if (col.tag == Tags.BOMB) {
						ReleaseSpawn ();
				}
		}
		
}
