﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour
{

		public bool canSpawn = true;
		public float timeTillCanSpawn = 8.0f;
		bool isStarting = true;
		float t = 0;


		void Awake ()
		{
				EnemySpawnManager.instance.OnSpawnEnemy += HandleOnSpawnEnemy;
		}

		void HandleOnSpawnEnemy (SpawnPoint spawnPoint, bool isHeld)
		{
				if (spawnPoint == this) {
						if (isHeld) {
								UseSpawn ();

						} else if (!isHeld) {
								ResetSpawnPoint ();
						}
					
				}
		}

		// Use this for initialization
		void Start ()
		{
				EnemySpawnManager.instance.spawnPoints.Add (this);
				G.i.OnPlayerDeath += HandleOnPlayerDeath;
				//timeTillCanSpawn = 5.0f;
		}

		void HandleOnPlayerDeath ()
		{
				ResetSpawnPoint ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (canSpawn == false) {
						if (isStarting) {
							
								isStarting = false;
						}
						if (t < timeTillCanSpawn) {

								t += Time.deltaTime;
						} else
								ResetSpawnPoint ();
				}
				
		}

		public void UseSpawn ()
		{
//				//debug.log (gameObject.name + " spawn Point being helllllld");
				EnemySpawnManager.instance.spawnPoints.Remove (this);
				canSpawn = false;
		}

		public void ResetSpawnPoint ()
		{
//				//debug.log (gameObject.name + " spawn point being reset");
				EnemySpawnManager.instance.spawnPoints.Add (this);
				isStarting = true;
				canSpawn = true;
				t = 0;
		}

}
