﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ControlPoint
{
	
		public Transform target;
		public EaseType easers;
		public float duration;
		


}
