﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveRectOnStateChange : MonoBehaviour
{
		public GameState state;
	
		public bool moveFrom;
		
		public RectTransform target;
		RectTransform trans;

		public Vector2 targetPos;
		public float duration;
		public float delay;
		public EaseType easer;
	
		private Vector2 origPos;
		public bool onlyOnce = true;
		public bool goBack = false;
		bool firstTime = true;
		void Awake ()
		{
				trans = GetComponent<RectTransform> ();
				origPos = trans.anchoredPosition;

				targetPos.y = origPos.y;

				if (target != null)
						targetPos = target.anchoredPosition;
				
				if (moveFrom)
						trans.anchoredPosition = targetPos;
		}
	
		void OnEnable ()
		{
				G.i.OnStateChange += HandleOnStateChange;
		}
		void OnDisable ()
		{
				if (G.i != null)
						G.i.OnStateChange -= HandleOnStateChange;
		}
	
		void HandleOnStateChange (GameState nextState)
		{
				targetPos.y = origPos.y;
				if (state == nextState && gameObject.activeSelf) {
						if (onlyOnce && firstTime == true) {
				
								firstTime = false;
						} else if (onlyOnce && !firstTime)
								return;
			
						if (moveFrom) {
								trans.anchoredPosition = targetPos;
								StartCoroutine (MoveTo (delay, origPos));
						} else {
								trans.anchoredPosition = origPos;
								StartCoroutine (MoveTo (delay, targetPos));
						}
				} else if (state != nextState && gameObject.activeSelf && goBack) {
						trans.anchoredPosition = origPos;
				}
		
		
		}
	
	
	
		public void MoveAtStart ()
		{
				targetPos.y = origPos.y;
				trans.anchoredPosition = origPos;
				StartCoroutine (MoveTo (delay, targetPos));
		
		}
	
		public void MoveBack (System.Action OnComplete)
		{
				targetPos.y = origPos.y;
				if (moveFrom) {
			
						StartCoroutine (MoveTo (delay, targetPos, OnComplete));
				} else {
			
						StartCoroutine (MoveTo (delay, origPos, OnComplete));
				}
		}
//	
//		IEnumerator MoveFrom (float delay)
//		{
//				yield return new WaitForSeconds (delay);
//				StartCoroutine (trans.MoveFrom (targetPos, duration, easer, null));
//		}
	
		IEnumerator MoveTo (float delay, Vector2 pos, System.Action OnComplete)
		{
				yield return new WaitForSeconds (delay);
				if (target != null)
						StartCoroutine (trans.MoveRectToTransform (target, duration, easer, null));
				else
						StartCoroutine (trans.MoveRectTo (pos, duration, easer, OnComplete));
		}
	
		IEnumerator MoveTo (float delay, Vector2 pos)
		{
				yield return new WaitForSeconds (delay);
				if (target != null)
						StartCoroutine (trans.MoveRectToTransform (target, duration, easer, null));
				else
						StartCoroutine (trans.MoveRectTo (pos, duration, easer, null));
		}
	
	
	
}
