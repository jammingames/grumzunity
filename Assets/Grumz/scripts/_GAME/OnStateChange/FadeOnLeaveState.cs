﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeOnLeaveState : MonoBehaviour
{
	
		public float duration = 1f;
		public float delay = 0f;
		//public float start;
		public float target;
		SpriteRenderer spr;
		Graphic img;

		public bool randomize;
	
		public bool shouldDestroy = true;
		public bool startAtZero;
		public GameState state;
		public bool isImage = false;
		GameState lastState;


		void Awake ()
		{
				if (GetComponent<Graphic> ()) {
						img = GetComponent<Graphic> ();
				}
				if (GetComponent<SpriteRenderer> ()) {
						spr = GetComponent<SpriteRenderer> ();
				}
		}
	
		void OnEnable ()
		{
				G.i.OnStateChange += HandleOnStateChange;
		}
	
		void OnDisable ()
		{
				if (!isImage) {
						Color col = spr.color;
						col.a = 1;
						spr.color = col;
				} else if (isImage) {
						Color col = img.color;
						col.a = 1;
						img.color = col;
				}
				if (G.i != null)
						G.i.OnStateChange -= HandleOnStateChange;
		}
//
//		public void TurnOff()
//	{
//
//		StartCoroutine (Auto.Wait (delay, () => {
//			
//			
//			
//			if (GetComponent<SpriteRenderer> ()) {
//				spr = GetComponent<SpriteRenderer> ();
//				//float start = spr.color.a;
//				
//
//				StartCoroutine (spr.FadeSprite (spr.color.a, target, duration, delay, () => {
//					if (shouldDestroy)
//						GameObject.Destroy (gameObject);
//					//gameObject.Recycle ();
//				}));
//			}
//		}));
//
//	}
	
		void HandleOnStateChange (GameState nextState)
		{
				Color col = Color.white;
				if (state == lastState) {
						if (isImage) {
								col = img.color;
						} else if (!isImage) {

								col = spr.color;
						}
						if (startAtZero) {
				
								col.a = 0;
								if (isImage) {
										img.color = col;
										//StartCoroutine (img.LerpImageColor (img.color, 0.01f, 0.001f, null));
								} else if (!isImage) {

										spr.color = col;
										//					StartCoroutine (spr.FadeSprite (0, 0.01f, 0.001f, null));
								}
						}
						StartCoroutine (Auto.Wait (delay, () => {
				
				
				
//								if (GetComponent<SpriteRenderer> ()) {
//										spr = GetComponent<SpriteRenderer> ();
								//float start = spr.color.a;
					
								if (randomize)
										duration *= (Random.value + 0.2f);
								if (isImage) {
										StartCoroutine (img.FadeImageAlpha (img.color.a, target, duration, () => {
												if (shouldDestroy)
														gameObject.Recycle ();
//														GameObject.Destroy (gameObject);
										}));
								} else if (!isImage) {
										StartCoroutine (spr.FadeSprite (spr.color.a, target, duration, delay, () => {
												if (shouldDestroy)
														gameObject.Recycle ();
//														GameObject.Destroy (gameObject);
										}));

								}


						}));
				} 
		
		
		
				lastState = nextState;
		}
	
	
	
}
