﻿using UnityEngine;
using System.Collections;

public class ScaleOnStateChange : MonoBehaviour
{

		Vector3 origScale;
		public float duration;
		public Vector3 targetScale;
		public Vector3 offScale;
		public EaseType easer;
		


		public GameState onState;
		public GameState offState;
		//public bool MoveBeforeDisable = false;


		void OnEnable ()
		{
				transform.localScale = origScale;
		}

		// Use this for initialization
		void Awake ()
		{
//				targetScale.y = transform.localScale.y;
//				targetScale.z = transform.localScale.z;
				origScale = transform.localScale;
				G.i.OnStateChange += HandleOnStateChange;
				//enabled = false;
		}

	
		void HandleOnStateChange (GameState state)
		{
				if (state == onState) {
//						//debug.log ("scale on state working");
//						enabled = true;
//						targetScale.y = transform.localScale.y;
//						targetScale.z = transform.localScale.z;
//						origScale = transform.localScale;
//						StartCoroutine (transform.ScaleTo (targetScale, duration, easer, null));
						StartCoroutine (transform.ScaleTo (targetScale, duration, easer, null));
				} else if (state == offState)
						StartCoroutine (transform.ScaleTo (offScale, duration, easer, null));
				
		}

		public void Scale ()
		{
				targetScale.y = transform.localScale.y;
				targetScale.z = transform.localScale.z;
				origScale = transform.localScale;
				StartCoroutine (transform.ScaleTo (targetScale, duration, easer, () => {
						//enabled = false;
				}));
		}

		public void Scale (bool orig)
		{
				StartCoroutine (transform.ScaleTo (origScale, duration, easer, null));
		}

		void Disable ()
		{
				gameObject.SetActive (false);
		}
}
