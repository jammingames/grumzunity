﻿using UnityEngine;
using System.Collections;

public class MoveRectOnLeaveState : MonoBehaviour
{
		public GameState state;

		GameState lastState;
		public bool moveFrom;
		public bool travelForwards;
		public RectTransform target;
		public Vector2 targetPos;
		public float duration;
		public float delay;
		public EaseType easer;

		RectTransform trans;
	
		private Vector2 origPos;
		
		void Awake ()
		{
				trans = GetComponent<RectTransform> ();
				G.i.OnStateChange += HandleOnStateChange;
				origPos = trans.anchoredPosition;
				targetPos.y = origPos.y;
				
				if (target != null)
						targetPos = target.anchoredPosition;


				if (moveFrom)
						trans.anchoredPosition = targetPos;
		}
	
		void HandleOnStateChange (GameState nextState)
		{
				targetPos.y = origPos.y;
				if (nextState == state) {
						if (moveFrom) {
								trans.anchoredPosition = targetPos;
								//StartCoroutine (MoveTo (0, targetPos));
						} else {
								trans.anchoredPosition = origPos;
								//StartCoroutine (MoveTo (0, origPos));
						}
				}
				if (state == lastState && gameObject.activeSelf) {
			
					
						
						if (moveFrom) {

								trans.anchoredPosition = targetPos;
								StartCoroutine (MoveTo (delay, origPos));
						} else {
								trans.anchoredPosition = origPos;
								StartCoroutine (MoveTo (delay, targetPos));
						}

				}
				lastState = nextState;

		}
	
	
	
		public void MoveAtStart ()
		{
				targetPos.y = origPos.y;
				trans.anchoredPosition = origPos;
				StartCoroutine (MoveTo (delay, targetPos));
		
		}
	
		public void MoveBack (System.Action OnComplete)
		{
				targetPos.y = origPos.y;
				if (moveFrom) {
			
						StartCoroutine (MoveTo (delay, targetPos, OnComplete));
				} else {
			
						StartCoroutine (MoveTo (delay, origPos, OnComplete));
				}
		}
	
//		IEnumerator MoveFrom (float delay)
//		{
//				yield return new WaitForSeconds (delay);
//				StartCoroutine (trans.MoveFrom (targetPos, duration, easer, null));
//		}
	
		IEnumerator MoveTo (float delay, Vector2 pos, System.Action OnComplete)
		{
				yield return new WaitForSeconds (delay);
				StartCoroutine (trans.MoveRectTo (pos, duration, easer, OnComplete));
		}
	
		IEnumerator MoveTo (float delay, Vector2 pos)
		{
				yield return new WaitForSeconds (delay);
				StartCoroutine (trans.MoveRectTo (pos, duration, easer, null));
		}
	
	
	
}
