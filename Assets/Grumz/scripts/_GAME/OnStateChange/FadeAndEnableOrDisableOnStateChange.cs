﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeAndEnableOrDisableOnStateChange : MonoBehaviour
{

		public GameState onState, offState, immediateOffState;
		public GameObject gObj;
		public float duration;
		public bool isImage;
		
		SpriteRenderer spr;
		Image img;

		// Use this for initialization
		void Awake ()
		{
				if (isImage) {
						img = gObj.GetComponent<Image> ();
				} else {
						spr = gObj.GetComponent<SpriteRenderer> ();
				}
				
				G.i.OnStateChange += HandleOnStateChange;
		}

		

		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == onState) {


						gObj.SetActive (true);
						if (img != null) {
								StartCoroutine (img.FadeImageAlpha (img.color.a, 1, duration, null));
						}
								
						if (spr != null) {
								StartCoroutine (spr.FadeSprite (0, 1, duration, 0, null));
						}
						
						
				} else if (nextState == offState) {
						if (img != null) {
								StartCoroutine (img.FadeImageAlpha (img.color.a, 0, duration, () => {
										gObj.SetActive (false);
								}));
						} else if (spr != null)
								StartCoroutine (spr.FadeSprite (spr.color.a, 0, duration, 0, () => {
										gObj.SetActive (false);
								}));
				} else if (nextState == immediateOffState) {
						gObj.SetActive (false);
				}
		}

}
