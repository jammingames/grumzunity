﻿using UnityEngine;
using System.Collections;

public class MoveOnLeaveState : MonoBehaviour
{
		public GameState state;

		GameState lastState;
		public bool moveFrom;
		public bool travelForwards;
		public Transform target;
		public Vector3 targetPos;
		public float duration;
		public float delay;
		public EaseType easer;
	
		private Vector3 origPos;
		
		void Awake ()
		{
				G.i.OnStateChange += HandleOnStateChange;
				origPos = transform.localPosition;
				if (target != null)
						targetPos = target.position;
				if (travelForwards)
						targetPos = transform.localPosition + transform.forward * 7;
				if (moveFrom)
						transform.localPosition = targetPos;
		}
	
		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == state) {
						if (moveFrom) {
								transform.localPosition = targetPos;
								//StartCoroutine (MoveTo (0, targetPos));
						} else {
								transform.localPosition = origPos;
								//StartCoroutine (MoveTo (0, origPos));
						}
				}
				if (state == lastState && gameObject.activeSelf) {
			
					
						
						if (moveFrom) {
								transform.localPosition = targetPos;
								StartCoroutine (MoveTo (delay, origPos));
						} else {
								transform.localPosition = origPos;
								StartCoroutine (MoveTo (delay, targetPos));
						}

				}
				lastState = nextState;

		}
	
	
	
		public void MoveAtStart ()
		{
		
				transform.localPosition = origPos;
				StartCoroutine (MoveTo (delay, targetPos));
		
		}
	
		public void MoveBack (System.Action OnComplete)
		{
				if (moveFrom) {
			
						StartCoroutine (MoveTo (delay, targetPos, OnComplete));
				} else {
			
						StartCoroutine (MoveTo (delay, origPos, OnComplete));
				}
		}
	
		IEnumerator MoveFrom (float delay)
		{
				yield return new WaitForSeconds (delay);
				StartCoroutine (transform.MoveFrom (targetPos, duration, easer, null));
		}
	
		IEnumerator MoveTo (float delay, Vector3 pos, System.Action OnComplete)
		{
				yield return new WaitForSeconds (delay);
				StartCoroutine (transform.MoveTo (pos, duration, easer, OnComplete));
		}
	
		IEnumerator MoveTo (float delay, Vector3 pos)
		{
				yield return new WaitForSeconds (delay);
				StartCoroutine (transform.MoveTo (pos, duration, easer, null));
		}
	
	
	
}
