﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveOnStateChange : MonoBehaviour
{
		public GameState state;
		
		public bool moveFrom;
		public bool travelForwards;
		public Transform target;
		public Vector3 targetPos;
		public float duration;
		public float delay;
		public EaseType easer;
		
		private Vector3 origPos;
		public bool onlyOnce = true;
		public bool goBack = false;
		bool firstTime = true;
		void Awake ()
		{
				
				origPos = transform.localPosition;
				if (target != null)
						targetPos = target.position;
				if (travelForwards)
						targetPos = transform.localPosition + transform.forward * 7;
				if (moveFrom)
						transform.localPosition = targetPos;
		}

		void OnEnable ()
		{
				G.i.OnStateChange += HandleOnStateChange;
		}
		void OnDisable ()
		{
				if (G.i != null)
						G.i.OnStateChange -= HandleOnStateChange;
		}

		void HandleOnStateChange (GameState nextState)
		{
				
				if (state == nextState && gameObject.activeSelf) {
						if (onlyOnce && firstTime == true) {

								firstTime = false;
						} else if (onlyOnce && !firstTime)
								return;
				
						if (moveFrom) {
								transform.localPosition = targetPos;
								StartCoroutine (MoveTo (delay, origPos));
						} else {
								//transform.localPosition = origPos;
								StartCoroutine (MoveTo (delay, targetPos));
						}
				} else if (state != nextState && gameObject.activeSelf && goBack) {
						transform.localPosition = origPos;
				}
				
				
		}
	
		
	
		public void MoveAtStart ()
		{
		
				transform.localPosition = origPos;
				StartCoroutine (MoveTo (delay, targetPos));
		
		}
	
		public void MoveBack (System.Action OnComplete)
		{
				if (moveFrom) {
			
						StartCoroutine (MoveTo (delay, targetPos, OnComplete));
				} else {
			
						StartCoroutine (MoveTo (delay, origPos, OnComplete));
				}
		}
	
		IEnumerator MoveFrom (float delay)
		{
				yield return new WaitForSeconds (delay);
				StartCoroutine (transform.MoveFrom (targetPos, duration, easer, null));
		}
	
		IEnumerator MoveTo (float delay, Vector3 pos, System.Action OnComplete)
		{
				yield return new WaitForSeconds (delay);
				if (target != null)
						StartCoroutine (transform.MoveToTransform (target, duration, easer, null));
				else
						StartCoroutine (transform.MoveTo (pos, duration, easer, OnComplete));
		}
	
		IEnumerator MoveTo (float delay, Vector3 pos)
		{
				yield return new WaitForSeconds (delay);
				if (target != null)
						StartCoroutine (transform.MoveToTransform (target, duration, easer, null));
				else
						StartCoroutine (transform.MoveTo (pos, duration, easer, null));
		}
	
	
	
}
