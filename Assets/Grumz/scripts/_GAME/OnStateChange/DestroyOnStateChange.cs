﻿using UnityEngine;
using System.Collections;

public class DestroyOnStateChange : MonoBehaviour
{
	

		public GameState desiredState;

		
	
		void Start ()
		{
				G.i.OnStateChange += HandleOnStateChange;
		}
	
		void HandleOnStateChange (GameState state)
		{
				if (state == desiredState)
						Destroy (gameObject);
				
		}
	

	
}
