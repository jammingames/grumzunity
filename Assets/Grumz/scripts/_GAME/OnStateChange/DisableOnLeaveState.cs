﻿using UnityEngine;
using System.Collections;

public class DisableOnLeaveState : MonoBehaviour
{
	
	
		public GameState desiredState;
		GameState lastState;
	
	
		void Awake ()
		{
				G.i.OnStateChange += HandleOnStateChange;
		}
	
		void HandleOnStateChange (GameState nextState)
		{
				if (nextState == desiredState) {
						Debug.Log ("entering desired state");
				} else if (desiredState == lastState && gameObject.activeInHierarchy) {
						gameObject.SetActive (false);
				}
				lastState = nextState;
	
	
	
		}
}
