﻿using UnityEngine;
using System.Collections;

public class EnableOrDisableOnState : MonoBehaviour
{

		//[BitMask(typeof(GameState))]
		public GameState
				desiredState;
		public bool MoveBeforeDisable = false;
		public GameState extraState, thirdState;

		MoveOnStart mover;

		void Start ()
		{
				if (G.i.gameState == desiredState) {

						if (gameObject.activeSelf == false)
								gameObject.SetActive (true);
				} else if (G.i.gameState != desiredState && G.i.gameState != extraState)
						gameObject.SetActive (false);
				if (GetComponent<MoveOnStart> () != null && MoveBeforeDisable)
						mover = GetComponent<MoveOnStart> ();
				G.i.OnStateChange += HandleOnStateChange;
		}

		void HandleOnStateChange (GameState state)
		{
				if (state == desiredState || state == extraState || state == thirdState)
						gameObject.SetActive (true);
				else if (MoveBeforeDisable && gameObject.activeSelf == true) {
						mover.MoveBack (Disable);
				} else if (state != desiredState && state != extraState) {
						gameObject.SetActive (false);
				} else if (state != desiredState && state != extraState && state != thirdState)
						gameObject.SetActive (false);
		}
		

		void Disable ()
		{
				gameObject.SetActive (false);
		}
		
}
