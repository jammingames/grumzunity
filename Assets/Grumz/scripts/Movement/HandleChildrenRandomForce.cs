﻿using UnityEngine;
using System.Collections;

public enum HitPosition
{
		TopRight,
		TopLeft,
		BotRight,
		BotLeft,
		Center
}

public class HandleChildrenRandomForce : MonoBehaviour
{


		public AddRandom2DForceOnStart[] forces;
		public bool isShield = false;
		Transform par;
		Vector3 origPos;

		
		void Awake ()
		{
				par = transform.parent;
				origPos = transform.localPosition;
				forces = GetComponentsInChildren<AddRandom2DForceOnStart> ();
				if (!isShield) {
						G.i.OnCollision += ChangeForces;
						G.i.OnPlayerDeath += HandleOnPlayerDeath;
				} else {
						G.i.OnPlayerShield += HandleOnPlayerShield;
				}
				//AddForces ();
				
		}

		public void ChangeForces (HitPosition hitPos)
		{
				if (hitPos == HitPosition.TopLeft) {
						ChangeForces (-4, 15, -12, 6);
				} else if (hitPos == HitPosition.TopRight) {
						ChangeForces (-15, 4, -12, 6);
				} else if (hitPos == HitPosition.BotLeft) {
						ChangeForces (-4, 15, -6, 12);
				} else if (hitPos == HitPosition.BotRight) {
						ChangeForces (-15, 4, -6, 12);
				}
				AddForces ();
		}


		void ChangeForces (float xMin, float xMax, float yMin, float yMax)
		{
				for (int i = 0; i < forces.Length; i++) {
						forces [i].xForceMax = xMin;
						forces [i].xForceMin = xMax;
						forces [i].yForceMin = yMin;
						forces [i].yForceMax = yMax;
				}
				//AddForces ();
		}
		void HandleOnPlayerShield (bool isStart)
		{
				if (!isStart) {
						print ("addding forces");
						AddForces ();
				} else if (isStart) {
						StopAllCoroutines ();
						if (transform.parent != par)
								transform.parent = par;
						if (transform.localPosition != origPos)
								transform.localPosition = origPos;
						if (transform.localRotation != Quaternion.identity)
								transform.localRotation = Quaternion.identity;
				}
		}

		void HandleOnPlayerDeath ()
		{
				//	AddForces ();
		}

		// Use this for initialization
		void OnEnable ()
		{

//				for (int i = 0; i <forces.Length; i++) {
//						forces [i].transform.parent = transform;
////						forces [i].enabled = false;
//				}
				if (transform.parent != par)
						transform.parent = par;
				if (transform.localPosition != origPos)
						transform.localPosition = origPos;
				if (transform.localRotation != Quaternion.identity)
						transform.localRotation = Quaternion.identity;
		
//				gameObject.SetActive (false);
		}

		void OnDisable ()
		{
				if (isShield) {

						transform.parent = par;
						transform.localPosition = origPos;
						transform.localRotation = Quaternion.identity;	
						StopAllCoroutines ();
				}
		}

		public void AddForces ()
		{
				
//				//debug.log ("adding force");
				if (isShield)
						transform.parent = null;
				for (int i = 0; i <forces.Length; i++) {
						
						//forces [i].transform.parent = null;
						forces [i].AddForce ();


				}
				if (isShield) {

						StartCoroutine (Auto.Wait (3f, () => {

//						for (int i = 0; i <forces.Length; i++) {
//			
//								forces [i].transform.parent = transform;
//			
//			
//						}
				
								if (isShield) {
										transform.parent = par;
										transform.localPosition = origPos;
										transform.localRotation = Quaternion.identity;

										//gameObject.SetActive (false);
								}
						}));
				}
		}

		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.Z))
						AddForces ();
	
		}
}
