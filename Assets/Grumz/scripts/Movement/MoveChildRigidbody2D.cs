﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveChildRigidbody2D : MonoBehaviour
{

		
		public 	MoveableObject[] children;
		//public List<FadeAtEndGame> faders;
		public float delay = 0;
		public float duration = 0.2f;
		public EaseType easer;
		//public AnimationCurve curve;
		bool immediate = false;
		
		public AudioClip activated;
		public bool isShield = false;
		

		void Start ()
		{
				if (isShield)
						G.i.OnPlayerShield += HandleOnPlayerShield;
				if (delay <= 0)
						immediate = true;
				if (children.Length == 0) {
						children = GetComponentsInChildren<MoveableObject> ();
				}
						
		}

		void HandleOnPlayerShield (bool isStart)
		{
				
				//enabled = true;
				if (isStart) {
						gameObject.SetActive (false);
						gameObject.SetActive (true);
						//ScatterChildren ();
						//StartCoroutine (Enable ());
				} else if (!isStart) {
						gameObject.SetActive (true);
						MoveChildrenBack ();
				}

		}

		void OnEnable ()
		{
				//		if (immediate)
//				ScatterChildren ();
				StartCoroutine (Enable ());
			
		}
		

		public void SetUp ()
		{
				if (!immediate) {
						StartCoroutine (Enable ());
				}
		}
		
		IEnumerator Enable ()
		{
				yield return new WaitForEndOfFrame ();
				if (delay <= 0)
						immediate = true;
				if (immediate || delay <= 0)
						MoveChildren ();
				else
						StartCoroutine (Auto.Wait (delay, MoveChildren));
		}
		

		void OnDisable ()
		{
				MoveChildrenBack ();
				//var sprite = GetComponent<SpriteRenderer> ();

		}

		public void MoveChildren ()
		{
				for (int i = 0; i < children.Length; i++) {
						children [i].MoveBack (duration, easer);
				}
				if (activated != null)
						AudioManager.instance.PlayAudio (activated);
		}

		public void EnableCollider ()
		{
		
				for (int i = 0; i < children.Length; i++) {
						
						children [i].GetComponent<Collider2D> ().enabled = true;
				}
		}

		public void DisableCollider ()
		{
	
				for (int i = 0; i < children.Length; i++) {
						children [i].GetComponent<Collider2D> ().enabled = false;
				}
		}


//		public void FadeEnd ()
//		{
//				if (faders.Length > 0) {
//
//						for (int i = 0; i < faders.Length; i++) {
//								faders [i].FadeSprite ();
//						}
//				} else {
//						//debug.log ("no faders");
//				}
//		
//		}

		public void MoveChildrenBack ()
		{
				for (int i = 0; i < children.Length; i++) {
						children [i].MoveBack ();
				}
				
		}

		public void ScatterChildren ()
		{
				for (int i = 0; i < children.Length; i++) {
						children [i].Move ();
				}
		
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
