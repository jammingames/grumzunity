﻿using UnityEngine;
using System.Collections;

public class MoveableObject : MonoBehaviour
{
		
		public Vector3 origPos;
		Quaternion origRot;
		public Transform mover;
		public float xMin = 10;
		public float xMax = 15;
		public float yMin = -10;
		public float yMax = 10;
		public bool moveOnStart;
		public bool isImmediate = true;

		//MoveToPosition moveTest;

		//Vector3 offset fromTransform;

	
		void Awake ()
		{
				origPos = transform.localPosition;
//				print (origPos);
				origRot = transform.rotation;
				//	moveTest = GetComponent<MoveToPosition> ();
				//				if (moveOnStart)
				//				MoveBack ();
				//GetComponent<ParticleSystem>().startColor
		
		}


		public void MoveBack (float duration, EaseType easer)
		{

//				print ("starting move back coroutine");
				
				StartCoroutine (transform.MoveToTransformOffset (mover, origPos, duration, easer, null));
//				moveTest.enabled = true;
				StartCoroutine (transform.RotateTo (origRot, duration, easer, null));
		}

		public void MoveBack ()
		{
				if (transform.localPosition != origPos)
						transform.localPosition = origPos;
				if (transform.rotation != origRot)
						transform.rotation = origRot;
		}

		public void Move ()
		{
//				if (mover != null)
//						origPos = mover.position;
//				//debug.log ("moving object");
				if (Random.value > 0.5f) {
						transform.localPosition = new Vector3 (Random.Range (xMin, xMax), Random.Range (yMin, yMax), origPos.z);
				} else {
						transform.localPosition = new Vector3 (Random.Range (-xMin, -xMax), Random.Range (yMin, yMax), origPos.z);
				}
				transform.localRotation = Quaternion.Euler (0, 0, Random.Range (-90, 90));
		}


		void OnEnable ()
		{
				if (isImmediate) {
						

						MoveBack ();
//						if (GetComponent<SpriteRenderer> ()) {
//								Color col = GetComponent<SpriteRenderer> ().color;
//								col.a = 1;
//								GetComponent<SpriteRenderer> ().color = col;
//						}
						if (moveOnStart)
								Move ();	

						
				}
				
		}

}
