﻿using UnityEngine;
using System.Collections;

public class EnableColliderAfterDelay : MonoBehaviour
{

		Collider2D col;
		public float delay = 0.5f;
		void Awake ()
		{
				col = GetComponent<Collider2D> ();
				col.enabled = false;
		}
		void OnEnable ()
		{
				StartCoroutine (Auto.Wait (delay, () => {
						col.enabled = true;
				}));

		}
	
}
