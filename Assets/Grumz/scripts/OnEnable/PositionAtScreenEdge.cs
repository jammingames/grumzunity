﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof(Collider2D))]
public class PositionAtScreenEdge : MonoBehaviour
{

		float leftEdge, rightEdge;
		Collider2D col;

		void Awake ()
		{
				col = GetComponent<Collider2D> ();
				leftEdge = G.i.LEFT_SCREEN_EDGE;
				rightEdge = G.i.RIGHT_SCREEN_EDGE;
		}
		void Start ()
		{
				if (transform.localPosition.x > 0) {
						transform.localPosition = new Vector3 (rightEdge + col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
						
				} else {
						transform.localPosition = new Vector3 (leftEdge - col.bounds.size.x, transform.localPosition.y, transform.localPosition.z);
				}
		}

		

}
