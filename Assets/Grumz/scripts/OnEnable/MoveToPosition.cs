﻿using UnityEngine;
using System.Collections;

public class MoveToPosition : MonoBehaviour
{

		public Transform target;
		public float duration;
		public EaseType easer;

		float elapsed;
		Vector3 start;
		Vector3 range;


		void Awake ()
		{
				enabled = false;
		}

		void OnEnable ()
		{
				elapsed = 0;
				start = transform.localPosition;
		}
	


		void Update ()
		{
				
				if (transform.localPosition != target.localPosition) {
						////debug.log ("doing move");
						range = target.localPosition - start;
						elapsed = Mathf.MoveTowards (elapsed, duration, Time.deltaTime);
						transform.localPosition = (start + range * Ease.FromType (easer) (elapsed / duration));
				} else {
						////debug.log ("disabling moveTo");
						enabled = false;
				}
				


		}
	

}
