﻿using UnityEngine;
using System.Collections;

public class ChangeColorOfChildrenOnStart : MonoBehaviour
{

		SpriteRenderer[] renderers;


		// Use this for initialization
		void Awake ()
		{
				renderers = GetComponentsInChildren<SpriteRenderer> ();
		}

		public void ChangeColor (Color col)
		{
				for (int i = 0; i<renderers.Length; i++) {
						renderers [i].color = col;
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
