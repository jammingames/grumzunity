﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeGraphicOnStateChange : MonoBehaviour
{

		Graphic img;

		public GameState desiredState;

		public float target;
		public float duration = 0.3f;
		public float delay = 0;
		public bool disableAtEnd = true;

		void Awake ()
		{
				img = GetComponent<Graphic> ();
		}

		void OnEnable ()
		{
				G.i.OnStateChange += HandleOnStateChange;
				
				//col.a = 0;
				//img.color = col;
			
		}

		void OnDisable ()
		{
				Color col = img.color;
				col.a = 1;
				img.color = col;
				if (G.i != null) {
						G.i.OnStateChange -= HandleOnStateChange;
				}
		}

		void HandleOnStateChange (GameState nextState)
		{
				if (desiredState == nextState) {
						StartCoroutine (Auto.Wait (delay, () => {
								if (img != null)
										StartCoroutine (img.FadeImageAlpha (img.color.a, target, duration, () => {
												if (disableAtEnd) {
														gameObject.SetActive (false);
												}
										}));
						}));
				}
		}
	

}
