﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeInTextOnStart : MonoBehaviour
{
		public float duration = 0.3f;
		public float delay = 0;
		Text img;

	
		// Use this for initialization
		void OnEnable ()
		{
			
				img = GetComponent<Text> ();
				Color col = img.color;
				col.a = 0;
				img.color = col;
				StartCoroutine (Auto.Wait (delay, () => {

						StartCoroutine (img.FadeTextAlpha (0, 1, duration, null));
				}));
		}
	
		void OnDestroy ()
		{
		
		
				Color col = img.color;
				col.a = 1;
				img.color = col;
		
		
		}

}
