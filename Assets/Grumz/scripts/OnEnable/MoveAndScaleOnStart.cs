﻿using UnityEngine;
using System.Collections;

public class MoveAndScaleOnStart : MonoBehaviour
{


		public Transform target;
		public Vector3 scale ;
		public float duration;
		public EaseType easer;

		// Use this for initialization
		void Start ()
		{
				scale = new Vector3 (1.43f, 1.43f, 1);
				
		}


		public void MoveAndScale ()
		{
				StartCoroutine (transform.MoveToTransform (target, duration, easer, null));
				StartCoroutine (transform.ScaleTo (scale, duration, null));
		}


		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.J))
						MoveAndScale ();
		}
}
