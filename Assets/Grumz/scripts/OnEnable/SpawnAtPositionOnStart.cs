﻿using UnityEngine;
using System.Collections;

public class SpawnAtPositionOnStart : MonoBehaviour
{

		public GameObject prefab;
		public Transform spawnPos;
		public Transform parent;
		public Vector3 offset;
		public Vector3 rotation;

		// Use this for initialization
		void Start ()
		{
				Vector3 pos;
				if (spawnPos != null) {
						pos = spawnPos.localPosition + offset;

						
				} else {

						pos = transform.localPosition + offset;
				}
				var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				if (parent != null)
						go.transform.parent = parent;
		}


		public void SpawnPrefab (GameObject prefab)
		{
				Vector3 pos;
				if (spawnPos != null) {
						pos = spawnPos.localPosition + offset;
			
			
				} else {
			
						pos = transform.localPosition + offset;
				}
				var go = GameObject.Instantiate (prefab, pos, Quaternion.Euler (rotation)) as GameObject;
				if (parent != null)
						go.transform.parent = parent;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
