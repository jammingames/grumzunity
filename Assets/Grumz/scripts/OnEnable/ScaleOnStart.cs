﻿using UnityEngine;
using System.Collections;

public class ScaleOnStart : MonoBehaviour
{

		Vector3 origScale;
		public Vector3 targetScale;
		public float duration, delay;
		public EaseType easer;
		public bool scaleToOrig = true;
		public bool forceScale;
		public bool waitForScale = false;
		public bool flipOnRight = false;

		void Awake ()
		{
				origScale = transform.localScale;
			
		}

		// Use this for initialization
		void OnEnable ()
		{
				
				if (flipOnRight && transform.position.x > 0) {
						targetScale.x *= -1;
				}
				
				if (forceScale) {
						origScale = new Vector3 (10, 10, 1);
				}
				if (scaleToOrig) {
						transform.localScale = targetScale;
				} else if (!scaleToOrig) {
						transform.localScale = origScale;
				}
				
				StartCoroutine (Auto.Wait (delay, () => {
						if (waitForScale) {
								origScale = transform.localScale;
						}
						
						if (scaleToOrig) {
								//transform.localScale = targetScale;
								StartCoroutine (transform.ScaleTo (origScale, duration, easer, null));
						} else {
								//transform.localScale = origScale;
								StartCoroutine (transform.ScaleTo (targetScale, duration, easer, null));
						}

				}));
		}

		public void Disable ()
		{
				
				StartCoroutine (transform.ScaleTo (targetScale, duration, easer, () => { 
						gameObject.SetActive (false);
				}));
		}


		void OnDisable ()
		{
				StopAllCoroutines ();
				if (scaleToOrig) {
						transform.localScale = targetScale;
				} else
						transform.localScale = origScale;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
