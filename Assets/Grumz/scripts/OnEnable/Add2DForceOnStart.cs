﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof(Rigidbody2D))]
public class Add2DForceOnStart : MonoBehaviour
{

		public Vector2 force;
		public bool addRandom = false;
		public float forceOffset;
		public float maxVelocity;

		// Use this for initialization
		void Start ()
		{
				if (transform.position.x > 0) {
						force.x *= -1;
						GetComponent<Rigidbody2D> ().AddForce (force);
				} else
						GetComponent<Rigidbody2D> ().AddForce (force);

				////debug.log ("Adding force: " + force);
		}

		void FixedUpdate ()
		{
				if (addRandom) {

						Vector2 newForce;
						if (transform.position.y > 0)
								newForce = new Vector2 (0, Random.Range (0, -forceOffset));
						else
								newForce = new Vector2 (0, Random.Range (0, forceOffset));
						GetComponent<Rigidbody2D> ().AddForce (newForce);
				}
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, Mathf.Clamp (GetComponent<Rigidbody2D> ().velocity.y, -maxVelocity, maxVelocity));
		}

}
