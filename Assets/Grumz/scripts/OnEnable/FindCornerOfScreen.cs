﻿using UnityEngine;
using System.Collections;

public class FindCornerOfScreen : MonoBehaviour
{
		Camera cam;
		Vector3 pos;
		public Vector2 offsetPercent;
		public Vector2 newOffset;
		public bool applyOffset;
		public bool bannerOffset;
		public bool isVisible;
		
		public enum Corner
		{
				BottomRight,
				BottomLeft,
				TopRight,
				TopLeft
		}
		
		public Corner corner = Corner.BottomRight;

		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.G))
						SetPosition (64);
		}
		

		void Awake ()
		{
				//	G.OnResize += SetPosition;
				var _cam = Camera.main;
				//if (ORef.instance.secondCam != null)
				//	cam = ORef.instance.secondCam;
				//el/se
				//	(Camera.main);
				

				
//				cam = Camera.main
				Vector3 p = new Vector3 ();
				Vector3 boundaries = new Vector3 (_cam.pixelWidth, _cam.pixelHeight, _cam.nearClipPlane);
				Vector3 offset = Vector3.zero;
				if (applyOffset)
						offset = new Vector3 (boundaries.x * offsetPercent.x, boundaries.y * offsetPercent.y, 0);

						
				if (corner == Corner.BottomRight) {
						
						p = _cam.ScreenToWorldPoint (new Vector3 (boundaries.x - offset.x, 0 + offset.y, boundaries.z));
						

				} else if (corner == Corner.TopRight) {
						p = _cam.ScreenToWorldPoint (new Vector3 (boundaries.x - offset.x, boundaries.y - offset.y, boundaries.z));
						
				
				} else if (corner == Corner.TopLeft) {
						p = _cam.ScreenToWorldPoint (new Vector3 (0 + offset.x, boundaries.y - offset.y, boundaries.z));
						
				} else if (corner == Corner.BottomLeft) {
						p = _cam.ScreenToWorldPoint (new Vector3 (0 + offset.x, 0 + offset.y, boundaries.z));
						
				}
				pos = new Vector3 (p.x, p.y, transform.position.z);
				
				transform.position = pos;	
		}

		void ChangeOffset ()
		{
				offsetPercent = newOffset;
				//	SetPosition ();
		}

		public void SetPosition (float offsetY)
		{
				var _cam = Camera.main;
				//if (ORef.instance.secondCam != null)
				//	cam = ORef.instance.secondCam;
				//el/se
				//	(Camera.main);
		
				
		
				//				cam = Camera.main
				Vector3 p = new Vector3 ();
				Vector3 boundaries = new Vector3 (_cam.pixelWidth, _cam.pixelHeight, _cam.nearClipPlane);
				Vector3 offset = Vector3.zero;
				if (applyOffset)
						offset = new Vector3 (boundaries.x * offsetPercent.x, boundaries.y * offsetPercent.y, 0);
		
		
				if (corner == Corner.BottomRight) {
			
						p = _cam.ScreenToWorldPoint (new Vector3 (boundaries.x - offset.x, 0 + offset.y + offsetY, boundaries.z));
			
			
				} else if (corner == Corner.TopRight) {
						p = _cam.ScreenToWorldPoint (new Vector3 (boundaries.x - offset.x, boundaries.y - offset.y, boundaries.z));
			
			
				} else if (corner == Corner.TopLeft) {
						p = _cam.ScreenToWorldPoint (new Vector3 (0 + offset.x, boundaries.y - offset.y, boundaries.z));
			
				} else if (corner == Corner.BottomLeft) {
						p = _cam.ScreenToWorldPoint (new Vector3 (0 + offset.x, 0 + offset.y + offsetY, boundaries.z));
			
				}
				pos = new Vector3 (p.x, p.y, transform.position.z);
				if (isVisible)
						transform.position = pos;
				//	G.TriggerReposition (pos);
	
		}
}
