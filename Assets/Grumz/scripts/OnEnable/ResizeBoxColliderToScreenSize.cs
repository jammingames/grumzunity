﻿using UnityEngine;
using System.Collections;

public class ResizeBoxColliderToScreenSize : MonoBehaviour
{

		public float skinWidth = 0.2f;
		BoxCollider2D worldCollider;
		PlayerMovement playerMove;

		// Use this for initialization
		void Start ()
		{
				playerMove = G.i.playerMove;
				worldCollider = GetComponent<BoxCollider2D> ();
				float offset = (playerMove.col.bounds.size.x * 2) + skinWidth;
				worldCollider.size = new Vector2 (Mathf.Abs (G.i.LEFT_SCREEN_EDGE) + G.i.RIGHT_SCREEN_EDGE - offset, Mathf.Abs (G.i.BOT_SCREEN_EDGE) + G.i.TOP_SCREEN_EDGE - offset);
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
