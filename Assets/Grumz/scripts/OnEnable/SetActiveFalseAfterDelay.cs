﻿using UnityEngine;
using System.Collections;

public class SetActiveFalseAfterDelay : MonoBehaviour
{

		public float delay;
		public bool recycle = true;
		// Use this for initialization
		void OnEnable ()
		{
				StartCoroutine (Auto.Wait (delay, () => {
						if (recycle)
								gameObject.Recycle ();
						else
								gameObject.SetActive (false);
				}));
		}
	

}
