﻿using UnityEngine;
using System.Collections;

public class RotateTrailOnStart : MonoBehaviour
{

		public Vector3 startRotation;
		public Vector3 targetScale;
//		Vector3 origScale;

//		Quaternion origRotation;
		// Use this for initialization
		void Start ()
		{
//				origScale = transform.localScale;
				transform.localScale = targetScale;
//				origRotation = transform.localRotation;
				transform.localRotation = Quaternion.Euler (startRotation);

				
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
