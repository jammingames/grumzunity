﻿using UnityEngine;
using System.Collections;

public class ResetPositionOnDisable : MonoBehaviour
{

		Vector3 origPos;

		// Use this for initialization
		void Start ()
		{
				origPos = transform.localPosition;
		}

		void OnDisable ()
		{
				transform.localPosition = origPos;
		}

}
