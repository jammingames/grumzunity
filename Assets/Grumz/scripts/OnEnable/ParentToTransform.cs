﻿using UnityEngine;
using System.Collections;

public class ParentToTransform : MonoBehaviour
{


		public Transform parent;

		void Start ()
		{
				if (parent)
						transform.parent = parent;
		}

}
