﻿using UnityEngine;
using System.Collections;

public class RotateCoroutineOnStart : MonoBehaviour
{

//		public float duration;
//		public float rotateDegrees;
		
		public float degreesPerSecond = 10f;
		public float rotationDegreesAmount = 90f;
		float totalRotation = 0;
		// Use this for initialization
		void Start ()
		{
				//		StartCoroutine (SwingOpen ());
		}
	
		// Update is called once per frame
		void Update ()
		{

				if (Input.GetKeyDown (KeyCode.V)) {
						StartCoroutine (SwingOpen ());
				}
				//if we haven't reached the desired rotation, swing
		
//				if (Mathf.Abs (totalRotation) < Mathf.Abs (rotationDegreesAmount))
//						SwingOpen ();
		}
	
		IEnumerator SwingOpen ()
		{   
				while (Mathf.Abs (totalRotation) < Mathf.Abs (rotationDegreesAmount)) {

						float currentAngle = transform.localRotation.eulerAngles.z;
						transform.rotation = 
							Quaternion.AngleAxis (currentAngle + (Time.deltaTime * degreesPerSecond), Vector3.forward);
						totalRotation += Time.deltaTime * degreesPerSecond;
						yield return null;
				}
				float roundedAngle = Mathf.Floor (transform.localRotation.eulerAngles.z);
				transform.rotation = Quaternion.Euler (transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, roundedAngle);
				
				////debug.log (roundedAngle);
				////debug.log ("done");
		}
}
