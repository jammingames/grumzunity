﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeInImageOnStart : MonoBehaviour
{

		public bool loadLevel = false;
		public float duration = 0.3f;
		public float delay = 0;
		[Range(0,1)]
		public float
				target = 1;
		Graphic img;
	
		void OnEnable ()
		{
				img = GetComponent<Graphic> ();
				if (target > 0) {
						Color col = img.color;
						col.a = 0;
						img.color = col;
				}
				StartCoroutine (Auto.Wait (delay, () => {
						if (img != null)
								StartCoroutine (img.FadeImageAlpha (img.color.a, target, duration, () => {
										if (loadLevel)
												Application.LoadLevel ("__init");
								}));
				}));
		}
	


}
