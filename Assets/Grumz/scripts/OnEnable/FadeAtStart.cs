﻿

using UnityEngine;
using System.Collections;

public class FadeAtStart : MonoBehaviour
{

		public float duration = 1f;
		public float delay = 0f;
		public float start;
		public float target;
		SpriteRenderer spr;
		public bool randomize;
		
		public bool shouldDestroy = true;
		public bool startAtZero;
		public bool stopOnPlayer = false;

		void OnEnable ()
		{
				//var sprite;
				Color col = GetComponent<SpriteRenderer> ().color;
				if (startAtZero) {

						col.a = 0;
						GetComponent<SpriteRenderer> ().color = col;
						
				}

//				G.i.OnPlayerDeath += HandleOnPlayerDeath;
					
				StartCoroutine (Auto.Wait (delay, () => {


		
						if (GetComponent<SpriteRenderer> ()) {
								spr = GetComponent<SpriteRenderer> ();
								//float start = spr.color.a;
								
								if (randomize)
										duration *= (Random.value + 0.2f);
								StartCoroutine (spr.FadeSprite (start, target, duration, delay, () => {
										if (shouldDestroy)
												GameObject.Destroy (gameObject);
										//gameObject.Recycle ();
								}));
						}
				}));
				
				if (stopOnPlayer)
						G.i.OnPlayerDeath += HandleOnPlayerDeath;
			
		}

		void HandleOnPlayerDeath ()
		{
				StopAllCoroutines ();
		}


		void OnDisable ()
		{
				if (startAtZero) {
						Color col = GetComponent<SpriteRenderer> ().color;
						col.a = 0;
						GetComponent<SpriteRenderer> ().color = col;
				} else {

						Color col = GetComponent<SpriteRenderer> ().color;
						col.a = 1;
						GetComponent<SpriteRenderer> ().color = col;
				}
		}

}
