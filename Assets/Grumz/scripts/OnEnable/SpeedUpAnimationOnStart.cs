﻿using UnityEngine;
using System.Collections;

public class SpeedUpAnimationOnStart : MonoBehaviour
{

		Animator anim;
		public float targetSpeed;
		public float duration;
		public float delay = 6;
		float origDuration, origDelay;
		float origSpeed;


		public SpriteRenderer render;


		void Awake ()
		{
				origDuration = duration;
				origDelay = delay;
				//render = GetComponent<SpriteRenderer> ();
				anim = render.gameObject.GetComponent<Animator> ();
			
				origSpeed = anim.speed;

				G.i.OnSuperInvul += SuperInvul;
				G.i.OnPlayerInvul += HandleOnPlayerInvul;
		}

		void SuperInvul (bool isStart, float length)
		{
				if (isStart) {
						origSpeed = anim.speed;
						float newDelay = length;
//						float newDuration = (duration / (newDelay + duration) * length);
						
						
						
						float newDuration = length * 0.1f;
						
						duration = newDuration;
						delay = newDelay;
						

//						StopAllCoroutines ();
//						
//						StartCoroutine (Auto.Wait (newDelay, () =>
//						{
//								if (render != null)
//										StartCoroutine (render.FadeSprite (render.color.a, 0, newDuration, 0.8f, null));
//								StartCoroutine (anim.SpeedUp (targetSpeed, newDuration, null));
//						}));
				} else {
						duration = origDuration;
						delay = origDelay;
				}
		}

		// Use this for initialization
		void OnEnable ()
		{
				

//				anim = GetComponent<Animator> ();
			
		}

		void HandleOnPlayerInvul (bool isStart)
		{
				if (isStart) {
						StartCoroutine (Auto.Wait (delay, () =>
						{
								if (render != null)
										StartCoroutine (render.FadeSprite (render.color.a, 0, duration, 0.8f, null));
								StartCoroutine (anim.SpeedUp (targetSpeed, duration, null));
						}));
				} else {

				}
		}

		void OnDisable ()
		{
				anim.speed = origSpeed;
//				if (G.i != null) {
//						G.i.OnSuperInvul -= SuperInvul;
//				}
		}



		

	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
