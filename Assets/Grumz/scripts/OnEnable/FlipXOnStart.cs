﻿using UnityEngine;
using System.Collections;

public class FlipXOnStart : MonoBehaviour
{

		// Use this for initialization
		void OnEnable ()
		{

				if (transform.position.x > 0) {
//						//debug.log ("changing scale from: " + transform.localScale.x);
						Vector3 scale = transform.localScale;
						scale.x *= -1;
						transform.localScale = scale;
//						//debug.log ("scale changed to : " + transform.localScale.x);
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
