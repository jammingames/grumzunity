﻿using UnityEngine;
using System.Collections;

public class RotateDiagonally : MonoBehaviour
{

//		public float duration;
//		public float rotateDegrees;
		public float duration;
		public float degreesPerSecond = 49.1f;
		public float rotationDegreesAmount = 540f;
		public AudioSource weeeeoooowwww;
		
		
		public Transform midLine;


		float totalRotation = 0;
		// Use this for initialization
		void Awake ()
		{
				degreesPerSecond = rotationDegreesAmount / duration;
				
				
				//		StartCoroutine (SwingOpen ());
				G.i.OnSuperInvul += HandleOnSuperInvul;
		}

		void HandleOnSuperInvul (bool isStart, float duration)
		{

				if (isStart) {
						degreesPerSecond = rotationDegreesAmount / duration;
						StartCoroutine (SwingOpen ());
				}
		}

		void OnEnable ()
		{
				transform.rotation = Quaternion.identity;
				G.i.TriggerSuperInvul (false, 0);
		}
	
		// Update is called once per frame
		void Update ()
		{

				if (Input.GetKeyDown (KeyCode.V)) {
						G.i.TriggerSuperInvul (true, duration);
						//StartCoroutine (SwingOpen ());
				}
				if (Input.GetKeyDown (KeyCode.C)) {
						StartCoroutine (SwingClosed ());
				}
				//if we haven't reached the desired rotation, swing
		
//				if (Mathf.Abs (totalRotation) < Mathf.Abs (rotationDegreesAmount))
//						SwingOpen ();
		}

		//get your shit together and name this better
		public IEnumerator SwingClosed ()
		{   
				while (Mathf.Abs (totalRotation) < Mathf.Abs (rotationDegreesAmount)) {
			
						float currentAngle = transform.localRotation.eulerAngles.z;
						transform.rotation = 
				Quaternion.AngleAxis (currentAngle + (Time.deltaTime * -degreesPerSecond), Vector3.forward);
						totalRotation += Time.deltaTime * degreesPerSecond;
						yield return null;
				}
				float roundedAngle = Mathf.Floor (transform.localRotation.eulerAngles.z);
				transform.rotation = Quaternion.Euler (transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, roundedAngle);

				////debug.log (roundedAngle);
				////debug.log ("done");
		}
	
		public IEnumerator SwingOpen ()
		{   
				//	G.i.TriggerSuperInvul (true, 0);
				//AudioManager.instance.PlayAudio (weeeeoooowwww);
				
				//	render.sharedMaterial.shader = defaultShader;

				while (Mathf.Abs (totalRotation) < Mathf.Abs (rotationDegreesAmount)) {

						float currentAngle = transform.localRotation.eulerAngles.z;
						midLine.rotation = Quaternion.AngleAxis (currentAngle + (Time.deltaTime * degreesPerSecond), Vector3.forward);
						transform.rotation = 
							Quaternion.AngleAxis (currentAngle + (Time.deltaTime * degreesPerSecond), Vector3.forward);
						totalRotation += Time.deltaTime * degreesPerSecond;
						yield return null;
				}
				if (transform.rotation.eulerAngles.z > 100 && transform.rotation.eulerAngles.z < 300) {
						transform.rotation = Quaternion.Euler (transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, 180);
						midLine.rotation = Quaternion.Euler (transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, 180);
				} else {
						transform.rotation = Quaternion.Euler (transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, 0);
						midLine.rotation = Quaternion.Euler (transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, 0);
				}
				float roundedAngle = Mathf.Floor (transform.localRotation.eulerAngles.z);
				transform.rotation = Quaternion.Euler (transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, roundedAngle);
				midLine.rotation = Quaternion.Euler (transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, roundedAngle);
				totalRotation = 0;
				//mat.shader = maskShader;
				
				////debug.log (roundedAngle);
				////debug.log ("done");
				//G.i.TriggerSuperInvul (false);
		}
}
