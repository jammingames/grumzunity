﻿using UnityEngine;
using System.Collections;

public class AddRandom2DForceOnStart : MonoBehaviour
{
		[Range (-15, 15)]
		public float
				xForceMin;
		[Range (-15, 15)]
		public float
				xForceMax;

		[Range (-15, 15)]
		public float
				yForceMin;
		[Range (-15, 15)]
		public float
				yForceMax;


		Vector3 origPos;
		Quaternion origRot;

		public bool isImmediate = true;
		public float delay = 0;

		void Awake ()
		{
				origPos = transform.position;
				origRot = transform.rotation;
		}

		void OnEnable ()
		{
				if (isImmediate)
						AddForce ();
		}

		// Use this for initialization
		public void AddForce ()
		{
				StartCoroutine (Auto.Wait (delay, () => {

						Rigidbody2D rb = GetComponent<Rigidbody2D> ();
						rb.isKinematic = false;
						Vector2 force = new Vector2 (Random.Range (xForceMin, xForceMax), Random.Range (yForceMin, yForceMax));
						rb.AddForce (force);
				}));
		}

		void OnDisable ()
		{
				transform.position = origPos;
				transform.rotation = origRot;
		}
	
		
}
