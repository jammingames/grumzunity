﻿using UnityEngine;
using System.Collections;

public class ResetColorOnDisable : MonoBehaviour
{

		Color origColor;


		
		// Use this for initialization
		void Awake ()
		{
				origColor = GetComponent<SpriteRenderer> ().color;
		}


		void OnDisable ()
		{
				GetComponent<SpriteRenderer> ().color = origColor;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
