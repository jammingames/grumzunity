﻿using UnityEngine;
using System.Collections;

public class FadeOnStartAfterDelay : MonoBehaviour
{

		public Transform target;
		// Use this for initialization
		void OnEnable ()
		{
				foreach (Transform child in target) {
						StartCoroutine (child.GetComponent<SpriteRenderer> ().FadeSprite (1, 0, 12, 6f, null));
				}
		}

		void OnDestroy ()
		{
				if (!target)
						return;

				foreach (Transform child in target) {
						Color col = child.GetComponent<SpriteRenderer> ().color;
						col.a = 1;
						child.GetComponent<SpriteRenderer> ().color = col;
				}
				
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
