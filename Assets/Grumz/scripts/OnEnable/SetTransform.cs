﻿using UnityEngine;
using System.Collections;

public class SetTransform : MonoBehaviour
{


		public Transform parentTransform;
		public Vector3 pos;
//		Vector3 origPos;
		// Use this for initialization
		void Awake ()
		{
//				origPos = transform.position;
				G.i.OnPlayerShield += HandleOnPlayerShield;
		}

		void OnEnable ()
		{
				SetParent ();
		}

		void HandleOnPlayerShield (bool isStart)
		{
				if (isStart) {
						SetParent ();
				} else if (!isStart)
						RemoveParent ();
		}

		public void SetParent ()
		{
				transform.parent = parentTransform;
				transform.localPosition = new Vector3 (0, 0, 0.92f);
				
				transform.rotation = Quaternion.identity;
//				//debug.log (transform.parent);
		}

		public void RemoveParent ()
		{
				transform.parent = null;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
