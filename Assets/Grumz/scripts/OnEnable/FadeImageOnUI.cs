﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeImageOnUI : MonoBehaviour
{
		public float duration = 0.3f;
		public float delay = 0;
		[Range(0,1)]
		public float
				target = 1;
		Graphic img;

		public bool shouldDisable = false;

		bool canFire = false;

		void OnEnable ()
		{
				img = GetComponent<Graphic> ();
				Color col = img.color;
				col.a = 0;
				img.color = col;

				StartCoroutine (Auto.Wait (1.5f, () => {
						canFire = true;
				}
				));
		}
//		Image img;
	
		public void OnFade ()
		{
				//if (GetComponent<gra> ())
				if (canFire) {

					
					
					
						StartCoroutine (Auto.Wait (delay, () => {
								//if (img2 != null)
								//		StartCoroutine (img2.FadeImageAlpha (0, 1, duration, null));
								if (img != null)
										StartCoroutine (img.FadeImageAlpha (img.color.a, target, duration, () => {
												if (shouldDisable)
														gameObject.SetActive (false);
										}));
						}));
				}
		}
//	
//		void OnDisable ()
//		{
//				Color col = img.color;
//				col.a = 1;
//				img.color = col;
//		}

}
